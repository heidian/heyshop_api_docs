#线下商铺

**以下api基于HOST: `https://offline.heidian.io/`**

{% method -%}

##店员查看本门店的自提包裹列表

```
GET /api/orders/pick_up_code/
```

**该接口需要Authorization为`stafftoken`**

###参数 Query

```
id: 1001
id__in: 1001,1002,1003
stock_status: unprepare
stock_status__in: unprepare, prepared
heyshop_order_id: 1001
heyshop_order_id__in: 1001,1002,1003
key: 123456
key_in: 123456,654321
key_status: used
key_status__in: used, unused, expired, canceled
```

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/pick_up_code/?id=1001
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##查看订单详情

```
GET /api/orders/pick_up_code/[id]/order/
```

**该接口需要Authorization为`stafftoken`**

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/pick_up_code/1001/order/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##customer 通过 order token 查询对应的提货信息

```
GET /api/orders/pick_up_code/fetch/?order_token=xxxxxx&shop=super.heidianer.com
```

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/pick_up_code/fetch/?order_token=123456&shop=super.heidianer.com
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##更新备货状态

```
POST /api/orders/pick_up_code/[id]/stock_prepare/
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/pick_up_code/1001/stock_prepare/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##店员通过提货码核对信息

```
GET /api/orders/pick_up_code/check/?key=xxxxxx
```

**该接口需要Authorization为`stafftoken`**

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/pick_up_code/check/?key=129322
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##店员核销提货码

```
POST /api/orders/pick_up_code/consume/
```

**该接口需要Authorization为`stafftoken`**

###数据格式

```
{
  "key": "129322"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/pick_up_code/consume/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##店员冲正提货码

```
POST /api/orders/pick_up_code/rollback/
```

**该接口需要Authorization为`stafftoken`**

###数据格式

```
{
  "key": "129322"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/pick_up_code/rollback/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##获取线下门店列表

```
GET  /api/shops/offline_shop/?shop=super.heidianer.com
```

{% sample lang="http" -%}

###发送请求

```
GET  /api/shops/offline_shop/?shop=super.heidianer.com
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##获取下属员工列表

```
GET  /api/shops/staff/
```

**该接口需要Authorization为`stafftoken`，且身份为店长**

{% sample lang="http" -%}

###发送请求

```
GET  /api/shops/staff/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##门店经理创建下属员工

```
POST  /api/shops/staff/
```

**该接口需要Authorization为`stafftoken`，且身份为店长**

###数据格式

```
{
  "username": "xxx",
  "password": "yyy"
}
```

{% sample lang="http" -%}

###发送请求

```
POST  /api/shops/staff/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##门店员工登录(经理也属于员工)

```
POST /api/shops/staff_login/
```

###数据格式

```
{
  "username": "xxx",
  "password": "yyy"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/staff_login/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}



{% method -%}

##查询操作记录

```
GET /api/shops/action_history/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/action_history/
```

###返回结果

```json
{
	...
}
```

{% endmethod %}







