#VIP会员积分插件

**以下api基于HOST: `https://loyalty.heidian.io/`**

{% method -%}

##嘿店授权链接跳转

```
GET /api/shops/oauth/jump/
```

## Query 参数

```
shop_name: test
```

{% sample lang="js" -%}

###返回结果

该接口返回结果为html页面，页面内容为具有授权功能的跳转URL

{% endmethod %}


{% method -%}

##使用授权code换取access token

```
POST /api/shops/oauth/token/
```

###数据格式

```json
{
	"code": "abcde"
	"state": "testshop"
	"shop": "testshop.heidianer.com"
}
```

###返回结果

```json
{
	"id": 123,
	"access_token": "xuyeXxLv2dkxtMdOsbGjzoYRg6RTFm", 
	"refresh_token": "veuc9YGW6BSJDiBUBPVeHxpPK2c8Mf", 
	"scope": "read write", 
	"expires_in": 30000 
}
```

{% endmethod %}



{% method -%}

##查询会员等级规则

```
GET /api/vip/level/
```


{% sample lang="js" -%}

###返回结果

```json
[
	{
	"id": 1001,
	"priority": 2,
	"title": "test",
	"is_enabled": true,       //是否启用该会员等级规则
	"freeshipping": true,     //包邮
	"sign_points": 1234,             //签到送积分值
	"discount": 15,        
	"initial_coupons": [	
		{ 
			"coupon_id": 1001,     //必须从已有的优惠券内选择,优惠券id
			"coupon_title": "test",    
			"count": 1,
		},
		...
	],
    "birthday_coupons": [],   	 //优惠券生日赠送,[]则为选择不赠送
    "condition": {
				    "category": "register",     //升级条件, register(注册升级)/orders(累计消费笔数)/money(累计消费金额)/points(累计积分)
				    "value": 100        //设置升级条件的数值，仅适用于orders,money,points情况下。
    			}
    },
    ...
]
```

{% endmethod %}



{% method -%}

##查询指定会员等级规则

```
GET /api/vip/level/[id]/
```


{% sample lang="js" -%}

###返回结果

```json
{
	"id": 1001,
	"priority": 2,
	"title": "test",
	"is_enabled": true,       //是否启用该会员等级规则
	"freeshipping": true,     //包邮
	"sign_points": 1234,             //签到送积分值
	"discount": 15,        
	"initial_coupons": [	
   						 { 
	      					"coupon_id": 1001,     //必须从已有的优惠券内选择,优惠券id
	     					"coupon_title": "test",    
	     					"count": 1,
            			 },
            			...
            		   ],
    "birthday_coupons": [],   	 //优惠券生日赠送,[]则为选择不赠送
    "condition": {
				    "category": "register",     //升级条件, register(注册升级)/orders(累计消费笔数)/money(累计消费金额)/points(累计积分)
				    "value": 100        //设置升级条件的数值，仅适用于orders,money,points情况下。
    			}
}
```

{% endmethod %}



{% method -%}

##创建会员等级规则

```
POST /api/vip/level/
```

###数据格式

```json
{
	"is_enabled": true,       //是否启用该会员等级规则
	"freeshipping": true,     //包邮
	"sign_points": 1234,             //签到送积分值
	"discount": 15,        
	"initial_coupons": [	
   						 { 
	      					"coupon_id": 1001,     //必须从已有的优惠券内选择,优惠券id
	     					"count": 1,            //优惠券数量不能超过该优惠券最大值
            			 },
            			...
            		   ],
    "birthday_coupons": [],   	 //优惠券生日赠送,[]则为选择不赠送
    "condition": {
				    "category": "register",     //升级条件, register(注册升级)/orders(累计消费笔数)/money(累计消费金额)/points(累计积分)
				    "value": 100        //设置升级条件的数值，仅适用于orders,money,points情况下。
    			}
}
```

{% sample lang="js" -%}

###返回结果

同查询指定会员等级规则接口 返回完整的规则信息

{% endmethod %}



{% method -%}

##更新会员等级规则

```
PATCH /api/vip/level/[id]/
```

###数据格式

```json
{
	"title": "test",
	"is_enabled": true,       //是否启用该会员等级规则
	"freeshipping": true,     //包邮
	"sign_points": 1234,             //签到送积分值
	"discount": 15,        
	"initial_coupons": [	
   						 { 
	      					"coupon_id": 1001,     //必须从已有的优惠券内选择,优惠券id
	     					"count": 1,            //优惠券数量不能超过该优惠券最大值
            			 },
            			...
            		   ],
    "birthday_coupons": [],   	 //优惠券生日赠送,[]则为选择不赠送
    "condition": {
				    "category": "register",     //升级条件, register(注册升级)/orders(累计消费笔数)/money(累计消费金额)/points(累计积分)
				    "value": 100        //设置升级条件的数值，仅适用于orders,money,points情况下。
    			}
}
```

{% sample lang="js" -%}

###返回结果

同查询指定会员等级规则接口 返回完整的规则信息

{% endmethod %}


{% method -%}

##删除会员等级规则

```
DELETE /api/vip/level/[id]/
```

{% sample lang="js" -%}

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询积分规则

```
GET /api/vip/point_rule/
```

{% sample lang="js" -%}

###返回结果

```json
{
	"id": 12, 
	"is_enabled": true,    // 是否开启积分设置
	"can_combine": true,   // 积分和优惠券是否能同时使用
	"gain_rules":  [
					    {   
					      "level_id": 1,    //该会员等级对应的id值
					      "level_title": "test",  
					      "category":  "order", // order(每消费x笔，获取1积分)/money(消费满x元，获取1积分) 
					      "value": 100,         // 消费额
					    }，
					    ...
  					],
    "redeem_rules": [
					    {
					      "level_id": 2,
					      "level_title":  "test", 
					      "category": "coupon",  // coupon(满x积分，兑换quantity张优惠券)/money(每x积分，抵扣amount元)
					      "value": 10,   // 规定所需求的积分值
					      "coupon_id": 123,   
					      "coupon_title": "test", 
					      "quantity": 123,     //优惠券张数
					      "amount":  123      //可抵扣金额
					    },
					    ...
  					],
  	"redeem_limitations": [				//兑换限制
							    {
							      "level_id": 123,    //该会员等级对应的id值
							      "level_title":  "test",  
							      "value":123   //每月兑换积分上限
							    },
							    ...
  							],
	"clear_rule":{
						"category": "register",  //register(自注册日起)/gain(自积分获取日起)/fixed(固定时间) 
					    "days": 123,     //每x天
					    "date": "2017-03-31"    //开始日期
				}
}
```

{% endmethod %}



{% method -%}

##更新积分规则

```
PATCH /api/vip/point_rule/
```

###数据格式

```json
{
	"is_enabled": true,    // 是否开启积分设置
	"can_combine": true,   // 积分和优惠券是否能同时使用
	"gain_rules":  [
					    {   
					      "level_id": 1,    //该会员等级对应的id值
					      "category":  "order", // order(每消费x笔，获取1积分)/money(消费满x元，获取1积分) 
					      "value": 100          // 消费额
					    }，
					    ...
  					],
    "redeem_rules": [
					    {
					      "level_id": 2, //该会员等级对应的id值
					      "category": "coupon",   // coupon(满x积分，兑换quantity张优惠券)/money(每x积分，抵扣amount元)
					      "value": 10,   // 规定所需求的积分值
					      "coupon_id": 1001,    //已创建优惠券所对应的ID
					      "quantity": 10,     //优惠券张数
					      "amount":  10      //可抵扣金额
					    },
					    ...
  					],
  	"redeem_limitations": [				//兑换限制
							    {
							      "level_id": 123,    //该会员等级对应的id值
							      "value":123   //每月兑换积分上限
							    },
							    ...
  							],
	"clear_rule":{
						"category": "register",  //register(自注册日起)/gain(自积分获取日起)/fixed(固定时间) 
					    "days": 123,     //每x天
					    "date": "2017-03-31"    //开始日期
				}
}
```

{% sample lang="js" -%}

###返回结果

同查询积分规则接口，返回完整的规则信息

{% endmethod %}



{% method -%}

##查询全部积分兑换记录

```
GET /api/vip/redeem_history/
```

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="js" -%}

###返回结果

```json
{
    "count": 7,
    "next": null,
    "previous": null,
    "results": [
					{
					  "id": 12,  
					  "customer":  {
					    "id": 123,
					    "mobile": "131329193213",
					    ...		//完整的顾客信息
					  },
					  "points": 100,        // 使用积分
					  "category": "coupon",  // coupon(满x积分，兑换quantity张优惠券)/money(每x积分，抵扣amount元)
					  "coupon_id":123, 		//已创建优惠券的ID值
					  "coupon_title":  "test",    
					  "quantity":  20,     //优惠券张数
					  "amount":  100,      //可抵扣金钱
					  "created_at": "2017-01-01T12:10:12" 
					},
					...
				]
}	
```

{% endmethod %}



{% method -%}

##查询指定积分兑换记录

```
GET /api/vip/redeem_history/[id]/
```

{% sample lang="js" -%}

###返回结果

```json
{
	"id": 12,  
	"customer":  {
					    "id": 123,
					    "mobile": "131329193213",
					    ...		//完整的顾客信息
					 },
	"points": 100,        // 使用积分
	"category": "coupon",  // coupon(满x积分，兑换quantity张优惠券)/money(每x积分，抵扣amount元)
	"coupon_id":123, 		//已创建优惠券的ID值
	"coupon_title":  "test",    
	"quantity":  20,     //优惠券张数
	"amount":  100,      //可抵扣金钱
	"created_at": "2017-01-01T12:10:12" 
}
```

{% endmethod %}




