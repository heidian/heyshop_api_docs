# 博客文章


{% method -%}

## 查询文章列表

```
GET /api/shopfront/article/
```

### Query 参数

```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
title: title1
tag: tag1
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
page: 1
page_size: 10
```


{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/article/?page=1
```

### 返回结果

```json
{
  "count": 8,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 121,
      "name": "asdfasdf",
      "title": "jsdfasdf",
      "author": "asdfasdfasdf",
      "description": "asdfasdfasdfasdfasdfasdfasdfasdf...",
      "image": {
        "src": "https://up.img.heidiancdn.com/o_1apuui18darur5p17c210sgfe301-Banner.jpg",
        "metafield": {}
      },
      "tags": [
        "拉拉"
      ],
      "body_html": "<p>asdfasdfasdfasdfasdfasdfasdfasdf</p>",
      "published": true,
      "published_at": "2017-02-19T15:28:23.349790+08:00",
      "created_at": "2016-08-15T15:41:59+08:00",
      "updated_at": "2017-02-15T08:52:29.832662+08:00",
      "meta_title": "jsdfasdf",
      "meta_description": "asdfasdfasdfasdfasdfasdfasdfasdf...",
      "meta_image": "https://up.img.heidiancdn.com/o_1apuui18darur5p17c210sgfe301-Banner.jpg"
    },
    {
      "id": 139,
      "name": "sajkf",
      "title": "test",
      "author": "Anonymous",
      "description": "test",
      "image": {
      "src": "",
      "metafield": {}
      },
      "tags": [],
      "body_html": "<p>test<br /><br /><img title=\"images.png\" src=\"http://up.img.heidiancdn.com/o_1b5opfklnhbv1r6j11l61luvigl0images.png\"  data-src=\"http://up.img.heidiancdn.com/o_1b5opfklnhbv1r6j11l61luvigl0images.png\" data-ratio=\"1\" /><br />sdsdsd<img title=\"images.png\" src=\"http://up.img.heidiancdn.com/o_1b5opfklnhbv1r6j11l61luvigl0images.png\" data-src=\"http://up.img.heidiancdn.com/o_1b5opfklnhbv1r6j11l61luvigl0images.png\" data-ratio=\"1\" /></p>",
      "published": true,
      "published_at": "2017-02-19T15:28:23.349790+08:00",
      "created_at": "2016-08-30T18:41:34+08:00",
      "updated_at": "2017-02-15T08:52:29.441423+08:00",
      "meta_title": "test",
      "meta_description": "test",
      "meta_image": "https://up.img.heidiancdn.com/o_1bek0qh1ct741bq5rql1akfhpq0black-heys"
    },
    ...
  ]
}
```

{% endmethod %}


{% method -%}

## 查询指定文章

```
GET /api/shopfront/article/[id]/
```

{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/article/6378/
```

### 返回结果

```json
{
    "id": 6378,
    "name": "c4",
    "title": "test_title",
    "description": "test",
    "image": {
        "src": "test.jpg",
        "metafield": {}
    },
    "published": true,
    "published_at": "2016-12-07T08:12:55.281588Z",
    "body_html": "test",
    "order_by": "",
    "smart": false,
    "disjunctive": false,
    "created_at": "2017-09-06T07:04:22.805606Z",
    "updated_at": "2017-09-06T07:04:22.805631Z",
    "meta_title": "test",
    "meta_description": "test",
    "meta_image": "test.jpg"
}
```

{% endmethod %}


{% method -%}

## 查询博客标签

```
GET /api/shopfront/article_tag/
```



{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/article_tag/
```

###返回结果

```json
{
  "count": 6,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 92,
      "title": "1"
    },
    {
      "id": 99,
      "title": "a"
    },
    {
      "id": 2,
      "title": "aa"
    },
    {
      "id": 248,
      "title": "b"
    },
    {
      "id": 27,
      "title": "拉拉"
    },
    {
      "id": 755,
      "title": "测试"
    }
  ]
}
```
{% endmethod %}


{% method -%}
## 查询特定标签的博客列表

```
GET /api/shopfront/article/
```

### Query 参数

```
tag: tagTitle
id__in: 1002,1005
name: test
name__in: test1,test2
title: title1
tag: tag1
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/article_tag/?tag=测试标签
```

###返回结果

```json
{
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 64,
      "name": "aaa",
      "title": "啊啊啊",
      "author": "就不告诉你",
      "description": "...",
      "image": {
        "src": "https://up.img.heidiancdn.com/o_1ap2qni8r4j61bp1bsk1nr5niu0IMG_0653.jpg?imageMogr2/crop/!521.8461538461538x560a207.6923076923075a216. 9230769230769",
        "metafield": {}
      },
      "tags": [
        "测试标签"
      ],
      "body_html": "<p><img title=\"IMG_0653.jpg\" src=\"http://up.img.heidiancdn.com/o_1apuqjvgen5c1k1l163nkok1u4i0undefined\" /></p>",
      "published": true,
      "published_at": "2017-02-19T15:28:23.349790+08:00",
      "created_at": "2016-07-28T10:13:23+08:00",
      "updated_at": "2017-02-15T08:52:30.914733+08:00",
      "meta_title": "啊啊啊",
      "meta_description": "...",
      "meta_image": "https://up.img.heidiancdn.com/o_1ap2qni8r4j61bp1bsk1nr5niu0IMG_0653.jpg?imageMogr2/crop/!521.8461538461538x560a207.  6923076923075a216.9230769230769"
    }
  ]
}
```

{% endmethod %}

