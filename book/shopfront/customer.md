# 登录

`新用户第一次登录即为注册账号`；如果登录方式不是`验证码登录` 或者 `小程序授权 getPhoneNumber 登录`之一的话，需要**绑定手机号**；

### `grant_type` 说明：
|      grant_type 值   |      说明       |
|      :------   |    :-------    |
|    code        |  验证码登录，请求参数里 code 必填 |
|    password        |  密码登录，适用于已注册且设置过密码的用户，请求参数里 password 必填 |
|    wechat_mp        |  微信公众号登录 |
|    wechat_mini        |  微信小程序登录，需要调用 wx.login 获取到 code，作为请求参数之一  |
|    wechat_mini_phone        |  微信小程序授权获取手机号登录，需要调用 wx.login 以及 getPhoneNumber，获取到 code/encryptedData/iv, 作为请求参数之一 |

## 验证码登录

{% method -%}
### 获取验证码

```
POST /api/customers/send_code/
```
#### 数据格式

```json
{
  "mobile": "13412345678"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/send_code/
PAYLOAD {
  "mobile": "13412345678"
}
```

### 返回结果
状态码 `204`， 验证码短信发送到该手机

{% endmethod %}

{% method -%}
### 验证码登录

> 该方式适用于所有用户

```
POST /api/customers/login/
```
#### 数据格式

```json
{
  "mobile": [手机号],
  "code": [验证码],  // 接收到的短信验证码，一般为四位数字
  "grant_type": "code",  // 该字段可选，因为请求体里有 code 字段
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/login/
PAYLOAD {
  "mobile": "13412345678",
  "code": "9527"
}
```

### 返回结果
```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```

{% endmethod %}

## 密码登录

{% method -%}

```
POST /api/customers/login/
```
#### 数据格式

```json
{
  "mobile": [手机号],
  "password": [账户密码],
  "grant_type": "password",  // 该字段可选，因为请求体里有 password 字段
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/customers/login/
PAYLOAD {
  "mobile": "13412345678",
  "password": "passWord"
}
```

### 返回结果
```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```

{% endmethod %}

{% method -%}
## 小程序快捷登录(授权 wx.getPhoneNumber 登录)

> 该方式适用于所有用户

```
GET /api/customers/login/
```

### 数据格式
```json
{
  appid: [小程序 appid],
  js_code: [wx.login 接口获取到的 code],  // 建议每次登录前获取新的 code
  encryptedData: encryptedData, // 必填
  iv: iv, // 必填
  grant_type: "wechat_mini_phone"  // 该字段必填
}
```
`encryptedData` 和 `iv` 都是通过 `open-type="getPhoneNumber"` 组件获取到的返回值，具体参见 [微信公众平台：获取手机号](https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/getPhoneNumber.html)

{% sample lang="http" -%}

### 发送请求

```
POST /api/customers/login/
PAYLOAD {
  "appid": "wx1234567891112131",
  "js_code": "081RyJ123456788011119875",
  "encryptedData": "enk+DCo5n8atOlZeYYgkBuesbUn2Gyg4FrLidgQd5SimHUc4pzpm8UVD2F1l**********ye9DTDrp8kx25p3vUuPaEyi4ypWt3f0SD6lcbzu6lW53KP+Ul1grLD3IevTPLFSK13lZcN/zscSkzl1XkOOxHF+6Z++How6wRpzalxJdbgG0+mRz2l7vZX**********gDv9o+1Ks1Pw==",
  "iv": "CPhIvdZN9+zp123456780G9Yw==",
  "grant_type": "wechat_mini_phone"
}
```

### 返回结果
```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```

{% endmethod %}

{% method -%}
## 微信公众号登录

```
GET /api/customers/login/
```
`state_key`: 用户访问[微信网页授权](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html)，确认授权后重定向的url的query 参数里的 state 参数值。
### 数据格式

```json
{
  "state_key": [state_key],
  "grant_type": "wechat_mp"  // 该字段必填
}
```
{% sample lang="http" -%}
### 发送请求

```
POST /api/customers/login/
PAYLOAD {
  "state_key": "12345678",
  "grant_type": "wechat_mp"
}
```

### 返回结果

##### 情形1: 未绑定手机号

返回400错误，提示 `Wechat login requires mobile number`, 意味着需要进行一次`绑定手机号`的操作。
```json
{
  "detail": "Wechat login requires mobile number",
}
```

##### 情形2: 已绑定手过机号，正常返回登录成功的 token 等信息

```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```
{% endmethod %}


{% method -%}
## 微信小程序登录

```
GET /api/customers/login/
```
`state_key`: 用户访问[微信网页授权](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html)，确认授权后重定向的url的query 参数里的 state 参数值。
### 数据格式

```json
{
  appid: [小程序 appid],
  js_code: [wx.login 接口获取到的 code],  // 建议每次登录前获取新的 code
  grant_type: "wechat_mini"  // 该字段必填
}
```
{% sample lang="http" -%}
### 发送请求

```
POST /api/customers/login/
PAYLOAD {
  "appid": "wx1234567891112131",
  "js_code": "081RyJ123456788011119875",
  "grant_type": "wechat_mini"
}
```

### 返回结果

##### 情形1: 未绑定手机号

返回400错误，提示 `Wechat login requires mobile number`, 意味着需要进行一次`绑定手机号`的操作。
```json
{
  "detail": "Wechat login requires mobile number",
}
```

##### 情形2: 已绑定手过机号，正常返回登录成功的 token 等信息

```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```
{% endmethod %}


{% method -%}
## 绑定手机号

```
POST /api/customers/login/
```

### 参数说明

|      参数   |      说明       |
|      :------   |    :-------    |
|    mobile       |  用户绑定的`手机号` |
|    code         |  用户绑定的手机号接收到的`短信验证码` |
|    grant_type   |  `wechat_mp` 或者  `wechat_mini` 之一  |
|    state_key    |  微信公众号登录授权后，重定向的url 里的 state 参数，在 `grant_type: "wechat_mp" 时必填` |
|    appid        |  微信小程序 appid, 在 `grant_type: "wechat_mini" 时必填` |
|    js_code      |  微信小程序通过 wx.login 获取到的 code, 在 `grant_type: "wechat_mini" 时必填` |


{% sample lang="http" -%}
### 发送请求

##### 小程序快捷登录 `grant_type: wechat_mini`

```
POST /api/customers/login/
PAYLOAD {
  "mobile": "13412345678",
  "code": "1234",
  "appid": "wx1234567891112131",
  "js_code": "081RyJ123456788011119875",
  "grant_type": "wechat_mini"
}
```

##### 微信公众号授权登录 `grant_type: wechat_mp`

```
POST /api/customers/login/
PAYLOAD {
  "mobile": "13412345678",
  "code": "1234",
  "state_key": "12345678",
  "grant_type": "wechat_mp"
}
```

### 返回结果

```json
{
  "id": 1234,
  "mobile": "13412345678",
  "token": "8bbc98765155e7881339bad014c4c1cbc1123444"  // 这个就是 Customer token
}
```

{% endmethod %}


{% method -%}
## 退出登录

`清除本地缓存的 Customer token 即可`

{% endmethod %}