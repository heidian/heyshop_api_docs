# 商品


{% method -%}

## 查询商品列表

```
GET /api/shopfront/product/
```


### Query 参数

```
id: 1002
id__in: 1002,1005
name: shoe
name__in: shoe,shoe2
voucherproduct__is_enabled: true  // 是否为代金券商品
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
published: true
title: 测试商品鞋
price__gte: 10.00
price__lte: 22.55
collection: 22
collection__exclude: 22
category: 5
category__in: 5,6,7,10
coupons: 28
coupons__exclude: 28
q: 搜索关键词
order_by: id (id / created_at / name / inventory_quantity / title / price / published_at)
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/product/?page=1
```

### 返回结果

```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 575,
      "name": "datuxiaoshangp",
      "title": "大图小商品"

      /*
       * 其他字段参见下方的获取商品接口
       */

    },
    {
      "id": 529,
      "name": "shoe",
      "title": "一双很好看的鞋子！"
    }
  ]
}
```

{% endmethod %}


{% method -%}

## 查询指定商品

```
GET /api/shopfront/product/[productId]/
```


###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    category_ids        | int型，由卖家创建商品和商品规则后生成   |
|    category_titles        | string型，由卖家创建商品和商品规则后生成   |
|    price        | int型，第一个 SKU 的价格   |
|    compare_at_price        | int型，第一个 SKU 的原价，值可以为空   |
|    inventory_quantity		|	int型，所有 SKU 的库存总和     |
|    inventory_policy		|	缺货是否可以继续购买 continue: 是，deny: 否 |
|	  published			   |	发布状态，发布后的商品才可以在店铺中看到   |
|	  options			|	细分属性，用于区分 SKU  |
|	  variants		|	细分商品，即该商品下的 SKU  |


{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/product/529/
```

### 返回结果


```json
{
  "id": 529,
  "name": "shoe",
  "title": "test",
  "body_html": "test",
  "vendor": "",
  "image": "http://up.img.heidiancdn.com/o_1ar8usd4t139mks15imb82r6d0IMG_8613.JPG",

  "category_ids": [
    550,
    553
  ],

  "category_titles": [
    "男鞋",
    "男装"
  ],

  "price": 10,

  "compare_at_price": null,

  "inventory_quantity": 38,
  "requires_shipping": true,

  "inventory_policy": "deny",

  "published": true,

  "published_at": "2016-08-25T05:26:59Z",
  "created_at": "2016-08-25T05:26:59Z",
  "updated_at": "2016-11-15T17:23:57.739528Z",
  "metafield": {},
  "tags": [],

  "options": [
    {
      "id": 331,

      "title": "颜色",

      "values": [
        "白色",
        "红色"
      ]
    },
    {
      "id": 332,
      "title": "尺码",
      "values": [
        "大号",
        "小号"
      ]
    }
  ],

  "variants": [
    {
      "id": 787,
      "barcode": "",

      "sku": "A9938",
      "title": "颜色: 红色, 尺码: 大号",
      "position": 1,

      "price": "10.00",

     "compare_at_price": null,

      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-08-25T05:26:59Z",

     "options": [
        {
          "title": "颜色",
          "value": "红色"
        },
        {
          "title": "尺码",
          "value": "大号"
        }
      ],

      "inventory_policy": "deny",

      "inventory_quantity": 20
    },
    {
      "id": 788,
      "barcode": "",
      "sku": "A39C50",
      "title": "颜色: 白色, 尺码: 小号",
      "position": 2,
      "price": "11.00",
      "compare_at_price": null,
      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-08-25T05:26:59Z",
      "options": [
        {
          "title": "颜色",
          "value": "白色"
        },
        {
          "title": "尺码",
          "value": "小号"
        }
      ],
      "inventory_policy": "deny",
      "inventory_quantity": 10
    },
    {
      "id": 789,
      "barcode": "",
      "sku": "BEDD70",
      "title": "颜色: 白色, 尺码: 大号",
      "position": 3,
      "price": "12.00",
      "compare_at_price": null,
      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-10-28T09:21:08.029140Z",
      "options": [
        {
          "title": "颜色",
          "value": "白色"
        },
        {
          "title": "尺码",
          "value": "大号"
        }
      ],
      "inventory_policy": "deny",
      "inventory_quantity": 8
    }
  ],
  "images": [
    {
      "id": 1676,
      "src": "http://up.img.heidiancdn.com/o_1ar8usd4t139mks15imb82r6d0IMG_8613.JPG",
      "position": 1,
      "metafield": {}
    },
    {
      "id": 1677,
      "src": "http://up.img.heidiancdn.com/o_1ap2b9eo9ghoum6tqm1rf8vr60粉色眼影.jpg",
      "position": 2,
      "metafield": {}
    }
  ],

  "voucher_product": {
      "id": 1,
      "product_id": 529,
      "is_enabled": false,
      "white_product_ids": [],
      "black_product_ids": [],
      "expire_days": 0
  }
}
```

{% endmethod %}



{% method -%}

##查询供应商列表

```
GET /api/shopfront/vendor/
```


### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/vendor/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 625,
            "title": "test"
        },
        {
            "id": 626,
            "title": "test2"
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定供应商

```
GET /api/shopfront/vendor/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/vendor/625/
```

###返回结果

```json
{
    "id": 625,
    "title": "test"
}
```

{% endmethod %}





{% method -%}

##查询已上架商品标签列表

```
GET /api/shopfront/product_tag/
```


### Query 参数

```
title: tag_name
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/product_tag/?title=a
```

###返回结果

```json
[
    {
        "id": 14,
        "title": "a"
    },
    {
        "id": 172,
        "title": "yeah!"
    }
]
```

{% endmethod %}



{% method -%}

##查询指定可用标签

```
GET /api/shopfront/product_tag/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/tag/14/
```

###返回结果

```json
{
    "id": 14,
    "title": "a"
}
```

{% endmethod %}

