# 专题


{% method -%}

## 查询专题列表

```
GET /api/shopfront/collection/
```

### Query 参数

```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    smart        |  智能专题，是否使用 rule 匹配 product，boolean型  |

{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/collection/?page=1
```

### 返回结果

```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 10,
            "name": "c1",
            "title": "test_title",
            "description": "test_content",
            "image": {
                "src": "test.jpg",
                "metafield": {}
            },
            "published": true,
            "published_at": "2016-12-07T08:12:55.226141Z",
            "body_html": "test_content",
            "order_by": "",
            "smart": false,
            "disjunctive": false,
            "rules": [],
            "metafield": {
                "description": "test_content"
            },
            "created_at": "2017-09-06T07:04:22.689358Z",
            "updated_at": "2017-09-06T07:04:22.689385Z",
            "meta_title": "test_meta_title",
            "meta_description": "test_content",
            "meta_image": "test.jpg",
            "is_running": true
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

## 查询指定专题

```
GET /api/shopfront/collection/[id]/
```

{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/collection/6378/
```

### 返回结果

```json
{
    "id": 6378,
    "name": "c4",
    "title": "test_title",
    "description": "test",
    "image": {
        "src": "test.jpg",
        "metafield": {}
    },
    "published": true,
    "published_at": "2016-12-07T08:12:55.281588Z",
    "body_html": "test",
    "order_by": "",
    "smart": false,
    "disjunctive": false,
    "created_at": "2017-09-06T07:04:22.805606Z",
    "updated_at": "2017-09-06T07:04:22.805631Z",
    "meta_title": "test",
    "meta_description": "test",
    "meta_image": "test.jpg"
}
```

{% endmethod %}


{% method -%}

## 查询专题商品

```
GET /api/shopfront/product/
```

### Query 参数

```
collection: 22
```

###部分字段说明（请参考`商品`接口）

|       字段     |      说明       |
|      :------   |    :-------    |
|    fields        | 指定结果返回的字段   |


{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/product/?collection=22
```

###返回结果

```json
{
	"count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 13887,
            "product": {
                "id": 16511,
                "name": "p1",
                "title": "test",
                "description": "test",
                "body_html": "test",
                "body_html_mobile": "",
                "vendor": null,
                "image": {
                    "id": 62345,
                    "src": "test",
                    "position": 1,
                    "metafield": {}
                },
                "category_ids": [
                    17568,
                    17569
                ],
                "category_titles": [
                    "test1",
                    "tes2"
                ],
                "price": "10.00",
                "compare_at_price": null,
                "inventory_quantity": 100,
                "requires_shipping": true,
                "inventory_policy": "deny",
                "published": true,
                "published_at": "2016-12-07T08:12:54.777101Z",
                "created_at": "2017-09-06T07:04:20.136370Z",
                "updated_at": "2017-09-06T07:04:20.136398Z",
                "metafield": {},
                "meta_title": "test",
                "meta_description": "test",
                "meta_image": "test",
                "tags": [],
                "options": [],
                "variants": [
                    {
                        "id": 30619,
                        "barcode": "",
                        "sku": "S10001",
                        "title": "",
                        "position": 1,
                        "price": "10.00",
                        "compare_at_price": null,
                        "grams": "0.00",
                        "image": {
                            "id": 62345,
                            "src": "test.jpg",
                            "position": 1,
                            "metafield": {}
                        },
                        "productimage": null,
                        "options": [],
                        "inventory_policy": "deny",
                        "requires_shipping": true,
                        "inventory_quantity": 100,
                        "old_inventory_quantity": 0,
                        "inventory_quantity_adjustment": 0,
                        "metafield": {},
                        "created_at": "2017-09-06T07:04:20.395431Z",
                        "updated_at": "2017-09-06T07:04:20.395483Z",
                        "product_id": 16511
                    }
                ],
                "images": [
                    {
                        "id": 62345,
                        "src": "test.jpg",
                        "position": 1,
                        "metafield": {}
                    },
                    ...
                ],
                "sold_quantity": 0,
                "shipping_method_group_id": null,
                "voucher_product": null
            },
            "featured": false,
            "position": 0,
            "created_at": "2017-09-08T04:16:09.776825Z",
            "updated_at": "2017-09-08T04:16:09.776878Z"
        },
        ...
        ]
}
```
{% endmethod %}

