# 商品评论

{% method -%}

## 查询商品评论

```
GET /api/shopfront/review/
```

### Query 参数

```
product: [商品ID]
```
或者
```
reply_to: [评论ID]
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
| customer | 评论创建者 |
| reply_to_id | 当前评论是回复哪条评论的, 对应 query 参数里的 reply_to |
| content | 评论内容，文本格式 |
| rating | 评分，自然数 |
| images | 评论图片，列表 |
| replies | 当前评论的回复，count是 回复数量，results 是只包含第一条回复的数组 |

{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/review/?product=112
```

### 返回结果

```json
{
  count: 1,
  previous: null,
  next: null,
  results: [
    {
      "id": 152,
      "customer": {
        "avatar": "",
        "full_name": "Someone",
        "mobile": "130****1234"
      },
      "reply_to_id": null,
      "stickied": false,
      "content": "好评",
      "rating": 0,
      "images": [],
      "created_at": "2021-04-07T19:05:10.989506+08:00",
      "updated_at": "2021-04-07T19:05:10.989547+08:00",
      "replies": {
        "count": 0,
        "results": []
      }
    }
  ]
}
```
{% endmethod %}