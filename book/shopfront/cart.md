# 购物车

## 关于购物车 cartToken 说明（适用于以下本文里的API）
```
headers: {
  'X-Heidian-Cart-Token': cartToken
}
```
1. 如果没有指定 cartToken ，则请求每一次都创建或返回一个具有新的 `cartToken` 的购物车；
2. 如果指定了请求头属性 `X-Heidian-Cart-Token`, 则会针对到服务器根据 cartToken 匹配到的购物车进行操作；


{% method -%}

## 获取购物车详情

```
GET /api/shopfront/cart/
```

**如果添加 Authorization: customertoken xxxxxxx 的 Request Header，则会返回该账户当前有效的购物车条目详情和 cartToken；**

{% sample lang="http" -%}

### 发送请求

```
GET /api/shopfront/cart/

headers: {
  'X-Heidian-Cart-Token': 4a2851b0d5fd489654c0a99d6b55935236f9e375
}
```

### 返回结果
```json
{
  "token":"4a2851b0d5fd489654c0a99d6b55935236f9e375",
  "total_price":"100.00",
  "items":[{
    "total_price":"100.00",
    "variant":{
      "id":938984,
      "sku":"SKU0001",
      "barcode":"",
      "title":"颜色: red",
      "image":{
        "src":"https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
        "metafield":{"height":318,"width":318,"mimeType":"image/svg+xml","size":19831},
        "id":101356
      },
      "price":"100.00",
      "grams":"0.00",
      "options":[{"title":"颜色","value":"red"}]},
      "product":{
        "id":25115,
        "name":"1111test",
        "image":{
          "src":"https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield":{
            "height":318,
            "width":318,
            "mimeType":"image/svg+xml",
            "size":19831
          },
          "id":101356
        },
        "title":"1111test",
        "description":"这是一段描述"
      },
      "quantity":1,
      "checked":true,
      "attributes":{},
      "id":938984
    }
  ],
  "note":""
}
```
{% endmethod %}

{% method -%}
## 加入商品到购物车 或 设置购物车商品数量

```
POST /api/shopfront/cart/
```
### 数据格式
```json
{
  variant_id: variant.id,
  quantity: [数量],
  attributes: {} // 可选参数，可以对购物车条目做一些备注等
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/shopfront/cart/

PAYLOAD {
  varint_id: 12695,
  quantity: 3,
  attributes: {
    "note": "这是个活动商品"
  }
}

HEADERS: {
  'X-Heidian-Cart-Token': '4a2851b0d5fd489654c0a99d6b55935236f9e375'
}
```

### 返回结果
```json
{
  "token": "4a2851b0d5fd489654c0a99d6b55935236f9e375",
  "total_price": "100.01",
  "items": [
    {
      "total_price": "0.01",
      "variant": {
        "id": 12695,
        "sku": "DC771C",
        "barcode": "",
        "title": "名称: 50.00 元代金券",
        "image": {
          "src": "",
          "metafield": {}
        },
        "price": "0.01",
        "grams": "0.00",
        "options": [
          {
            "title": "名称",
            "value": "50.00 元代金券"
          }
        ]
      },
      "product": {
        "id": 7574,
        "name": "FD1B6B",
        "image": {
          "src": "",
          "metafield": {}
        },
        "title": "aasaaa",
        "description": ""
      },
      "quantity": 1,
      "checked": true,
      "attributes": {
        "note": "这是个活动商品"
      },
      "id": 12695
    },
    {
      "total_price": "100.00",
      "variant": {
        "id": 938984,
        "sku": "SKU0001",
        "barcode": "",
        "title": "颜色: red",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "price": "100.00",
        "grams": "0.00",
        "options": [
          {
            "title": "颜色",
            "value": "red"
          }
        ]
      },
      "product": {
        "id": 25115,
        "name": "1111test",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "title": "1111test",
        "description": "这是一段描述"
      },
      "quantity": 1,
      "checked": true,
      "attributes": {},
      "id": 938984
    }
  ],
  "note": ""
}
```
{% endmethod %}

{% method -%}
## 删除购物车条目

`quantity 设置为 0, 可将商品从购物车中删除`

```
POST /api/shopfront/cart/
```
### 数据格式
```json
{
  variant_id: variant.id,
  quantity: 0,  // 这里 quantity 设置为 0，即可从购物车中删除
  attributes: {}
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/shopfront/cart/

PAYLOAD {
  varint_id: 12695,
  quantity: 0
}

HEADERS: {
  'X-Heidian-Cart-Token': '4a2851b0d5fd489654c0a99d6b55935236f9e375'
}
```

### 返回结果
```json
{
  "token": "4a2851b0d5fd489654c0a99d6b55935236f9e375",
  "total_price": "100.00",
  "items": [
    {
      "total_price": "100.00",
      "variant": {
        "id": 938984,
        "sku": "SKU0001",
        "barcode": "",
        "title": "颜色: red",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "price": "100.00",
        "grams": "0.00",
        "options": [
          {
            "title": "颜色",
            "value": "red"
          }
        ]
      },
      "product": {
        "id": 25115,
        "name": "1111test",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "title": "1111test",
        "description": "这是一段描述"
      },
      "quantity": 1,
      "checked": true,
      "attributes": {},
      "id": 938984
    }
  ],
  "note": ""
}
```
{% endmethod %}


{% method -%}
## 设置购物车条目勾选状态

```
POST /api/shopfront/cart/
```
### 数据格式
```json
{
  variant_id: variant.id,
  checked: [true or false]
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/shopfront/cart/

PAYLOAD {
  varint_id: 12695,
  checked: false
}

HEADERS: {
  'X-Heidian-Cart-Token': '4a2851b0d5fd489654c0a99d6b55935236f9e375'
}
```

### 返回结果
```json
{
  "token": "4a2851b0d5fd489654c0a99d6b55935236f9e375",
  "total_price": "100.00",
  "items": [
    {
      "total_price": "0.01",
      "variant": {
        "id": 12695,
        "sku": "DC771C",
        "barcode": "",
        "title": "名称: 50.00 元代金券",
        "image": {
          "src": "",
          "metafield": {}
        },
        "price": "0.01",
        "grams": "0.00",
        "options": [
          {
            "title": "名称",
            "value": "50.00 元代金券"
          }
        ]
      },
      "product": {
        "id": 7574,
        "name": "FD1B6B",
        "image": {
          "src": "",
          "metafield": {}
        },
        "title": "aasaaa",
        "description": ""
      },
      "quantity": 1,
      "checked": false,
      "attributes": {},
      "id": 12695
    },
    {
      "total_price": "100.00",
      "variant": {
        "id": 938984,
        "sku": "SKU0001",
        "barcode": "",
        "title": "颜色: red",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "price": "100.00",
        "grams": "0.00",
        "options": [
          {
            "title": "颜色",
            "value": "red"
          }
        ]
      },
      "product": {
        "id": 25115,
        "name": "1111test",
        "image": {
          "src": "https://up.img.heidiancdn.com/o_1b8bum8r5avm1vp91o5e3p71ovg01.svg",
          "metafield": {
            "height": 318,
            "width": 318,
            "mimeType": "image/svg+xml",
            "size": 19831
          },
          "id": 101356
        },
        "title": "1111test",
        "description": "这是一段描述"
      },
      "quantity": 1,
      "checked": true,
      "attributes": {},
      "id": 938984
    }
  ],
  "note": ""
}
```
{% endmethod %}