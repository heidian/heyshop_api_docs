# 授权


## 1 申请应用

请联系我们工作人员，并提供以下资料。


<style>
table th:first-of-type {  
    width: 90px; 
}
</style>

|       提供信息          |      信息说明       | 
|      :---------------  |    :-------    |    
|    应用名称             |   第三方应用接受中文和英文名称   | 
|    回调地址             |   提供的回调地址将用于接收授权结果，称之为 `redirect_uri`   | 
|    应用所需权限        |   用户需要提供如下范围的权限： `read`:读取所有信息； 	`write`:写入说有信息；`catalogue:read`:读取商品目录；`catalogue:write`:写入商品目录；`orders:read`:读取订单信息；`orders:write`:写入订单信息；`shops:read`:读取店铺信息；`shops:write`:写入店铺信息   | 


通过审核后，我们会为您开通一个服务应用，并告知您该应用的 `client_id` 和 `client_secret`。

请记下该以上三个数据，之后会用到。


## 2 发起授权

假设要获取 `example.heidianer.com` 这家店的授权

授权链接，参考如下：
```
https://example.heidianer.com/api/oauth/authorize/?scope=read+write&state=123456&redirect_uri=http://yourdomain.com/login/heyshop/&response_type=code&client_id=NgM7pan9zTrd1Wgd0sMcwsxI69yxQrcO6iUb2QXP
```

参数说明：

`client_id ` 您应用的 client_id

`redirect_uri` 您应用的 redirect_uri，授权通过后会跳转至此地址

`response_type ` 始终为 "code"

`state ` 表示客户端的当前状态，可以指定任意值，我们服务器会原封不动地返回这个值。

`scope ` 表示申请的权限范围（每个权限以空格相隔）。

"read" 是针对所有资源的读取权限，"write" 是针对所有资源的写入权限，如果只需要得到部分模块的权限，则写作 "模块名:read"、"模块名:write" 。

例如订单模块的读取和写入的权限："order:read order:write"

以下是所有可提供的权限：
```
read
write
balance:read
balance:write
blogs:read
blogs:write
catalogue:read
catalogue:write
crm:read
crm:write
discounts:read
discounts:write
medias:read
medias:write
orders:read
orders:write
notifications:read
notifications:write
payments:read
payments:write
shippings:read
shippings:write
shops:read
shops:write
shopthemes:read
shopthemes:write
statistics:read
statistics:write
users:read
users:write
webhooks:read
webhooks:write
vouchers:read
vouchers:write
```



## 获取授权

example 店铺的管理者打开授权链接后，如点击同意授权按钮，将跳转至上述的 redirect_uri 地址，并携带 GET 参数如下：

`state` 之前发起授权时提交的 state 参数，原封不动返回

`code` 授权口令，用于接下来获取 access_token 


得到 code 后，请向服务器发起如下的 POST 请求
```
POST https://example.heidianer.com/api/oauth/token/?grant_type=authorization_code&code=xxx&redirect_uri=xxx
```

###Basic Auth:

在 HTTP 头部中添加

```
Authorization: Basic {base64(client_id:client_secret)}
```

url query 参数:

`code` 前面获取到的 code

`redirect_uri` 您应用的 redirect_uri

`grant_type` 始终为 "authorization_code"


返回结果如下：
```
{
  "access_token": "5azuebWcFg8cyUazWCbJDH9QvWp431",
  "scope": "read write",
  "refresh_token": "KFEtCAB8Sr2jZO9jIZ2tC634djLKR1",
  "expires_in": 2592000,  // 过期时间 单位为秒
  "token_type": "Bearer",
  "shop_id": 1203
}
```
请记录下此处的 `access_token` 和 `refresh_token`



## 调用 API

在 HTTP 头部中添加

```
Authorization: Bearer {access_token}
```

其中 `access_token` 中上述流程中获取



## 刷新 access_token

发起 POST 请求
```
POST https://client_id:client_secret@example.heidianer.com/api/oauth/token/?grant_type=refresh_token&refresh_token=xxx&redirect_uri=xxx
```

Basic Auth:
```
client_id:client_secret   (您应用的 client_id 以及 client_secret)
```

url query 参数:

`refresh_token` 中上述流程中获取

`redirect_uri` 您应用的 redirect_uri

`grant_type` 始终为 "refresh_token"

返回结果如下：
```
{
  "access_token": "5azuebWcFg8cyUazWCbJDH9QvWp431",
  "scope": "read write",
  "refresh_token": "KFEtCAB8Sr2jZO9jIZ2tC634djLKR1",
  "expires_in": 2592000,  // 过期时间 单位为秒
  "token_type": "Bearer",
  "shop_id": 1203
}
```

由此获取到一个新的 access_token



