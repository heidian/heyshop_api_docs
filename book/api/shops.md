#店铺

{% method -%}

##查询店铺信息

```
GET /api/shops/shop/
```

所需 scope: `shops:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/shop/?page=1
```

###返回结果

```json
{
    "id": 1,
    "name": "demo",
    "title": "demo",
    "slogan": "Hello, Heyshop!",
    "description": "This is a description for Demo",
    "logo": "https://up.img.heidiancdn.com/o_1bek0qh1ct741bq5rql1akfhpq0black-heys",
    "favicon": "https://up.img.heidiancdn.com/o_1bmjdlec319rimdqte62afg8l0icon3x.png",
    "domain": "demo.heidianer.com",
    "company": "上海市静安区泰兴路",
    "website": "http://www.heidianer.com",
    "address": "asdasda",
    "email": "xd@heidianer.com",
    "mobile": "135xxxx9595",
    "language": "zh",
    "customer_email": "xd@heidianer.com",
    "customer_phone": "135xxxxx595",
    "weixin_qr": "https://up.img.heidiancdn.com/o_1bf6673r61q6bhf680r1j0m1bih0qr-mammypi",
    "weibo_id": "",
    "beian_number": "",
    "version": 2411,
    "plan": {
        "id": 1,
        "name": "advanced",
        "period": "annual",
        "title": "高级 套餐  - 按年支付",
        "starts_at": "2017-05-19T00:00:00+08:00",
        "ends_at": "2018-04-02T00:00:00+08:00",
        "is_expired": false,
        "can_publish_products": -1,
        "days_left": 167,
        "active_billing": {
            "id": 452,
            "billing_type": "upgrade",
            "plan_name": "advanced",
            "plan_period": "annual",
            "title": "升级高级 套餐  - 按年支付",
            "amount": "2612.64",
            "starts_at": "2017-05-19T00:00:00+08:00",
            "ends_at": "2018-04-02T00:00:00+08:00",
            "status": "success",
            "order_number": "907620170519183315249177",
            "order_token": "36562d6be911ae455bd956da2de416d0439a33d8"
        },
        "next_billing": null
    },
    "sales_channels": {
        "wxapp": false,
        "online_store": {
            "theme": {
                "id": 5548,
                "title": "Pets - Rush+",
                "name": "rushplus--pets_c49dd9"
            }
        },
        "webapp": false,
        "pos": false,
        "buy_button": false
    },
    "metafield": {
        "icp": "AAAAAAAA"
    },
    "created_at": "2016-05-14T03:30:33+08:00",
    "updated_at": "2017-09-21T11:56:46.791201+08:00"
}
```

{% endmethod %}




{% method -%}

##查询店铺Ping++配置状态

```
POST /api/shops/shop/update_pingxx_partner_status/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/shop/update_pingxx_partner_status/
```

###返回结果

```json
{
    "is_bound": false,
    "weixin": {
        "partner_id": "hd1611092xxxxxxx2c99a0e5cfadf",
        "wx_key": "8da9c41fbxxxxxxx563576e43f6d1354",
        "identifier_id": "611",
        "wx_app_id": "wx3b0xxxxxfc3ab10"
    },
    "identifier_id": "611",
    "partner_id": "hd16110925a49e7d19e2c99a0e5cfadf",
    "is_signed": true,
    "enabled_channel": [
        "alipay",
        "wx",
        "wx_pub_qr",
        "alipay_qr",
        "alipay_wap",
        "alipay_pc_direct"
    ]
}
```

{% endmethod %}



{% method -%}

##重置店铺

```
POST /api/shops/shop/reset/
```

**此接口仅适用于创建完店铺的三小时内，超过三小时则无法重置店铺。**

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/shop/reset/
```

###返回结果

同查询店铺信息 返回完整的店铺信息

{% endmethod %}
