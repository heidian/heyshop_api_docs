{% method -%}

##重制密码

```
POST /api/customers/password/reset/
```

###数据格式

```json
{
	"mobile": "13916712923",
	"code": "123456",
	"new_passwird": "xxxxxxx"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/password/reset/
```

###返回结果

```json
http 204
```


{% endmethod %}