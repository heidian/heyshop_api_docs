{% method -%}

## 新用户注册

```
POST /api/customers/customer/
```

### Query 参数
```
mobile: '13412345678'
password: 'p@ssw1rd'		//短信验证码
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/customer/
```

### 返回结果

```json
{
  birthday: null,
  cart_token: null,
  email: "",
  full_name: "",
  gender: "unknown",
  id: 3733,
  mobile: "13412345678",
  token: "1234567890abdefg987654321"
}
```

{% endmethod %}