{% method -%}

## 快捷登录


### 1. 获取验证码

```
POST /api/customers/send_code/
```
#### 数据格式

```json
{
"mobile": "13412345678"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/send_code/
```

#### 返回结果

验证码发送到手机

{% endmethod %}
