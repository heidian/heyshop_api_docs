# 买家订单管理

本页接口均需要 Customer token 授权
即添加 Authorization: customertoken xxxxxxx 的 Request Header


{% method -%}

##获取买家订单列表

```
GET /api/customers/order/
```

### Query 参数

```
id: 1
id__in: 1,2,3
order_number: 902112331779
order_number__in: 902112331779,902112331780
order_status: open
order_status__in: open,closed,cancelled
financial_status: paid
financial_status__in: pending,partially_paid,paid
fulfillment_status: fulfilled
fulfillment_status__in: pending,partial,fulfilled
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/order/?page=1
```

### 返回结果

```json
{
    "count": 11,
    "next": "http://ywu18.heidianer.com/api/customers/order/?page=2",
    "previous": null,
    "results": [
        {
            "id": 23161,
            "order_number": "569720170911191554233211",
            "token": "f86a30b3fdaf0d627821478093d3e9eedd481a32",
            "cart_token": "834b137ac97cf739d95840cdb5f87cacf1bb2cff",
            "requires_shipping": true,
            "full_name": "测试",
            "email": "",
     /*
      * 订单其他字段参见下方的订单查询接口
      */
      	  },
      ...
      ]
}
```

{% endmethod %}



{% method -%}

##获取指定买家订单

```
GET /api/customers/order/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/order/23160/
```

###返回结果

```json
{
    "id": 23160,
    "order_number": "537920170911185616698736",
    "token": "df59a329b43ed5c303348dea7fa9072d3aadd174",
    "cart_token": "834b137ac97cf739d95840cdb5f87cacf1bb2cff",
    "requires_shipping": true,
    "full_name": "测试",
    "email": "",
    "mobile": "13916712923",
    "buyer_accepts_marketing": true,
    "total_line_items_price": "1.03",
    "total_discounts": "0.00",
    "shipping_cost": "0.00",
    "total_weight": "0.00",
    "total_price": "1.03",
    "order_status": "cancelled",
    "fulfillment_status": "pending",
    "financial_status": "refunded",
    "note": "",
    "created_at": "2017-09-11T10:56:16.701889Z",
    "updated_at": "2017-09-11T17:02:31.311780Z",
    "cancelled_at": "2017-09-11T17:02:31.305047Z",
    "cancel_reason": "Auto cancelled by system since all the payments are refunded",
    "closed_at": null,
    "referring_platform": "web",
    "customer": {
        "id": 11409,
        "mobile": "13916712923",
        "full_name": "测试",
        "gender": "unknown",
        "email": "",
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2017-09-11T07:14:52.940800Z",
        "updated_at": "2017-09-11T07:16:27.940411Z"
    },
    "shipping_address": {
        "full_name": "测试",
        "mobile": "13916712923",
        "address1": "xxx",
        "address2": "",
        "city": "市辖区",
        "district": "普陀区",
        "province": "上海市",
        "country": "",
        "place": {
        				...
        			},
        "lines": [
        {
            "id": 23813,
            "image": "https://up.img.heidiancdn.com/o_1b3cf0cq317l684p14051cs8ie80img11.jpg",
            "title": "标准床原木手作品 - 规格: 单床",
            "requires_shipping": true,
            "product_title": "标准床原木手作品",
            "variant_title": "规格: 单床",
            "quantity": 1,
            "price": "1.00",
            "total_price": "1.00",
            "total_discount": "0.00",
            "variant": {
                "id": 30620,
                "sku": "S30001",
                "title": "规格: 单床",
                "image": {
                    "src": "https://up.img.heidiancdn.com/o_1b3cf0cq317l684p14051cs8ie80img11.jpg",
                    "metafield": {}
                },
                "price": "1.00",
                "grams": "0.00",
                "options": [
                    {
                        "title": "规格",
                        "value": "单床"
                    }
                ]
            },
            "product": {
                "id": 16512,
                "name": "p3",
                "title": "标准床原木手作品"
            },
            "shipping_method_group_id": 1507,
            "selectable_shipping_methods": [
                {
                    "id": 3244,
                    "rate_type": "fixed",
                    "is_enabled": true,
                    "title": "快递_test",
                    "normal_price": "1.00",
                    "normal_unit": "1.00",
                    "over_price": "1.00",
                    "over_unit": "1.00",
                    "free_shipping_policy": "nope",
                    "free_shipping_price": "1.00",
                    "group_id": 1507,
                    "metafield": {},
                    "updated_at": "2017-09-11T07:17:29.316101Z",
                    "shipping_zones": [
                        {
                        ...
                        }
                        ],
                        ...

}
```

{% endmethod %}



{% method -%}

##更新买家订单

```
PATCH /api/customers/order/[id]/
```

###数据格式

```json
{
	"full_name":"test",
	"email":"test@test.com",
	"mobile":12345678901
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/customers/order/23160/
```

###返回结果

同获取指定买家订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##买家添加评论

```
POST /api/customers/order/[id]/add_comment/
```

###数据格式

```json
{
	"comment":"123test123"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/order/23160/add_comment/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##买家更新评论

```
PATCH /api/customers/order/[id]/update_comment/
```

###数据格式

```json
{
	"id": "16972",
	"comment":"test123123",
	"metafield":{}
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/customers/order/23160/update_comment/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##再次购买

```
POST /api/customers/order/[id]/buy_again/
```

	对象将会在购物车内生成，买家可在个人购物车内查看

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/order/23160/buy_again/
```

###返回结果

```json
{
    "id": 181205,
    "token": "834b137ac97cf739d95840cdb5f87cacf1bb2cff",
    "total_price": "10.00",
    "items": [
        {
            "id": 19084,
            "quantity": 10,
            "total_price": "10.00",
            "variant": {
                "id": 30620,
                "sku": "S30001",
                "title": "规格: 单床",
                "image": {
                    "src": "https://up.img.heidiancdn.com/o_1b3cf0cq317l684p14051cs8ie80img11.jpg",
                    "metafield": {}
                },
                "price": "1.00",
                "grams": "0.00",
                "options": [
                    {
                        "title": "规格",
                        "value": "单床"
                    }
                ]
            },
            "product": {
                "id": 16512,
                "name": "p3",
                "title": "标准床原木手作品"
            },
            "checked": true
        }
    ],
    "note": "",
    "created_at": "2017-09-08T07:12:33.415735Z",
    "updated_at": "2017-09-08T07:12:33.415792Z"
}
```

{% endmethod %}



{% method -%}

##申请退款

```
POST /api/customers/order/[id]/refund_request/
```

###数据格式

```json
{
	"request_reason":"test"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/order/23160/refund_request/
```

###返回结果

同获取指定买家订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##取消订单

```
POST /api/customers/order/[id]/cancel/
```

###数据格式

```json
{
	"reason":"test"
	//该接口只能在买家未付款且订单开启状态下可调用。
}
```


{% sample lang="http" -%}

###发送请求

```
POST /api/customers/order/23160/cancel/
```

###返回结果

同获取指定买家订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##获取买家订单详情（需要Order token）

```
GET /api/customers/ordertoken/[token]/
```

***该接口继承于order接口，包括更新订单，添加评论，更新评论，再次购买，申请退款，取消订单功能，用法与上述接口相同。***


{% sample lang="http" -%}

###发送请求

```
GET /api/customers/ordertoken/e82627a05484b1e054c29c5036cae0d44e5b4c22/
```

###返回结果

同获取指定买家订单接口 返回完整的订单信息

{% endmethod %}


{% method -%}

##查询买家收货地址

```
GET /api/customers/address/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/address/
```

###返回结果

```json
[
    {
        "id": 5965,
        "is_default": true,
        "address1": "xxx",
        "address2": "",
        "district": "普陀区",
        "city": "市辖区",
        "province": "上海市",
        "country": "",
        "full_name": "测试",
        "mobile": "13916712923",
        "place_code": "310107",
        "place": {
            "id": 866,
            "name": "普陀区",
            "code": "310107",
            "parent": 861,
            "area": "",
            "short_name": "普陀区",
            "pinyin": "PuTuoQu"
        }
    }
]
```

{% endmethod %}


{% method -%}

##查询指定买家收货地址

```
GET /api/customers/address/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/address/5965/
```

###返回结果

```json
[
    {
        "id": 5965,
        "is_default": true,
        "address1": "xxx",
        "address2": "",
        "district": "普陀区",
        "city": "市辖区",
        "province": "上海市",
        "country": "",
        "full_name": "测试",
        "mobile": "13916712923",
        "place_code": "310107",
        "place": {
            "id": 866,
            "name": "普陀区",
            "code": "310107",
            "parent": 861,
            "area": "",
            "short_name": "普陀区",
            "pinyin": "PuTuoQu"
        }
    }
]
```

{% endmethod %}



{% method -%}

##创建收货地址

```
POST /api/customers/address/
```

###数据格式

```json
    "address1": "test1",
    "address2": "test2",
    "district": "xxx",
    "city": "shanghai",
    "province": "xxx",
    "country": "xxx",
    "full_name": "xxx"
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/address/
```

###返回结果

```json
{
    "id": 6249,
    "is_default": false,
    "address1": "test1",
    "address2": "test2",
    "district": "xxx",
    "city": "shanghai",
    "province": "xxx",
    "country": "xxx",
    "full_name": "xxx",
    "mobile": "",
    "place_code": null,
    "place": null
}

```

{% endmethod %}


{% method -%}

##更新收货地址

```
PATCH /api/customers/address/[id]/
```

###数据格式

```json
{
	"address1": "123test1",
    "address2": "123test2",
    "district": "xxx",
    "city": "shanghai",
    "province": "xxx",
    "country": "xxx",
    "full_name": "xxx",
    "mobile": ""
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/customers/address/6249/
```

###返回结果

```json
{
    "id": 6249,
    "is_default": false,
    "address1": "123test1",
    "address2": "123test2",
    "district": "xxx",
    "city": "shanghai",
    "province": "xxx",
    "country": "xxx",
    "full_name": "xxx",
    "mobile": "",
    "place_code": null,
    "place": null
}
```

{% endmethod %}



{% method -%}

##删除收货地址

```
DELETE /api/customers/address/[id]/
```

{% sample lang="http" -%}

###发送请求

```
DELETE /api/customers/address/6249/
```

###返回结果

```json
http 204
```

{% endmethod %}