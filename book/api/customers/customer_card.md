#买家卡券管理

本页接口均需要 Customer token 授权
即添加 Authorization: customertoken xxxxxxx 的 Request Header

{% method -%}

##查询已获得优惠券

```
GET /api/customers/coupon_code/
```

### Query 参数

```
id: 1
id_in: 1,2,3
status: unused
status__in : unused,used
coupon__code_prefix: xx7GMABp
coupon__code_prefix_in: xx7GMABp,xx8GMABp
coupon_id: 101
coupon_id__in: 101,102,103
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/coupon_code/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 27499,
            "coupon": {
                "id": 986,
                "title": "test",
                "verbose_title": "10% off",
                "description": "",
                "token": "ad5cebd6da35f3df9c1a",
                "code_prefix": "iy7GMABp",
                "discount_type": "percentage",
                "value": "10.00",
                "minimum_amount": "0.00",
                "starts_at": "2017-10-08T16:00:00Z",
                "ends_at": null,
                "is_forever": true,
                "quantity": 100,
                "redeem_count": 1,
                "created_at": "2017-10-10T07:33:05.742052Z"
            },
            ...
}
```

{% endmethod %}



{% method -%}

##查询指定已获得优惠券

```
GET /api/customers/coupon_code/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/coupon_code/27500/
```

###返回结果

```json
{
    "id": 27500,
    "coupon": {
        "id": 987,
        "title": "test2",
        "verbose_title": "50% off",
        "description": "",
        "token": "032cd50ebb046f4a5778",
        "code_prefix": "jB7nNJPb",
        "discount_type": "percentage",
        "value": "50.00",
        "minimum_amount": "0.00",
        "starts_at": "2017-10-10T07:34:29Z",
        "ends_at": null,
        "is_forever": true,
        "quantity": 100,
        "redeem_count": 1,
        "created_at": "2017-10-10T07:34:43.314372Z"
    },
    "code": "jB7nNJPba60799",
    "status": "unused",
    "verbose_title": "50% off",
    "used_at": null
}
```

{% endmethod %}



{% method -%}

##批量查询优惠券信息(通过优惠口令码)

```
GET /api/customers/coupon/?code_prefix=[value]
```

**该接口中，参数是必须的，code_prefix为自定义优惠口令，请通过code_prefix或code_prefix_in进行查询**

###Query 参数

```
code_prefix: xx7GMABp
code_prefix__in: xx7GMABp,xx8GMABp
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/coupon/?code_prefix=jB7nNJPb
```

###返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 987,
            "code_prefix": "jB7nNJPb",
            "expired_days": 0,
            "can_redeem": true,
            "limit_reached": false,
            "title": "test2",
            "verbose_title": "打5折",
            "starts_at": "2017-10-10T15:34:29+08:00",
            "ends_at": null,
            "price_rule": {
                "once_per_customer": false,
                "target_selection": "all",
                "entitled_product_ids": null,
                "entitled_collection_ids": null,
                "entitled_category_ids": null,
                "entitled_vendor_ids": null,
                "target_type": "line_item",
                "allocation_method": "across",
                "value": "-50.00",
                "value_type": "percentage",
                "accumulate": false,
                "prerequisite_quantity_range": null,
                "prerequisite_shipping_price_range": null,
                "prerequisite_subtotal_range": null,
                "prerequisite_to_entitlement_quantity_ratio": null
            }
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定优惠券信息(通过优惠口令码)

```
GET /api/customers/coupon/[code_prefix]/
```

**该接口中，参数是必须的，code_prefix为自定义优惠口令，请通过code_prefix或code_prefix_in进行查询**

{% sample lang="http" -%}

###发送请求

```
GET /api/customers/coupon/xx7GMABp/
```

###返回结果

```json
{
    "id": 987,
    "code_prefix": "jB7nNJPb",
    "expired_days": 0,
    "can_redeem": true,
    "limit_reached": false,
    "title": "test2",
    "verbose_title": "打5折",
    "starts_at": "2017-10-10T15:34:29+08:00",
    "ends_at": null,
    "price_rule": {
        "once_per_customer": false,
        "target_selection": "all",
        "entitled_product_ids": null,
        "entitled_collection_ids": null,
        "entitled_category_ids": null,
        "entitled_vendor_ids": null,
        "target_type": "line_item",
        "allocation_method": "across",
        "value": "-50.00",
        "value_type": "percentage",
        "accumulate": false,
        "prerequisite_quantity_range": null,
        "prerequisite_shipping_price_range": null,
        "prerequisite_subtotal_range": null,
        "prerequisite_to_entitlement_quantity_ratio": null
    }
}
```

{% endmethod %}


{% method -%}

##兑换优惠券

```
POST /api/customers/coupon/[code_prefix]/redeem/
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/coupon/xx7GMABp/redeem/
```

###返回结果

```json
{
    "id": 423942,
    "code": "jB7nNJPba05609",
    "title": "test2",
    "verbose_title": "打5折",
    "starts_at": "2017-10-10T15:34:29+08:00",
    "ends_at": null,
    "coupon": {
        "code_prefix": "jB7nNJPb",
        "can_redeem": true
    },
    "price_rule": {
        "once_per_customer": false,
        "target_selection": "all",
        "entitled_product_ids": null,
        "entitled_collection_ids": null,
        "entitled_category_ids": null,
        "entitled_vendor_ids": null,
        "target_type": "line_item",
        "allocation_method": "across",
        "value": "-50.00",
        "value_type": "percentage",
        "accumulate": false,
        "prerequisite_quantity_range": null,
        "prerequisite_shipping_price_range": null,
        "prerequisite_subtotal_range": null,
        "prerequisite_to_entitlement_quantity_ratio": null
    },
    "status": "unused",
    "used_at": null,
    "created_at": "2019-07-05T18:08:26.742848+08:00"
}
```

{% endmethod %}



{% method -%}

##查询已获得礼品卡

```
GET /api/customers/voucher/
```

###Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送数据

```
GET /api/customers/voucher/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 350,
            "customer": {
                "id": 11409,
                "mobile": "13916712923",
                "full_name": "测试",
                "gender": "unknown",
                "email": "",
                "birthday": null,
                "note": "",
                "accepts_marketing": true,
                "created_at": "2017-09-11T07:14:52.940800Z",
                "updated_at": "2017-09-11T07:16:27.940411Z",
                "tags": [],
                "addresses": [
                    {
                        ...
                     },
                    {
                        "id": 6248,
                        "is_default": false,
                        "address1": "",
                        "address2": "",
                        "district": "",
                        "city": "",
                        "province": "",
                        "country": "",
                        "full_name": "",
                        "mobile": "",
                        "place_code": null,
                        "place": null
                    }
                ],
                "segments": [],
                "last_order_id": null,
                "last_order_paid_at": null,
                "abandoned_checkouts": 1,
                "orders": 11,
                "paid_orders": 5,
                "total_spent": "0.15",
                "average_order_price": "0.00",
                "visits": 0,
                "visits_per_day": "0.00",
                "customers_referred": 0,
                "total_spent_of_referred": "0.00"
            },
            "code": "ABC3AE42BEE5F706",
            "title": "Voucher",
            "total_balance": "100.00",
            "balance": "100.00",
            "status": "available",
            "white_product_ids": [],
            "black_product_ids": [],
            "minimum_amount": "0.00",
            "expire_days": 360,
            "activate_expires_at": "2017-10-10T08:32:38Z",
            "starts_at": "2017-10-10T08:32:46.450484Z",
            "ends_at": "2018-10-05T08:32:46.450484Z"
        },
        ...
        ]
}
```

{% endmethod %}



{% method -%}

##查询指定礼品卡

```
GET /api/customers/voucher/[id]/
```

{% sample lang="http" -%}

###发送数据

```
GET /api/customers/voucher/351/
```

###返回结果

```json
{
    "id": 351,
    "customer": {
        "id": 11409,
        "mobile": "13916712923",
        "full_name": "测试",
        "gender": "unknown",
        "email": "",
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2017-09-11T07:14:52.940800Z",
        "updated_at": "2017-09-11T07:16:27.940411Z",
        "tags": [],
        "addresses": [
            {
               ...
            },
            {
                "id": 6248,
                "is_default": false,
                "address1": "",
                "address2": "",
                "district": "",
                "city": "",
                "province": "",
                "country": "",
                "full_name": "",
                "mobile": "",
                "place_code": null,
                "place": null
            }
        ],
        "segments": [],
        "last_order_id": null,
        "last_order_paid_at": null,
        "abandoned_checkouts": 1,
        "orders": 11,
        "paid_orders": 5,
        "total_spent": "0.15",
        "average_order_price": "0.00",
        "visits": 0,
        "visits_per_day": "0.00",
        "customers_referred": 0,
        "total_spent_of_referred": "0.00"
    },
    "code": "17413D1F6538C87D",
    "title": "Voucher",
    "total_balance": "150.00",
    "balance": "150.00",
    "status": "available",
    "white_product_ids": [],
    "black_product_ids": [],
    "minimum_amount": "0.00",
    "expire_days": 360,
    "activate_expires_at": "2017-10-09T16:00:00Z",
    "starts_at": "2017-10-10T08:34:15.651439Z",
    "ends_at": "2018-10-05T08:34:15.651439Z"
}
```

{% endmethod %}



{% method -%}

##礼品卡发放总金额

```
GET /api/customers/voucher/sum_balance/
```

{% sample lang="http" -%}

###发送数据

```
GET /api/customers/voucher/sum_balance/
```

###返回结果

```json
{
    "sum_balance": 250
}
```

{% endmethod %}



{% method -%}

##查询礼品码

```
GET /api/customers/voucher/code/[code]/
```

	本接口中，code为礼品卡编号,

{% sample lang="http" -%}

###发送数据

```
GET /api/customers/voucher/code/ABC3AE42BEE5F706/
```

###返回结果

```json
{
    "id": 351,
    "customer": {
        "id": 11409,
        "mobile": "13916712923",
        "full_name": "测试",
        "gender": "unknown",
        "email": "",
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2017-09-11T07:14:52.940800Z",
        "updated_at": "2017-09-11T07:16:27.940411Z",
        "tags": [],
        "addresses": [
            {
               ...
            },
            {
                "id": 6248,
                "is_default": false,
                "address1": "",
                "address2": "",
                "district": "",
                "city": "",
                "province": "",
                "country": "",
                "full_name": "",
                "mobile": "",
                "place_code": null,
                "place": null
            }
        ],
        "segments": [],
        "last_order_id": null,
        "last_order_paid_at": null,
        "abandoned_checkouts": 1,
        "orders": 11,
        "paid_orders": 5,
        "total_spent": "0.15",
        "average_order_price": "0.00",
        "visits": 0,
        "visits_per_day": "0.00",
        "customers_referred": 0,
        "total_spent_of_referred": "0.00"
    },
    "code": "17413D1F6538C87D",
    "title": "Voucher",
    "total_balance": "150.00",
    "balance": "150.00",
    "status": "available",
    "white_product_ids": [],
    "black_product_ids": [],
    "minimum_amount": "0.00",
    "expire_days": 360,
    "activate_expires_at": "2017-10-09T16:00:00Z",
    "starts_at": "2017-10-10T08:34:15.651439Z",
    "ends_at": "2018-10-05T08:34:15.651439Z"
}
```

{% endmethod %}



{% method -%}

##激活礼品卡

```
POST /api/customers/voucher/code/[code]/activate/
```

	当用户购买礼品卡后，可在个人中心通过查看订单获取礼品卡编号

{% sample lang="http" -%}

###发送数据

```
POST /api/customers/voucher/code/ABC3AE42BEE5F706/activate/
```

###返回结果

```json
{
    "id": 352,
    "customer": {
        "id": 11409,
        "mobile": "13916712923",
        "full_name": "测试",
        "gender": "unknown",
        "email": "",
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2017-09-11T07:14:52.940800Z",
        "updated_at": "2017-09-11T07:16:27.940411Z",
        "tags": [],
        "addresses": [
            {
                ...
            },
            {
                "id": 6248,
                "is_default": false,
                "address1": "",
                "address2": "",
                "district": "",
                "city": "",
                "province": "",
                "country": "",
                "full_name": "",
                "mobile": "",
                "place_code": null,
                "place": null
            }
        ],
        "segments": [],
        "last_order_id": null,
        "last_order_paid_at": null,
        "abandoned_checkouts": 1,
        "orders": 11,
        "paid_orders": 5,
        "total_spent": "0.15",
        "average_order_price": "0.00",
        "visits": 0,
        "visits_per_day": "0.00",
        "customers_referred": 0,
        "total_spent_of_referred": "0.00"
    },
    "code": "9D2298DE7E125524",
    "title": "test111",
    "total_balance": "50.00",
    "balance": "50.00",
    "status": "available",
    "white_product_ids": [],
    "black_product_ids": [],
    "minimum_amount": "0.00",
    "expire_days": 0,
    "activate_expires_at": null,
    "starts_at": null,
    "ends_at": null
}
```

{% endmethod %}