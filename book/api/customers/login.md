{% method -%}

## 密码登录

```
POST /api/customers/login/
```

### 数据格式
```
mobile: '13412345678'
password: 'p@ssw1rd'		//短信验证码
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/login/
```

### 返回结果

```json
{
  token: "1234567890abdefg987654321"
}
```

{% endmethod %}