# API Endpoint


嘿店的每一家店铺都有独立的二级域名，所有的 API 都可以通过这个域名来访问，跟网址为：

```
https://[shopname].heidianer.com/api/
```

比如

```
https://example.heidianer.com/api/
```
