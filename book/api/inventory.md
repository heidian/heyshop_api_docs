# 库存

{% method -%}

## 增量更新库存

```
POST /api/inventory/inventory_level/adjust/
```

所需 scope: `inventory:write` 或 `write`

###部分字段说明

|       字段      |      说明       | 
|      :------   |    :-------    |    
|    inventory_item_id        |    库存商品id    |     
|    barcode    |    库存商品条形码    | 
|    sku        |    库存商品sku    |  
|    available_adjustment        |    调整量，正数则会在原库存上增加相应数量，负数则会在原库存上减去相应数量    | 
|    note        |    此次更新备注信息    | 


###数据格式  

```json
{
    "inventory_levels": [{
        "inventory_item_id": 100,
        "barcode": "000000000",
        "sku": "S0001",
        "available_adjustment": -1,
        "note": "损坏"
    }, {
        "inventory_item_id": 101,
        "barcode": "111111111",
        "sku": "S0002",
        "available_adjustment": 1,
        "note": "补货"
    }]
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/inventory/inventory_level/adjust/
```

### 返回结果

```json
{
    "inventory_levels": [{
        "inventory_item_id": 100,
        "barcode": "000000000",
        "sku": "S0001",
        "available_adjustment": -1,
        "note": "损坏"
    }, {
        "inventory_item_id": 101,
        "barcode": "111111111",
        "sku": "S0002",
        "available_adjustment": 1,
        "note": "补货"
    }]
}
```

{% endmethod %}



{% method -%}

## 覆盖更新库存

```
POST /api/inventory/inventory_level/set/
```

所需 scope: `inventory:write` 或 `write`

###部分字段说明

|       字段      |      说明       | 
|      :------   |    :-------    |    
|    inventory_item_id        |    库存商品id    |     
|    barcode    |    库存商品条形码    | 
|    sku        |    库存商品sku    |  
|    available        |    该数据会覆盖原有库存数量，修改前请仔细核对   | 
|    note        |    此次更新备注信息    | 

###数据格式  

```json
{
    "inventory_levels": [{
        "inventory_item_id": 123,
        "barcode": "000000000",
        "sku": "S0001",
        "available": 100,
        "note": "无"
    }, {
        "inventory_item_id": 234,
        "barcode": "111111111",
        "sku": "S0002",
        "available": 200,
        "note": "上新"
    }]
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/inventory/inventory_level/set/
```

### 返回结果

```json
{
    "inventory_levels": [{
        "inventory_item_id": 123,
        "barcode": "000000000",
        "sku": "S0001",
        "available": 100,
        "note": "无"
    }, {
        "inventory_item_id": 234,
        "barcode": "111111111",
        "sku": "S0002",
        "available": 200,
        "note": "上新"
    }]
}
```

{% endmethod %}
