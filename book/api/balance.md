#代收款提现

本接口仅适用于使用代收款服务的店主

{% method -%}

##查询在线付款订单

```
GET /api/balance/in/
```

所需 scope: `balance:read` 或 `read`

该接口只能查询通过在线付款，并且付款成功的订单，手动标记付款的订单并不会在此接口中显示

### Query 参数

```
created_at: 2017-03-31 00:00:00
created_at__gte: 2017-03-31 00:00:00
created_at__lte: 2017-03-31 00:00:00
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       | 
|      :------  |    :-------    |    
|    transaction_type        |  有`sale`(出售)和`refund`(退款)两种交易类型    |    
|    channel        |  交易平台，嘿店支持微信和支付宝支付    | 
|    status        |  支付状态：`pending`(待支付) `success`(支付成功) `failure`(支付失败) `error`(错误)    |  


{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/in/?page=1
```

###返回结果

```json
{
    "count": 6,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 8490,
            "transaction_type": "refund",
            "amount": "0.01",
            "channel": "alipay_pc_direct",
            "transaction_number": "74520170911191804304",
            "status": "success",
            "created_at": "2017-09-11T19:18:04.307909+08:00",
            "order": {
                "id": 23161,
                "image": "https://up.img.heidiancdn.com/o_1b3cf0cq1173vg6c9fabj1e30img9.jpg",
                "title": "皮质抱枕手作品"
            }
        },
        ...
     ]
}
```

{% endmethod %}



{% method -%}

##查询指定在线付款订单

```
GET /api/balance/in/[id]/
```

所需 scope: `balance:read` 或 `read`

{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/in/8459/
```

###返回结果

```json
{
    "id": 8459,
    "transaction_type": "sale",
    "amount": "0.02",
    "channel": "alipay_pc_direct",
    "transaction_number": "39020170911151806614",
    "status": "success",
    "created_at": "2017-09-11T15:18:06.617944+08:00",
    "order": {
        "id": 23128,
        "image": "https://up.img.heidiancdn.com/o_1b3cf0cq1173vg6c9fabj1e30img9.jpg",
        "title": "皮质抱枕手作品"
    }
}
```

{% endmethod %}



{% method -%}

##查询账户提现订单

```
GET /api/balance/out/
```

所需 scope: `balance:read` 或 `read`

## Query 参数

```
created_at: 2017-03-31 00:00:00
created_at__gte: 2017-03-31 00:00:00
created_at__lte: 2017-03-31 00:00:00
page: 1
page_size: 10
```

{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/out/?page=1
```

###返回结果

```json
{
    "count": 5,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 127,
            "request_money": "111.06",
            "charge_fee": 2.22,
            "final_money": "108.84",
            "full_name": "fasdfasdf",
            "alipay": "asdasdasd",
            "status": "failure",
            "reason": "no_money",
            "last_checked_at": "2017-05-12T17:15:07.640691+08:00",
            "created_at": "2017-05-05T11:28:15.758281+08:00"
        },
        ...
     ]
}
```

{% endmethod %}



{% method -%}

##查询指定提现订单

```
GET /api/balance/out/[id]/
```

所需 scope: `balance:read` 或 `read`

{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/out/27/
```

###返回结果

```json
{
    "id": 27,
    "request_money": "111.18",
    "charge_fee": 2.22,
    "final_money": "108.96",
    "full_name": "dondon",
    "alipay": "hadxxxzhy",
    "status": "failure",
    "reason": "no_money",
    "last_checked_at": "2016-12-04T23:26:00.972969+08:00",
    "created_at": "2016-11-29T10:45:07.183927+08:00"
}
```

{% endmethod %}



{% method -%}

##申请提现

```
POST /api/balance/out/
```

所需 scope: `balance:write` 或 `write`

{% sample lang="http" -%}

### 发送请求

```
POST /api/balance/out/
```

###返回结果

同查询指定提现订单接口 返回完整的提现信息

{% endmethod %}



{% method -%}

##查看账户余额

```
GET /api/balance/wallet/
```

所需 scope: `balance:read` 或 `read`

###部分字段说明

|       字段     |      说明       | 
|      :------   |    :-------    |    
|    balance        | 可体现余额，若金额小于10元，则无法提现    |    

{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/wallet/
```

###返回结果

```json
{
    "id": 1,
    "full_name": "test",
    "alipay": "test@gmail.com",
    "balance": "111.06"
}
```

{% endmethod %}