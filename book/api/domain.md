

# 域名

本页接口均需要 ShopAdminUser token 授权
即添加 Authorization: Token xxxxxxx 的 Request Header


{% method -%}

## 查看域名是否可购买

```
GET /api/domains/domain/check_allow/
```

### Query 参数

```
domainname: 'test123.com'
```

{% sample lang="http" -%}

###发送请求

```
GET /api/domains/domain/check_allow/?domainname:deom.heidianer.com
```

### 返回结果

```json
{
    "result": false,  //false表示该域名不可购买，true表示该域名可购买
    "pricing": [
        {
            "price": 39,
            "term": 1,
            "renew_price": 39
        },
        {
            "price": 117,
            "term": 3,
            "renew_price": 117
        },
        {
            "price": 195,
            "term": 5,
            "renew_price": 195
        },
        {
            "price": 390,
            "term": 10,
            "renew_price": 390
        }
    ],
    "recommends": [
        "test123123.shop",
        "test1232017.shop",
        "heytest123.shop"
    ]
}
```

{% endmethod %}



{% method -%}

## 域名绑定

```
POST /api/domains/domain/[id]/bind/
```

{% sample lang="http" -%}

###发送请求

```
POST /api/domains/domain/10/bind/
```

###返回结果

```json  
{
	"id": 10,   
    "domain": "test1234561",  
    "beian_number": 12345,  
    "is_default": "true",  
    "ssl": "false",  
    "ssl_cert": "true",  
    "ssl_cert_key": "true"
}
```




{% endmethod %}



{% method -%}

## 创建域名

```
POST /api/domains/domain/
```

### 数据格式
```json
{
    "domainname": "test123.com",
    "dom_org": 英文所有者（限4 - 80个字符），填对应的英文或拼音,
    "dom_fn": 英文名（限2-30个字符），例如注册者为张小宝，这里填：xiao bao,
    "dom_ln": 英文姓（限2-30个字符），例如注册者为张小宝，这里填：zhang,
    "dom_adr1": 英文地址,
    "dom_ct": 英文城市,
    "dom_st": 英文省,
    "dom_co": 国家代码,
    "dom_pc": 邮编,
    "dom_ph": 电话（格式例如：028-86263960或直接填手机号码13812345xxx）,
    "dom_fax": 传真（格式例如：028-86263960或直接填手机号码13812345xxx）,
    "dom_em": 电子邮件,
    "dom_org_m": 中文所有者信息,
    "dom_fn_m": 中文名,
    "dom_ln_m": 中文姓,
    "dom_adr_m": 中文地址,
    "dom_ct_m": 中文城市,
    "dom_st_m": 中文省
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/domains/domain/
```

### 返回结果

```json
{
    "id": 1,
    "domainname": "test1234561.test",
    "term": 1,
    "dom_org": "test",
    "dom_fn": "test",
    "dom_ln": "test",
    "dom_adr1": "testtesttest",
    "dom_ct": "test",
    "dom_st": "test",
    "dom_co": "cn",
    "dom_pc": "123456",
    "dom_ph": "13412345678",
    "dom_fax": "13412345678",
    "dom_em": "taojy123@163.com",
    "dom_org_m": "测试测试",
    "dom_fn_m": "测试",
    "dom_ln_m": "测试",
    "dom_adr_m": "上海市静安区xxx",
    "dom_ct_m": "上海",
    "dom_st_m": "上海",
    "order_number": "886420170606154741677677",  // 支付此订单 完成域名的买入 status 就变为 available
    "status": "unavailable", // unavailable, failure, available, expired
    "ends_at": null,
    "failure_msg": ""
}
```

{% endmethod %}



{% method -%}

## 查看域名列表

```
GET /api/domains/domain/
```

### Query 参数
```
status: unavailable / failure / available / expired
status__in: failure,expired
id: 123
id__in: 123,135
order_by: id / domainname / status
```

{% sample lang="http" -%}

###发送请求

```
GET /api/domains/domain/?page=1
```

### 返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "domainname": "test1234561.test",
            // ...
            // 参考 创建域名 返回结果
        }
     ...
    ]
}
```

{% endmethod %}



{% method -%}

## 查询已购买域名

```
GET /api/domains/domain/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/domains/domain/10/
```

###返回结果

```json
{
	"id": 10,
    "domainname": "test123456.test",
    "term": 10,
    "dom_org": "test",
    "dom_fn": "test",
    "dom_ln": "test",
    "dom_adr1": "testtesttest",
    "dom_ct": "test",
    "dom_st": "test",
    "dom_co": "cn",
    "dom_pc": "123456",
    "dom_ph": "13412345678",
    "dom_fax": "13412345678",
    "dom_em": "taojy123@163.com",
    "dom_org_m": "测试测试",
    "dom_fn_m": "测试",
    "dom_ln_m": "测试",
    "dom_adr_m": "上海市静安区xxx",
    "dom_ct_m": "上海",
    "dom_st_m": "上海",
    "order_number": "886420170606154741677677",  // 支付此订单 完成域名的买入 status 就变为 available
    "status": "unavailable", // unavailable, failure, available, expired
    "ends_at": null,
    "failure_msg": ""
}
```


{% endmethod %}



{% method -%}

##域名续费列表查询

```
GET /api/domains/domain/[id]/domain_renew/
```

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求 

```
GET /api/domains/domain/10/domain_renew/?page=1
```

###返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id"： 10,    
            "domain_id"： 12345,  
            "term"： "test_term",  
            "order_number"： 123,  
            "status": "success",  
            "failure_msg": ""
        }
     ...
    ]
 
}
```

{% endmethod %}



{% method -%}

##域名续费查询

```
GET /api/domains/domain/[id]/domain_renew/[renew_id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/domains/domain/10/domain_renew/10/
```

###返回结果

```json
{
	"id"： 10,    
    "domain_id"： 12345,  
    "term"： 1,  
    "order_number"： 123-345-567,  
    "status": "success",  
    "failure_msg": ""
}
```

{% endmethod %}



{% method -%}

##域名续费创建

```
POST /api/domains/domain/[id]/domain_renew/
```

###数据格式

```json
{
    "domain_id": 56789,
    "term": 默认值为1，最小值为1，最大值为10 
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/domains/domain/11/domain_renew/
```

###返回结果

```json
{
    "id"： 11,    
    "domain_id"： 56789,  
    "term"： 2,  
    "order_number"： "aaa",  
    "status": "success",  
    "failure_msg": ""
}
```


{% endmethod %}



{% method -%}

##生成ssl证书

```
POST /api/shops/custom_domain/[id]/makessl/
```

该接口所需权限 scope: `shops:write` 或 `write`

调用该接口后，可自动为自定义域名生成 ssl 证书，之后可以通过 https 方式访问网站

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/custom_domain/11/makessl/
```

###返回结果

```json
http 204
```

{% endmethod %}



