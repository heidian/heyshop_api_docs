
# 应用商店(Appstore)


{% method -%}

## 查询应用列表

```
GET /api/appstore/application_listing/
```

所需 scope: `appstore:read` 或 `read`

### Query 参数lang="http"

``` 
  page: 1  
  page_size: 10
```


###部分字段说明

|       字段      |      说明       | 
|      :------   |    :-------    |    
|    listing_id        |    已上架插件的id，部分未上架插件id值为空    |     
|    selling_points    |    由多个营销文案组成的list    | 
|    logo        |    存放logo图标的url地址    |  


{% sample lang="http" -%}

### 发送请求

```
GET /api/appstore/application_listing/?page=1
```

### 返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "application": {
                			 "id": 12,
                			 "name": "tset123",
                			 "title": "KOL营销插件",
                			 "description": "嘿店支持从有赞一键导入商品进行售卖，快来试试吧",
                			 "logo": "https://timgsa.baidu.com/example.jpg",
                			 "listing_id": 1
            },
            "body_html": "11",
            "screenshots": [],
            "selling_points": [],
            "title": "KOL营销插件",
            "description": "嘿店支持从有赞一键导入商品进行售卖，快来试试吧",
            "logo": "https://timgsa.baidu.com/example.jpg"
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

## 查询指定应用

```
GET /api/appstore/application_listing/[id]/
```

所需 scope: `appstore:read` 或 `read`


{% sample lang="http" -%}

### 发送请求

```
GET /api/appstore/application_listing/1/
```

### 返回结果

```json
{
	   		  "id": 1,
            "application": {
                			 "id": 2,
                			 "name": "tset123",
                			 "title": "KOL营销插件",
                			 "description": "嘿店支持从有赞一键导入商品进行售卖，快来试试吧",
                			 "logo": "https://timgsa.baidu.com/exmple.jpg",
                			 "listing_id": 1
            },
            "body_html": "11",
            "screenshots": [],
            "selling_points": [],
            "title": "KOL营销插件",
            "description": "嘿店支持从有赞一键导入商品进行售卖，快来试试吧",
            "logo": "https://timgsa.baidu.com/example.jpg"
}
```

{% endmethod %}



{% method -%}

##安装应用

```
POST /api/appstore/application_listing/[id]/install/
```

所需 scope: `appstore:write` 或 `write`


{% sample lang="http" -%}

### 发送请求

```
POST /api/appstore/application_listing/1/install/
```

### 返回结果

```json
{
    "url": "/api/oauth/authorize/?scope=read+write&response_type=code&client_id=JSDlUMBhbNusNx56bkmMGKNuymPwn82RoQHwVthe&redirect_uri=https%3A%2F%2Fyouzan.heidian.io%2Fheyshop_token"
}
```


{% endmethod %}


{% method -%}

## 获取已安装应用列表

```
GET /api/appstore/shop_application/
```

所需 scope: `appstore:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/appstore/shop_application/
```

### 返回结果

```json
[
    {
        "id": 12,
        "name": "heyshop_app_youzan",
        "title": "有赞商品导入",
        "embeded": false,
        "installed": true,
        "description": "品牌可将已有有赞店铺內商品一键导入嘿店。",
        "logo": "https://up.img.heidiancdn.com/o_1bpd6n5b16n61m9q5p4290h7r0logo.png",
        "url": "https://youzan.heidian.io/heyshop_token",
        "authorize_url": "/api/oauth/authorize/?xxxxxxxx",
        "listing_id": 1
    },
    ...
]
```

{% endmethod %}



{% method -%}

## 获取已安装应用详情

```
GET /api/appstore/shop_application/[id]/
```

所需 scope: `appstore:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/appstore/shop_application/2/
```

###返回结果


```json
{
        "id": 12,
        "name": "heyshop_app_youzan",
        "title": "有赞商品导入",
        "embeded": false,
        "installed": true,
        "description": "品牌可将已有有赞店铺內商品一键导入嘿店。",
        "logo": "https://up.img.heidiancdn.com/o_1bpd6n5b16n61m9q5p4290h7r0logo.png",
        "url": "https://youzan.heidian.io/heyshop_token",
        "authorize_url": "/api/oauth/authorize/?xxxxxxxx",
        "listing_id": 1
}
```

{% endmethod %}



{% method -%}

## 获取已安装应用的使用网址

```
POST /api/appstore/shop_application/[id]/app_url/
```

所需 scope: `appstore:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/appstore/shop_application/13/app_url/
```

###返回结果

```json
{
    "url": "https://kol.heidian.io/heyshop_token?code=dOVVA40NphHNBCkcfmjGNtjP2XHbd2&shop=test124.heidianer.com&state=test124”
}
```

{% endmethod %}

