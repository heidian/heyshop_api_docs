
# 优惠券

{% method -%}

##查询全部优惠券

```
GET /api/shuyun/coupon/
```



### Query 参数

```
id: 筛选指定id的优惠券
discount_type: 筛选优惠类型 percentage / fixed_amount / shipping
title__icontains: 筛选title中含有指定字符的优惠券
created_at__lte: 筛选创建时间小于等于指定时间的优惠券
created_at__gte: 筛选创建时间大于等于指定时间的优惠券
updated_at__lte: 筛选更新时间小于等于指定时间的优惠券
updated_at__gte: 筛选更新时间大于等于指定时间的优惠券
page: 页号
page_size: 分页大小
```

{% sample lang="example" -%}

###示例URL

```
GET /api/shuyun/coupon/?id=2&discount_type=percentage&title__icontains=test&created_at__gte=2017-10-10T16:52:01.141630%2B08:00&title__icontains=test&created_at__lte=2017-10-11T16:52:01.141630%2B08:00&page=1&page_size=10
```

###返回结果

```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 988,
            "title": "test3",   // 优惠券标题
            "verbose_title": "10% off",  // 优惠规则说明
            "description": "",  // 优惠券简介
            "code_prefix": "ArApJBDB",  // 兑换码
            "discount_type": "percentage",  // 优惠类型
            "value": "10.00",  // 优惠价值。本例中当 discount_type 为 percentage 时表示打9折；fixed_amount 时表示减 10 元
            "minimum_amount": "0.00",  // 最小金额限制（满x元可用），若为 0 表示不限制
            "starts_at": "2017-10-10T16:51:30+08:00",  // 开始时间，此时间点之后方可使用
            "ends_at": null,  // 结束时间
            "is_forever": true,  // 是否永久可用
            "quantity": 100,  // 发放最大数量
            "is_limit_once": false,  // 是否每个客户只能使用一次
            "created_at": "2017-10-10T16:52:01.141630+08:00", // 创建时间
            "used_count": 0, // 已使用数量
            "redeem_count": 1, // 已被兑换数量
            "can_redeem": true, // 用户可通过 code_prefix 兑换
            "is_offline": false // 线下优惠券
        },
        ...
     ]
}        
```

{% endmethod %}





{% method -%}

##将优惠码发放给指定用户

```
POST /api/shuyun/coupon/coupon_code/
```


###数据格式

```json
[
    {
        "member_id": 1001,
        "coupon_id": 10
    },
    {
        "member_id": 1002,
        "coupon_id": 10,
        "code": "xxxyyyzzz"
    }
]
```

{% sample lang="example" -%}

###示例URL

```
POST /api/shuyun/coupon/coupon_code/
```

###返回结果

```json
[
    {
        "id": 27518,
        "customer": {
            "member_id": 1001
        },
        "coupon_id": 10,
        "code": "jB7nNJPbf16adc",
        "status": "unused",
        "starts_at": "2017-10-10T15:34:29+08:00",
        "ends_at": null,
        "used_at": null,
        "created_at": "2017-10-11T14:53:34.083556+08:00",
        "title": "test2",
        "verbose_title": "打5折",
        "discount_type": "percentage",
        "value": "50.00",
        "minimum_amount": "0.00"
    },
    {
        "id": 27502,
        "customer": {
            "member_id": 1002
        },
        "coupon_id": 10,
        "code": "xxxyyyzzz",
        "status": "unused",
        "starts_at": "2017-10-10T16:51:30+08:00",
        "ends_at": null,
        "used_at": null,
        "created_at": "2017-10-11T14:53:34.083556+08:00",
        "title": "test2",
        "verbose_title": "打5折",
        "discount_type": "percentage",
        "value": "50.00",
        "minimum_amount": "0.00"
    }
]
```

{% endmethod %}








