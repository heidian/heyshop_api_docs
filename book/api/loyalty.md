#VIP会员积分



{% method -%}

##查询积分等级规则

```
GET /api/loyalty/level/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/loyalty/level/
```

###返回结果

```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 5,
            "title": "初级会员",
            "condition_points": 0,
            "sign_points": 0,
            "gain_points": 0,
            "redeem_points": 0,
            "redeem_limitation": 0,
            "redeem_limitation_monthly": 0
        },
        {
            "id": 8,
            "title": "黄金会员",
            "condition_points": 1000,
            "sign_points": 0,
            "gain_points": 0,
            "redeem_points": 0,
            "redeem_limitation": 0,
            "redeem_limitation_monthly": 0
        },
        {
            "id": 9,
            "title": "白金会员",
            "condition_points": 2000,
            "sign_points": 0,
            "gain_points": 0,
            "redeem_points": 0,
            "redeem_limitation": 0,
            "redeem_limitation_monthly": 0
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定积分等级规则

```
GET /api/loyalty/level/[id]/
```


{% sample lang="http" -%}

###发送请求

```
GET /api/loyalty/level/5/
```

###返回结果

```json
{
    "id": 5,
    "title": "初级会员",
    "condition_points": 0,
    "sign_points": 0,
    "gain_points": 0,
    "redeem_points": 0,
    "redeem_limitation": 0,
    "redeem_limitation_monthly": 0
}
```

{% endmethod %}



{% method -%}

##创建积分等级规则

```
POST /api/loyalty/level/
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    title       |  str型，积分规则名称    |
|    condition_points       |  int型，达标积分要求 0表示不需要任务条件    |
|    sign_points       |  int型，签到赠送积分数    |
|    gain_points       |  float型，消费多少1元可额外获得多少积分（在默认获得基本积分1分的基础上）   |
|    redeem_points       |  int型，使用多少积分可兑换1元钱   |
|    redeem_limitation       |  int型，限制每次最对兑换多少积分（0为不限制）   |
|    redeem_limitation_monthly       |  int型，限制每月最对兑换多少积分（0为不限制）    |

###数据格式

```json
{
    "title": "特殊会员",
    "condition_points": 100,
    "sign_points": 0,
    "gain_points": 1,
    "redeem_points": 100,
    "redeem_limitation": 0,
    "redeem_limitation_monthly": 0
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/loyalty/level/
```

###返回结果

```json
{
    "id": 27,
    "title": "特殊会员",
    "condition_points": 100,
    "sign_points": 0,
    "gain_points": 1,
    "redeem_points": 100,
    "redeem_limitation": 0,
    "redeem_limitation_monthly": 0
}
```

{% endmethod %}



{% method -%}

##更新会员等级规则

```
PUT /api/loyalty/level/[id]/
```

###数据格式

```json
{
    "title": "特殊会员",
    "condition_points": 100,
    "sign_points": 0,
    "gain_points": 1,
    "redeem_points": 100,
    "redeem_limitation": 0,
    "redeem_limitation_monthly": 0
}
```

{% sample lang="http" -%}

###发送请求

```
PUT /api/loyalty/level/27/
```

###返回结果

同创建积分等级规则接口 返回完整的规则信息

{% endmethod %}



{% method -%}

##删除会员等级规则

```
DELETE /api/loyalty/level/[id]/
```

{% sample lang="http" -%}

###发送请求

```
DELETE /api/loyalty/level/27/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询积分历史

```
GET /api/loyalty/point_history/
```

### Query 参数
```
customer: 10000,
customer__in: 10000,100001
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/loyalty/point_history/
```

###返回结果

```json
{
    "count": 11,
    "next": "https://ywu18.heidianer.com/api/loyalty/point_history/?page=2",
    "previous": null,
    "results": [
        {
            "id": 11742,
            "customer": {
                "id": 50784,
                "mobile": "13400000000",
                "full_name": "test",
                "gender": "unknown",
                "email": null,
                "birthday": null,
                "note": "",
                "accepts_marketing": true,
                "created_at": "2018-09-03T13:56:15.425250+08:00",
                "updated_at": "2018-09-03T13:56:15.425284+08:00"
            },
            "points": 0,
            "kind": "buy",
            "order": {
                "id": 2528283,
                "order_number": "43720180904135428305",
                "token": "140d02c143da84766c595e1d7b26644abbf8399f",
                "cart_token": "",
                "requires_shipping": true,
                "full_name": "test",
                "email": "",
                "mobile": "13400000000",
                "buyer_accepts_marketing": true,
                "total_line_items_price": "0.02",
                "total_discounts": "0.00",
                "shipping_cost": "0.00",
                "total_weight": "1.00",
                "total_price": "0.02",
                "order_status": "open",
                "fulfillment_status": "pending",
                "financial_status": "paid",
                "attributes": {},
                "note": "",
                "created_at": "2018-09-04T13:54:28.306601+08:00",
                "updated_at": "2018-09-04T13:55:20.284446+08:00",
                "cancelled_at": null,
                "cancel_reason": "",
                "closed_at": null,
                "referring_platform": "web",
                "customer": {
                    "id": 50784,
                    "mobile": "13400000000",
                    "full_name": "test",
                    "gender": "unknown",
                    "email": null,
                    "birthday": null,
                    "note": "",
                    "accepts_marketing": true,
                    "created_at": "2018-09-03T13:56:15.425250+08:00",
                    "updated_at": "2018-09-03T13:56:15.425284+08:00"
                },
                "shipping_address": {
                    "full_name": "test",
                    "mobile": "13400000000",
                    "address1": "泰兴路",
                    "address2": "",
                    "city": "市辖区",
                    "district": "静安区",
                    "province": "上海市",
                    "country": "",
                    "place_code": "310106",
                    "place": {
                        "id": 865,
                        "name": "静安区",
                        "code": "310106",
                        "parent": 861,
                        "area": "",
                        "short_name": "静安区",
                        "pinyin": "JingAnQu"
                    },
                    "draft": null
                },
                "lines": [
                    {
                        "id": 3766696,
                        "order_id": 2528283,
                        "image": "https://up.img.heidiancdn.com/o_1bvlth8lr15pd1l811dvq1nk107t0condBG.jpg",
                        "title": "免费（测试）",
                        "requires_shipping": true,
                        "product_title": "免费（测试）",
                        "variant_title": "",
                        "sku": "FE4589",
                        "barcode": "",
                        "quantity": 2,
                        "price": "0.01",
                        "total_price": "0.02",
                        "total_discount": "0.00",
                        "variant": {
                            "id": 54439,
                            "sku": "FE4589",
                            "title": "",
                            "image": {
                                "src": "https://up.img.heidiancdn.com/o_1bvlth8lr15pd1l811dvq1nk107t0condBG.jpg",
                                "metafield": {
                                    "orientation": null,
                                    "ext": ".jpg",
                                    "height": 1200,
                                    "size": 314114,
                                    "width": 1900,
                                    "key": "o_1bvlth8lr15pd1l811dvq1nk107t0condBG.jpg",
                                    "imageAve": {
                                        "RGB": "0x6c6a46"
                                    },
                                    "name": "secondBG.jpg",
                                    "mimeType": "image/jpeg"
                                }
                            },
                            "price": "0.01",
                            "grams": "1.00",
                            "options": [],
                            "barcode": ""
                        },
                        "product": {
                            "id": 25673,
                            "name": "free",
                            "title": "免费（测试）",
                            "vendor_id": null
                        },
                        "fulfillment_status": "pending",
                        "shipping_method_id": 3244,
                        "shipping_method": {
                            "id": 3244,
                            "group_id": 1507,
                            "rate_type": "fixed",
                            "is_enabled": true,
                            "title": "快递_test",
                            "normal_price": "0.00",
                            "normal_unit": "1.00",
                            "over_price": "1.00",
                            "over_unit": "1.00",
                            "free_shipping_policy": "nope",
                            "free_shipping_price": "1.00"
                        },
                        "fulfillment_location": {
                            "id": 3326,
                            "active": true,
                            "title": "默认仓库",
                            "is_default": true
                        },
                        "attributes": {},
                        "fulfillable_quantity": 2,
                        "fulfillment_service": null
                    }
                ],
                "discounts": [],
                "coupon_codes": [],
                "paid_at": "2018-09-04T13:55:20.278918+08:00",
                "paid_price": "0.02",
                "refunded_price": "0.00",
                "is_fulfillment_silent": false,
                "is_refunding": false,
                "is_test": false,
                "timeline": [
                    {
                        "id": 79135,
                        "order_id": 2528283,
                        "action": "payment_succeeds",
                        "note": "",
                        "replied": true,
                        "metafield": {},
                        "created_at": "2018-09-04T13:55:20.311592+08:00"
                    }
                ],
                "fulfilled_at": null,
                "selected_channel": "wx_pub",
                "fulfillments": [],
                "transactions": [
                    {
                        "id": 34666,
                        "order_id": 2528283,
                        "transaction_number": "73920180904135502944",
                        "amount": "0.02",
                        "transaction_type": "sale",
                        "status": "success",
                        "payment_backend": "heyshop_delegated",
                        "delegated": true,
                        "channel": "wx_pub",
                        "refund_id": null,
                        "refund_url": "",
                        "refund_msg": "",
                        "refundable_amount": "0.02",
                        "issue": "",
                        "receipt": {},
                        "expires_at": "2018-09-04T15:55:14+08:00",
                        "succeeds_at": "2018-09-04T13:55:20.263595+08:00",
                        "created_at": "2018-09-04T13:55:02.946562+08:00"
                    }
                ],
                "pending_refund_requests": [],
                "title": "免费（测试）",
                "information": {
                    "campaign_name": "",
                    "campaign_source": "",
                    "campaign_medium": "",
                    "campaign_content": "",
                    "campaign_term": "",
                    "browser_ip": "192.168.2.180",
                    "user_agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
                    "source_name": "web"
                },
                "redeem_points": 0,
                "points_price": "0.00"
            },
            "created_at": "2018-09-04T13:55:20.294707+08:00"
        },
        ...
     ]
}
```

{% endmethod %}



{% method -%}

##查询指定积分历史

```
GET /api/loyalty/point_history/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/loyalty/point_history/11742/
```

###返回结果

```json
{
    "id": 11742,
    "customer": {
        "id": 50784,
        "mobile": "13400000000",
        "full_name": "test",
        "gender": "unknown",
        "email": null,
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2018-09-03T13:56:15.425250+08:00",
        "updated_at": "2018-09-03T13:56:15.425284+08:00"
    },
    "points": 0,
    "kind": "buy",
    "order_id": 2528283,
    "created_at": "2018-09-04T13:55:20.294707+08:00"
}
```

{% endmethod %}



{% method -%}

##调整指定用户积分

```
POST /api/loyalty/point_history/adjust/
```

###数据格式

```json
{
    "customer_id": 100000,
    "points": 100
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/loyalty/point_history/adjust/
```

###返回结果

```json
{
    "id": 92921,
    "customer": {
        "id": 100000,
        "mobile": "13400000000",
        "full_name": "test",
        "gender": "unknown",
        "email": null,
        "birthday": null,
        "note": "",
        "accepts_marketing": true,
        "created_at": "2018-09-03T13:56:15.425250+08:00",
        "updated_at": "2018-09-03T13:56:15.425284+08:00"
    },
    "points": 100,
    "kind": "manual",
    "order": null,
    "created_at": "2019-07-05T19:25:01.631379+08:00"
}
```

{% endmethod %}