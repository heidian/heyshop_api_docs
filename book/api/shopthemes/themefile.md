#主题文件

{% method -%}

##查询主题文件列表

```
GET /api/shopthemes/theme/[id]/file/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/1/file/
```

###返回结果

```json
[
	{
        "id": 1,
        "file_type": "blocks",
        "name": "article_list",
        "icon": "https://up.img.heidiancdn.com/o_1bh72u0csrng10p746o13l3rn905-%E5%8D%9A%E5%AE%A2%E5%88%97%E8%A1%A8.jpg?imageView2/2/w/1600/ignore-error/1",
        "title": "#{blocks.title.article_list}",
        "description": "",
        "content":" ",
        "src": "",
        "permanent": true,
        "settings_schema": [
            {
                "type": "Boolean",
                "id": "sidebar",
                "title": "#{article_list.sidebar.show}"
            },
            {
                "type": "Selection",
                "id": "article_style",
                "title": "#{article_list.article.style}",
                "options": [
                    {
                        "text": "#{article_list.article.left_image}",
                        "value": "row"
                    },
                    {
                        "text": "#{article_list.article.card}",
                        "value": "card"
                    },
                    {
                        "text": "#{article_list.article.square}",
                        "value": "square"
                    }
                ]
            },
            ...
        ],
        "settings_data": {
            "image_proportion": 1,
            "column_count": 3,
            "article_style": "row",
            "sidebar": true
        },
        "presets": [],
        "metafield": {
            "category": "misc"
        }
    },
]
```

{% endmethod %}



{% method -%}

##查询主题配置列表

```
GET /api/shopthemes/theme/[id]/pageconfig/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/34044/pageconfig/
```

###返回结果

```json
[
    {
        "id": 34044,
        "theme": 880,
        "page": {
            "id": 10059,
            "page_type": "product",
            "permanent": true,
            "name": "CCD2F0"
        },
        "locked": true,
        "template_name": "",
        "settings_data": {
            "components": [
                {
                    "name": "blocks/header"
                },
                {
                    "name": "blocks/product-detail",
                    "settings_data": {
                        "alignment": "left",
                        "__layout__": "A"
                    }
                },
                {
                    "name": "blocks/footer"
                }
            ]
        }
    },
    ...
]
```

{% endmethod %}



{% method -%}

##查询指定主题配置

```
GET /api/shopthemes/theme/[id]/pageconfig/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/34044/pageconfig/12/
```

###返回结果

```json
{
    "id": 34044,
    "theme": 880,
    "page": {
        "id": 10059,
        "page_type": "product",
        "permanent": true,
        "name": "CCD2F0"
    },
    "locked": true,
    "template_name": "",
    "settings_data": {
        "components": [
            {
                "name": "blocks/header"
            },
            {
                "name": "blocks/product-detail",
                "settings_data": {
                    "alignment": "left",
                    "__layout__": "A"
                }
            },
            {
                "name": "blocks/footer"
            }
        ]
    }
}
```

{% endmethod %}



{% method -%}

##更新主题配置

```
PATCH /api/shopthemes/theme/[id]/pageconfig/[id]/
```

所需 scope: `shopthemes: write` 或 `write`

###数据格式

```json
{
	"template_name":"test"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shopthemes/theme/34044/pageconfig/12/
```

###返回结果

同查询指定页面配置接口 返回完整的配置信息

{% endmethod %}



{% method -%}

##查询主题翻译

```
GET /api/shopthemes/theme/[id]/locale/
```

### Query 参数

```
language: en
language__in: en,zh
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/99615/locale/
```

###返回结果

```json
[
    {
        "id": 99615,
        "scope": "settings_schema",
        "language": "en",
        "translations": {
            "article_list": {
                "article": {
                    "card": "Card",
                    "left_image": "With images on the left",
                    "square": "Square",
                    "style": "Articles layout",
                    "swiper": "Swiper"
                },
                "sidebar": {
                    "show": "Show sidebar"
                }
            },
            "article_single": {
                "cover": {
                    "enable": "Enable Cover Image",
                    "min_height": "Minimum height of cover(px)"
                },
                "container": {
                    "width": "Customize max-width of the content"
                }
            },
            "blocks": {
                "title": {
                    "article_list": "Articles list",
                    "article_single": "Article detail",
                    "carousel": "Carousel",
                    ...
                }
            },
            ...
          },
          ...		//页面返回结果过多，请用户自行调用查看结果
]
```

{% endmethod %}



{% method -%}

##查询指定主题翻译

```
GET /api/shopthemes/theme/[id]/locale/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/[id]/locale/115/
```

###返回结果

同查询页面翻译接口 返回完整的翻译信息

{% endmethod %}



{% method -%}

##查询脚本标签

```
GET /api/shopthemes/script_tag/
```

所需 scope: `shopthemes:read` 或 `read`

### Query 参数

```
display_scope: all	//all, checkout, shopfront
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/script_tag/
```

###返回结果

```json
[
    {
        "id": 181,
        "application_id": 2,
        "src": "",
        "content": "",
        "display_scope": "shopfront",
        "created_at": "2017-10-18T15:33:58.986470+08:00",
        "updated_at": "2017-10-18T15:33:58.986523+08:00"
    },
    {
        "id": 182,
        "application_id": 2,
        "src": "",
        "content": "12312",
        "display_scope": "shopfront",
        "created_at": "2017-10-18T16:15:08.436398+08:00",
        "updated_at": "2017-10-18T16:15:08.436452+08:00"
    }
]
```

{% endmethod %}



{% method -%}

##查询指定脚本标签

```
GET /api/shopthemes/script_tag/[id]/
```

所需 scope: `shopthemes:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/script_tag/182/
```

###返回结果

```json
{
    "id": 182,
    "application_id": 2,
    "src": "",
    "content": "12312",
    "display_scope": "shopfront",
    "created_at": "2017-10-18T16:15:08.436398+08:00",
    "updated_at": "2017-10-18T16:15:08.436452+08:00"
}
```

{% endmethod %}



{% method -%}

##创建脚本标签

```
POST /api/shopthemes/script_tag/
```

所需 scope: `shopthemes:write` 或 `write`

###数据格式

```json
{
    "src": "https://www.test.com",
    "content": "12312",
    "display_scope": "shopfront"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/script_tag/
```

###返回结果

同查询脚本标签接口 返回完整的标签信息

{% endmethod %}



{% method -%}

##更新脚本标签

```
PATCH /api/shopthemes/script_tag/[id]/
```

所需 scope: `shopthemes:write` 或 `write`

###数据格式

```json
{
    "src": "https://www.test.com",
    "content": "12312",
    "display_scope": "shopfront"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shopthemes/script_tag/182/
```

###返回结果

同查询脚本标签接口 返回完整的标签信息

{% endmethod %}



{% method -%}

##删除脚本标签

```
DELETE /api/shopthemes/script_tag/[id]/
```

所需 scope: `shopthemes:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
DELETE /api/shopthemes/script_tag/182/
```

###返回结果

```json
http 204
```

{% endmethod %}





