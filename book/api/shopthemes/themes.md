#主题市场

{% method -%}

##查询主题列表

```
GET /api/themes/theme/
```

### Query 参数

```
name: garden		//garden, jewelry,jelly,botanic,qimin(器皿),blue,rush,rushplus,mobile
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/themes/theme/?page=1
```

### 返回结果

```json
{
    "count": 8,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 3,
            "name": "garden",
            "title": "Garden",
            "description": "",
            "version": "1.0",
            "author": "鬼骨孖@HeyShop",
            "price": "0.00",
            "screenshot": "https://up.img.heidiancdn.com/o_1b3fi77bid4vu3510n81e7d1grv0web.jpg",
            "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fi77bhf041pols1c4vj1ko10mobile.jpg",
            "metafield": {
                "assets_prefix": "https://theme.heidiancdn.com/1504809189778/garden/theme/assets/"
            },
            "presets": [
                {
                    "id": 4123,
                    "theme": 3,
                    "name": "coffee",
                    "title": "咖啡手艺",
                    "description": "简洁大方的黑色背景\n醇厚留香的视觉感受\n故事感满满的复古搭配\n完美地适用于食品与饮料等行业",
                    "color": "#FFE2AF",
                    "demo": "http://garden-coffee.heidianer.com/",
                    "screenshot": "https://up.img.heidiancdn.com/o_1b3fi6tgr19h3t111i521861oh90web.jpg",
                    "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fi6tgqpdsuua16po1mse4k10mobile.jpg",
                    "assets": [
                        "stylesheets/styles.css.njk"
                    ],
                    "settings_data": {
                        "menu": {
                            "name": "main"
                        },
                        "show_demo_products": false,
                        "colors": {
                            "color-bg": "rgba(33,33,33,1)",
                            "color-text": "#ffffff",
                            "color-secondary": "rgba(200,200,200,1)",
                            "color-link": "#ffffff",
                            "color-primary": "#ffe2af",
                            "color-btn-text": "rgba(33,33,33,1)"
                        },
                        "header_image_height": 52,
                        "header_image": "https://up.img.heidiancdn.com/o_1b39gtu45ap6j9v12uj10rqvsd0logo.png"
                    },
                    "tags": [
                        "食品与饮料",
                        "健康与养生",
                        "艺术与建筑"
                    ]
                },
                {
                    "id": 4122,
                    "theme": 3,
                    "name": "default",
                    "title": "果然市集",
                    "description": "简约的布局\n通用的样式\n清新的视觉效果\n完美地适用于多种行业",
                    "color": "#FFF",
                    "demo": "http://garden.heidianer.com/",
                    "screenshot": "https://up.img.heidiancdn.com/o_1b3fi77bid4vu3510n81e7d1grv0web.jpg",
                    "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fi77bhf041pols1c4vj1ko10mobile.jpg",
                    "assets": [
                        "stylesheets/styles.css.njk"
                    ],
                    "settings_data": {
                        "menu": {
                            "name": "main"
                        },
                        "show_demo_products": false,
                        "colors": {
                            "color-bg": "rgba(255,255,255,1)",
                            "color-text": "rgba(85,85,85,1)",
                            "color-secondary": "rgba(66,66,66,1)",
                            "color-link": "rgba(51,51,51,1)",
                            "color-primary": "rgba(66,66,66,1)",
                            "color-btn-text": "rgba(255,255,255,1)"
                        },
                        "header_image_height": 80,
                        "header_image": "https://up.img.heidiancdn.com/o_1anc1m2nd1d471vfbt3o1l3h1gfn0.png"
                    },
                    "tags": [
                        "服饰与箱包",
                        "珠宝与美妆",
                        "家具与家居",
                        "电子与电器",
                        "食品与饮料",
                        "母婴与玩具",
                        "健康与养生",
                        "运动与户外",
                        "艺术与建筑",
                        "其他"
                    ]
                }
            ]
        },
        ...
       ]
}
```

{% endmethod %}



{% method -%}

##查询指定主题

```
GET /api/themes/theme/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/themes/theme/4/
```

### 返回结果

```json
{
    "id": 4,
    "name": "jewelry",
    "title": "Jewelry",
    "description": "",
    "version": "1.0",
    "author": "李然@HeyShop",
    "price": "0.00",
    "screenshot": "https://up.img.heidiancdn.com/o_1b3fol0jh1e5c1o3c1u131u931qnp0web.jpg",
    "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fol0jdhprlag1uqr6tecs10mobile.jpg",
    "metafield": {
        "assets_prefix": "https://theme.heidiancdn.com/1504809210457/jewelry/theme/assets/"
    },
    "presets": [
        {
            "id": 4124,
            "theme": 4,
            "name": "default",
            "title": "珠宝首饰",
            "description": "简洁大方的黑色背景\n醇厚留香的视觉感受\n故事感满满的复古搭配\n完美地适用于食品与饮料等行业",
            "color": "#FFF",
            "demo": "http://jewelry.heidianer.com/",
            "screenshot": "https://up.img.heidiancdn.com/o_1b3fol0jh1e5c1o3c1u131u931qnp0web.jpg",
            "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fol0jdhprlag1uqr6tecs10mobile.jpg",
            "assets": [
                "stylesheets/styles.css.njk"
            ],
            "settings_data": {
                "menu": {
                    "name": "main"
                },
                "colors": {
                    "color-title": "rgba(85,85,85,1)",
                    "color-secondary": "rgba(248,248,248,1)",
                    "color-border": "rgba(245,245,245,1)",
                    "color-primary": "rgba(66,66,66,1)",
                    "color-btn-primary": "rgba(66,66,66,1)",
                    "color-btn-primary-text": "rgba(255,255,255,1)",
                    "color-bg": "rgba(255,255,255,1)",
                    "color-text": "rgba(85,85,85,1)",
                    "color-btn-secondary": "rgba(248,248,248,1)",
                    "color-link": "rgba(51,51,51,1)",
                    "color-divider": "rgba(222,222,222,1)",
                    "color-btn-secondary-text": "rgba(85,85,85,1)"
                }
            },
            "tags": [
                "服饰与箱包",
                "珠宝与美妆",
                "艺术与建筑"
            ]
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询预设列表

```
GET /api/themes/preset/
```

### Query 参数

```
id: 1001
id__in: 1001,1002,1003
nmae: rushplus
name__in: modern,times
theme: 1001		//theme id值
```

{% sample lang="http" -%}

###发送请求

```
GET /api/themes/preset/?page=1
```

### 返回结果

```json
{
    "count": 38,
    "next": "http://ywu18.heidianer.com/api/themes/preset/?page=2",
    "previous": null,
    "results": [
        {
            "id": 4418,
            "theme": {
                "id": 10,
                "name": "rushplus",
                "title": "Rush+",
                "description": "",
                "author": "VariousDevs@HeyShop",
                "price": "0.00",
                "metafield": {
                    "assets_prefix": "https://theme.heidiancdn.com/1507543560305/rush2/theme/assets/"
                }
            },
            "name": "times",
            "title": "Times",
            "description": "我们期望家的样子\n简洁、明亮又温暖\n干净优雅的布局与展示\n舒服如在家中新购的地毯上\n光脚走了一圈\nWe all miss home from time to time. Warm, bright, clean…those were simpler times. Take a nap on the warm carpet in front of the hearth.",
            "color": "#000000",
            "demo": "https://rush-times.heidianer.com",
            "screenshot": "https://up.img.heidiancdn.com/o_1bj2djgbj15f01oob6o09091b7m0times-pc.j?imageView2/2/w/1600/ignore-error/1",
            "screenshot_mobile": "https://up.img.heidiancdn.com/o_1bj2djgbh1i3pfna4b21foe6270times-mobl?imageView2/2/w/1600/ignore-error/1",
            "assets": [
                "stylesheets/styles.heyshopstyle.css.njk",
                "stylesheets/customize.css.njk"
            ],
            "settings_data": {
                "footer_image": {
                    "src": "https://up.img.heidiancdn.com/o_1bb8jeh2dn36o051i9iorqqe90logo%EF%BC%8Dwhite"
                },
                "footer_table_container": "container--full",
                "footer_table_menu_title": {
                    "value": ""
                },
                "footer_table_social_title": {
                    "value": ""
                },
                "footer_menu": {
                    "name": "secondary"
                },
                "footer_table_link_text": {
                    "value": ""
                },
                "footer_simple_layout": "left_right",
                "footer_table_address_title": {
                    "value": ""
                },
                "footer_table_link_url": {
                    "value": ""
                },
                "header_image": {
                    "src": "https://up.img.heidiancdn.com/o_1bb8jeh2dn36o051i9iorqqe90logo%EF%BC%8Dwhite?imageMogr2/thumbnail/!100p|imageMogr2/crop/!644x307a87.5a225"
                },
                "footer_simple_social": false,
                "footer_normal_social": false,
                "footer_normal_layout": "B",
                "footer_simple_container": "container--full",
                "colors": {
                    "color-link": "rgba(0,0,0,1)",
                    "color-btn-primary-text": "rgba(255,255,255,1)",
                    "color-bg": "rgba(255,255,255,1)",
                    "color-btn-primary": "rgba(0,0,0,1)",
                    "color-text": "rgba(28,28,28,1)"
                },
                "footer_normal_slogan": true,
                "footer_simple_menu": false,
                "header_image_height": 0,
                "footer_opacity": 0,
                "footer_table_contents": "A",
                "footer_colorstyle_text": "rgba(255,255,255,1)",
                "footer_normal_address": false,
                "menu": {
                    "name": "main"
                },
                "footer_image_height": 100,
                "footer_normal_container": "container--full",
                "footer_show_mobile": false,
                "footer_colorstyle_bg": "rgba(0, 0, 0, 1)",
                "footer_table_link_title": {
                    "value": ""
                }
            },
            "tags": [
                "家具与家居",
                "电子与电器",
                "运动与户外",
                "艺术与建筑",
                "其他"
            ]
        },
        ...
      ]
}
```

{% endmethod%}



{% method -%}

##查询指定预设

```
GET /api/themes/preset/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/themes/preset/4420/
```

### 返回结果

```json
{
    "id": 4420,
    "theme": {
        "id": 10,
        "name": "rushplus",
        "title": "Rush+",
        "description": "",
        "author": "VariousDevs@HeyShop",
        "price": "0.00",
        "metafield": {
            "assets_prefix": "https://theme.heidiancdn.com/1507543560305/rush2/theme/assets/"
        }
    },
    "name": "print",
    "title": "Print",
    "description": "返朴归真的展示\n天然与绿色的延绵伸展\n健康环保的生活方式\n完美适用于手工艺术品等行业\nEarthy yet exotic aesthetics for the brand that loves the planet. A unique preset for anything handmade, DIY, or green in general.",
    "color": "#5BA1C2",
    "demo": "https://rush-print.heidianer.com",
    "screenshot": "https://up.img.heidiancdn.com/o_1bj2l60fn1eds1upsara90a1muf0rintpc.jpg?imageView2/2/w/1600/ignore-error/1",
    "screenshot_mobile": "https://up.img.heidiancdn.com/o_1bj2l60fm15n71esm17k81glgqm90mobile.jpg?imageView2/2/w/1600/ignore-error/1",
    "assets": [
        "stylesheets/styles.heyshopstyle.css.njk",
        "stylesheets/customize.css.njk"
    ],
    "settings_data": {
        "menu": {
            "name": "main"
        },
        "footer_table_container": "container--full",
        "footer_table_menu_title": {
            "value": ""
        },
        ...,
        "header_image": {
            "src": "https://up.img.heidiancdn.com/o_1bar6jeiepar1avl1hr71n9k18im0logo-black?imageMogr2/thumbnail/!225.00p|imageMogr2/crop/!1302x1042.5a265a322.5",
            "metafield": {}
        },
        "footer_simple_social": false,
        "footer_normal_social": false,
        "footer_normal_layout": "B",
        "footer_simple_container": "container--full",
        "colors": {
            "color-link": "rgba(0,0,0,1)",
            "color-btn-primary-text": "rgba(255,255,255,1)",
            "color-bg": "rgba(250,250,250,1)",
            "color-btn-primary": "rgba(91,161,194,1)",
            "color-text": "rgba(0,0,0,1)"
        },
        "footer_normal_slogan": false,
        "footer_simple_menu": false,
        "header_image_height": 0,
        "footer_opacity": 0,
        "footer_table_contents": "A",
        "footer_colorstyle_text": "rgba(0, 0, 0, 1)",
        "footer_normal_address": true,
        "footer_image_height": 46,
        "footer_normal_container": "container",
        "footer_table_social_title": {
            "value": ""
        },
        "footer_show_mobile": false,
        "footer_colorstyle_bg": "rgba(255,255,255,1)",
        "footer_table_link_title": {
            "value": ""
        }
    },
    "tags": [
        "家具与家居",
        "健康与养生",
        "艺术与建筑",
        "其他"
    ]
}
```

{% endmethod %}



{% method -%}

##上传主题

```
PATCH /api/themes/uploadtheme/[id]/
```

所需 scope: `themes:write` 或 `write`

```
TODO
```


{% endmethod %}
