
# 博客


{% method -%}

## 查询文章列表

```
GET /api/blogs/article/
```

所需 scope: `blogs:read` 或 `read`

### Query 参数
```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
published: true
title: test_tiele
tag: tag_title1,tag_title2
order_by: id (id / created_at / published_at)
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       | 
|      :------   |    :-------    |    
|    tags        | 由多个文章标签组成的列表    |  
|    published        | 博客文章是否发布，`true`为发布，`false` 为尚未发布   |  


{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/wallet/?id=1
```

### 返回结果

```json
{
    "count": 7,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 61,
            "name": "test_name1",
            /*
            * 其他字段参见下方的获取文章接口
            */
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

## 查询指定文章

```
GET /api/blogs/article/[id]/
```

所需 scope: `blogs:read` 或 `read`

{% sample lang="http" -%}

### 发送请求

```
GET /api/balance/wallet/61/
```

### 返回结果

```json
{
    "id": 61,
    "name": "test_name1",
    "title": "test_title",
    "author": "test_author",
    "description": "",
    "image": "",
    "tags": [],
    "body_html": "",
    "published": true,
    "published_at": "2017-02-08T04:23:01.701721Z",
    "metafield": {},
    "created_at": "2017-02-08T04:23:01.702455Z",
    "updated_at": "2017-02-15T09:05:09.648844Z"
}
```

{% endmethod %}



{% method -%}

## 创建文章

```
POST /api/blogs/article/
```

所需 scope: `blogs:write` 或 `write`

### 数据格式
```json
{
    "name": "values",
    "title": "有关零售关系",
    "author": "匿名",
    "description": "几年前晃悠过摩洛哥蓝色小城舍夫沙万的一家地毯店...",
    "image": "http://up.img.heidiancdn.com/o_1arqmr67k13gt1rmqppo11q68jd0o_1amvf8p6omdrg2j1uff1n2oi6k0.jpg",
    "tags": [],
    "body_html": "<p>几年前晃悠过摩洛哥蓝色小城舍夫沙万的一家地毯店...</p>",
    "published": true,
    "published_at": "2016-12-20T06:55:39.462709Z",
    "metafield": {},
    "created_at": "2016-12-20T06:55:39.463739Z",
    "updated_at": "2017-02-15T00:52:12.349106Z"
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/balance/wallet/
```

### 返回结果

同获取文章接口 返回完整的文章信息

{% endmethod %}



{% method -%}

## 更新文章

```
PATCH /api/blogs/article/[id]/
```

所需 scope: `blogs:write` 或 `write`

### 数据格式
```json
{
    "id": 1024,
    "name": "values",
    "title": "有关零售关系",
    "author": "匿名",
    "description": "几年前晃悠过摩洛哥蓝色小城舍夫沙万的一家地毯店...",
    "image": "http://up.img.heidiancdn.com/o_1arqmr67k13gt1rmqppo11q68jd0o_1amvf8p6omdrg2j1uff1n2oi6k0.jpg",
    "tags": [],
    "body_html": "<p>几年前晃悠过摩洛哥蓝色小城舍夫沙万的一家地毯店...</p>",
    "published": true,
    "published_at": "2016-12-20T06:55:39.462709Z",
    "metafield": {},
    "created_at": "2016-12-20T06:55:39.463739Z",
    "updated_at": "2017-02-15T00:52:12.349106Z"
}
```

{% sample lang="http" -%}

### 发送请求

```
PATCH /api/balance/wallet/61/
```

### 返回结果

同获取文章接口 返回完整的文章信息

{% endmethod %}



{% method %}

##删除文章

```
DELETE /api/blogs/article/[id]/
```

所需 scope: `blogs:write` 或 `write`

{% sample lang="http" -%}

### 发送请求

```
DELETE /api/balance/wallet/61/
```

###返回结果

```json  
http 204
```

{% endmethod %}

{% method -%}

##查询文章meta field

```
GET /api/blogs/article/[id]/metafields/
```

所需 scope: `blogs:read` 或 `read`

###部分字段说明

|       字段     |      说明       | 
|      :------   |    :-------    |    
|    key        | 关键值,只能包含字母，数字，下划线或者中划线，key值不可重复，不可更改    |  
|    namespace        | 命名空间,只能包含字母，数字，下划线或者中划线，namespace值不可更改    |
|    value        | 属性值,只能包含字母，数字，下划线或者中划线    |  

 


{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/article/8926/metafields/
```

###返回结果

```json
[
    {
        "id": 188,
        "namespace": "123",
        "key": "123",
        "value_type": "string",
        "value": "test",
        "description": ""
    },
    {
        "id": 189,
        "namespace": "123",
        "key": "1234",
        "value_type": "string",
        "value": "test",
        "description": "test"
    }
]
```

{% endmethod %}

{% method -%}

##查询指定文章meta field

```
GET /api/blogs/article/[id]/metafields/[id]/
```

所需 scope: `blogs:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/article/8926/metafields/189/
```

###返回结果

```json
{
    "id": 189,
    "namespace": "123",
    "key": "1234",
    "value_type": "string",
    "value": "test",
    "description": "test"
}
```


{% endmethod %}


{% method -%}

##创建文章meta field

```
POST /api/blogs/article/[id]/metafields/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
    "namespace": "123",
    "key": "1234",
    "value": "test",
    "description": "test"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/blogs/article/8926/metafields/
```

###返回结果

同查询指定文章meta field接口 返回完整的meta信息

{% endmethod %}



{% method -%}

##更新文章meta field

```
PATCH /api/blogs/article/[id]/metafields/[id]/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
    "value": "test2",
    "description": "test2"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/blogs/article/8926/metafields/188/
```

###返回结果

同查询指定文章meta field接口 返回完整的meta信息

{% endmethod %}



{% method -%}

##删除文章meta field

```
DELETE /api/blogs/article/[id]/metafields/[id]/
```

所需 scope: `blogs:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/blogs/article/8926/metafields/188/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询文章类别列表

```
GET /api/blogs/category/
```

所需 scope: `blogs:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/category/
```

###返回结果

```json
[
    {
        "id": 12,
        "title": "test"
    },
    ...
]
```

{% endmethod %}



{% method -%}

##查询文章类别

```
GET /api/blogs/category/[id]/
```

所需 scope: `blogs:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/category/12/
```

###返回结果

```json
{
    "id": 12,
    "title": "test"
}
```

{% endmethod %}



{% method -%}

##创建文章类别

```
POST /api/blogs/category/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
	"title": "test1"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/blogs/category/
```

###返回结果

同查询文章类别接口 返回完整的类别信息

{% endmethod %}



{% method -%}

##更新文章类别

```
PATCH /api/blogs/category/[id]/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
	"title": "test2"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/blogs/category/12/
```

###返回结果

同查询文章类别接口 返回完整的类别信息

{% endmethod %}



{% method -%}

##删除文章类别

```
DELETE /api/blogs/category/[id]/
```

所需 scope: `blogs:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/blogs/category/12/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询已应用文章标签列表

```
GET /api/blogs/tag/
```

所需 scope: `blogs:read` 或 `read`

### Query 参数

```
title: test
```

{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/tag/?title=test3
```

###返回结果

```json
[
    {
        "id": 604,
        "title": "test3"
    },
    ...
]
```

{% endmethod %}



{% method -%}

##查询已应用文章标签

```
GET /api/blogs/tag/[id]/
```

所需 scope: `blogs:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/blogs/tag/[id]/
```

###返回结果

```json
{
    "id": 604,
    "title": "test3"
}
```

{% endmethod %}



{% method -%}

##创建文章标签

```
POST /api/blogs/tag/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
    "title": "test4"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/blogs/tag/
```

###返回结果

同查询已应用文章标签接口 返回完整的标签信息

{% endmethod %}



{% method -%}

##更新文章标签

```
PATCH /api/blogs/tag/[id]/
```

所需 scope: `blogs:write` 或 `write`

###数据格式

```json
{
    "title": "test4"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/blogs/tag/604/
```

###返回结果

同查询已应用文章标签接口 返回完整的标签信息

{% endmethod %}



{% method -%}

##删除文章标签

```
DELETE /api/blogs/tag/[id]/
```

所需 scope: `blogs:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/blogs/tag/604/
```

###返回结果

```json
http 204
```

{% endmethod %}

