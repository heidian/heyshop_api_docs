
# 代金券

{% method -%}

## 创建代金券商品

```
POST /api/vouchers/voucher_product/
```

所需 scope: `vouchers:write` 或 `write`

### 数据格式
```json
{
  "title": "test2",
  "prices": [100, 300],
  "expire_days": 10,
  "activate_expires_at": "2017-01-02T14:00:00Z"
}
```
{% sample lang="http" -%}

###发送请求

```
POST /api/vouchers/voucher_product/
```

### 返回结果
```json
{
    "id": 32,
    "product_id": 288,
    "is_enabled": true,
    "white_product_ids": [],
    "black_product_ids": [],
    "expire_days": 10,
    "activate_expires_at": "2017-01-02T14:00:00Z"
}
```

{% endmethod %}




{% method -%}

## 发放代金券给用户

```
POST /api/vouchers/voucher/
```

所需 scope: `vouchers:write` 或 `write`

### 数据格式
```json
{
    "customer_ids": [1, 2],
    "title": "title123",
    "total_balance": "100",
    "white_product_ids": [1,2],
    "black_product_ids": [],
    "expire_days": 3,
    "starts_at": null,
    "ends_at": null
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/vouchers/voucher/
```

### 返回结果
```json
[
    {
        "id": 6,
        "customer_id": 1,
        "code": "4E9ECFB05EED10A1C02A",
        "title": "title123",
        "total_balance": "100.00",
        "balance": "100.00",
        "status": "available",
        "white_product_ids": [
            1,
            2
        ],
        "black_product_ids": [],
        "expire_days": 3,
        "starts_at": "2017-04-25T07:29:52.445593Z",
        "ends_at": "2017-04-28T07:29:52.445593Z",
        "activate_expires_at": null
    
    },
    //......
]
```

{% endmethod %}



{% method -%}

## 获取代金券列表

```
GET /api/vouchers/voucher/
```

所需 scope: `vouchers:read` 或 `read`

### Query 参数
```
status: 'unactivated' / 'unstart' / 'expired' / 'available'
customer_id: 123
customer_id__in: 123,135
page: 1
page_size: 10
```


{% sample lang="http" -%}

###发送请求

```
GET /api/vouchers/voucher/?page=1
```

### 返回结果
```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            // 上述完整的 voucher 数据
        },
        // .....
    ]
}
```

{% endmethod %}



{% method -%}

## 激活代金券

```
POST /api/customers/voucher/activate/
```

Authorization: `customertoken`

### 数据格式
```json
{
    "code": "4E9ECFB05EED10A1C02A"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/voucher/activate/
```

### 返回结果
```json
{
    "id": 5,
    "customer_id": 2,
    "code": "51B34CA70AA3CBD4C029",
    "title": "title123",
    "total_balance": "100.00",
    "balance": "100.00",
    "status": "available",
    "white_product_ids": [
        1,
        2
    ],
    "black_product_ids": [],
    "expire_days": 3,
    "starts_at": "2017-04-25T07:53:00Z",
    "ends_at": "2017-04-28T07:53:00Z"
}
```

{% endmethod %}


