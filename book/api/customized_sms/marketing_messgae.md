#营销类短信



{% method -%}

##查看营销短信列表

```
GET /api/notifications/marketing_message/         
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/marketing_message/?page=1       
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
    			  {
    				"id": 1001,
    				"receiver_category": "all",		// all(全部客户)/segments(部分群组)/mobiles(部分用户手机号)
					"segments":   [
									{	
										"id": 1002,
										"title": "test",
									},
									...
								    ]
					"mobiles": [
									"135xxxx1234",
									"135xxxx2468",
									...
									],
					"content": "test",		//短信内容
					"send_at": "2017-03-31 00:00:00"      //定时发送功能，若未输入任何值，则默认为立刻发送营销短信
					"sent_at": null,
					"is_test": false,
					"total_customer_count": 0,
					"total_sms_count": 0,
					"success_customer_count": 0,
					"success_sms_count": 0,

    			  },
    			  ...
    		   ]
}
```


{% endmethod %}



{% method -%}

##查看营销短信详情

```
GET /api/notifications/marketing_message/[id]/         
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/marketing_message/1001/         
```

###返回结果

```json
{
   "id": 1001,
   "receiver_category": "all",	// all(全部客户)/segments(部分群组)/mobiles(部分用户手机号)
	"segments":   [
						{	
							"id": 1002,
							"title": "test"
						},
						...
					],
	"mobiles": [
					"135xxxx1234",
					"135xxxx2468",
					...
				],
	"content": "test",		//短信内容
	"send_at": "2017-03-31 00:00:00"		//定时发送功能，若未输入任何值，则默认为立刻发送营销短信
	"sent_at": null,
	"is_test": false,
	"total_customer_count": 0,
	"total_sms_count": 0,
	"success_customer_count": 0,
	"success_sms_count": 0,
}
```


{% endmethod %}



{% method -%}

##创建营销短信

```
POST /api/notifications/marketing_message/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"receiver_category": "all",	// all(全部客户)/segments(部分群组)/mobiles(部分用户手机号)
	"segment_ids": [1,2,3],		//id值为用户划分的群组id
	"mobiles": [
					"135xxxx1234",
					"135xxxx2468",
					...
					],
	"content": "test",		//短信内容
	"send_at": "2017-03-31 00:00:00"		//定时发送功能，若未输入任何值，则默认为立刻发送营销短信
	
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/marketing_message/
```

###返回结果

同尚未发送的营销短信接口 返回完整的信息

{% endmethod %}



{% method -%}

##更新营销短信（必须是未发出的）

```
PATCH /api/notifications/marketing_message/[id]/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"receiver_category": "all",	// all(全部客户)/segments(部分群组)/mobiles(部分用户手机号)
	"segment_ids": [1001,1002,1003],		//id值为用户划分的群组id
	"mobiles": [
					"135xxxx1234",
					"135xxxx2468",
					...
					],
	"content": "test",		//短信内容
	"send_at": "2017-03-31 00:00:00"		//定时发送功能，若未输入任何值，则默认为立刻发送营销短信	
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/notifications/marketing_message/1001/
```

###返回结果

同尚未发送的营销短信接口 返回完整的信息

{% endmethod %}



{% method -%}

##查询短信模板列表

```
GET /api/notifications/marketing_template/
```

所需 scope: `notifications:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/marketing_template/
```

###返回结果

```json
[
	{
    	"id":1001,
    	"category": "test"，   //待定
    	"content": "test"	//模板内容
    },
    ...    			  
]
```

{% endmethod %}



{% method -%}

##创建短信模板

```
POST /api/notifications/marketing_template/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"id":1001,
	"category": "test"，   //待定
	"content": "test"	//模板内容
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/marketing_template/
```

###返回结果

```json
{
   "id":1001,
   "category": "test"，   //待定
   "content": "test"	//模板内容
}
```

{% endmethod %}



{% method -%}

##删除短信模板

```
DELETE /api/notifications/marketing_template/[id]/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/notifications/marketing_template/1001/
```

###返回结果

```json
http 204
```

{% endmethod %}




{% method -%}

##发送统计

```
GET /api/notifications/sub_sms/marketing/summary/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/marketing/summary/
```

###返回结果

```json
{
	"total_count": 1000,             //总发送条数
	"success_count": 500,			 //成功到达条数
	"cost_count": 600,				 //计费条数
	"rest_count": 400,				 //剩余条数
}
```

{% endmethod %}




{% method -%}

##获取充值营销短信商品信息

```
GET /api/notifications/sub_sms/marketing/recharge_product/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/marketing/recharge_product/
```

###返回结果

`product`对象

{% endmethod %}



{% method -%}

##查询营销短信充值记录

```
GET /api/notifications/sub_sms/marketing/recharge_history/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/marketing/recharge_history/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
					product对象,
					...
				]		
}
```

{% endmethod %}


