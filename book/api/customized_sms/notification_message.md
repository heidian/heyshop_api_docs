#通知类短信


{% method -%}

##设置短信签名

```
POST /api/notifications/sub_sms/signature/
```

所需 scope: `notifications:write` 或 `write`


###数据格式

```json
{
    "signature": "测试店",
}
```


{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/sub_sms/signature/
```

###返回结果

```
HTTP 204
```

{% endmethod %}




{% method -%}

##查看短信签名及状态

```
GET /api/notifications/sub_sms/signature/
```

所需 scope: `notifications:read` 或 `read`



{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/signature/
```

###返回结果

```json
{
    "signature": "测试店",
    "signature_status": 1   // 0 - 未添加 , 1 - 待审核 , 2 - 审核通过 , 3 - 审核拒绝
}
```

{% endmethod %}





{% method -%}

##发送统计

```
GET /api/notifications/sub_sms/notice/summary/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/notice/summary/
```

###返回结果

```json
{
	"total_count": 1000,   //总发送条数
	"success_count": 500,			 //成功到达条数
	"cost_count": 600,				 //计费条数
	"rest_count": 400,				 //剩余条数
}
```

{% endmethod %}



{% method -%}

##通知短信发送记录

```
GET /api/notifications/sub_sms/notice/history/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
page: 1 
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/notice/history/
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
    			  {
    				"id": 1001,
    				“content”: "test",		//短信内容
    				"mobile": "135xxxx2468",		//买家手机号
    				"created_at": "2017-03-31 00:00:00"		//短信发送时间
    			  },
    			  ...
    		   ]
}
```

{% endmethod %}


{% method -%}

##查询通知类短信充值记录

```
GET /api/notifications/sub_sms/notice/recharge_history/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/sub_sms/notice/recharge_history/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
					product对象,
					...
				]		
}
```

{% endmethod %}






{% method -%}

##开启短信子账户
购买短信套餐后会自动开启，所以不建议使用此接口！

```
POST /api/notifications/sub_sms/setup/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"appid":49
}
```

当前版本可供选择的appid共有两类：“49:通知类短信”，“52:营销类短信”

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/sub_sms/setup/
```

###返回结果

```json
{
    "id": 6,
    "appid": 49,
    "sub_id": "36444",
    "username": "subsms_6",
    "password": "e7c02f",
    "signature": "",
    "signature_status": 0,
    "balance": 0,
    "error_result": {},
    "is_activated": true,
    "is_enabled": true
}
```

{% endmethod %}



{% method -%}

##查询店铺短信模版

```
GET /api/notifications/shop_sms_template/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/shop_sms_template/?page=1
```

###返回结果

```json
{
    "count": 6,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 7,
            "name": "voucher_issue_notice",
            "title": "代金券发放通知",
            "content": "您获得一张价值为 {{ voucher.total_balance }} 元的礼品卡 {{ voucher.title }}，立即登录个人中心查看吧 \bhttps://{{ shop.domain }}/account/vouchers",
            "is_enabled": true,
            "allow_tags": [
                "{{ voucher.total_balance }}",
                "{{ voucher.title }}",
                "{{ shop.domain }}"
            ]
        },
        ...
             ]
}
```

{% endmethod %}



{% method -%}

##查询指定店铺短信模版

```
GET /api/notifications/shop_sms_template/[id]/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/shop_sms_template/7/
```

###返回结果

```json
{
    "id": 7,
    "name": "voucher_issue_notice",
    "title": "代金券发放通知",
    "content": "您获得一张价值为 {{ voucher.total_balance }} 元的礼品卡 {{ voucher.title }}，立即登录个人中心查看吧 \bhttps://{{ shop.domain }}/account/vouchers",
    "is_enabled": true,
    "allow_tags": [
        "{{ voucher.total_balance }}",
        "{{ voucher.title }}",
        "{{ shop.domain }}"
    ]
}
```

{% endmethod %}



{% method -%}

##更新店铺短信模版

```
PATCH /api/notifications/shop_sms_template/[id]/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"name": "test",
    "title": "test_notification",
    "content": "您获得一张价值为 {{ voucher.total_balance }} 元的礼品卡 {{ voucher.title }}",
    "is_enabled": "true"
}
```

`
"content"格式限制：用户只能使用“allow_tags”中所拥有的tag，tag格式为{{ xxx }}。
`

{% sample lang="http" -%}

###发送数据

```
PATCH /api/notifications/shop_sms_template/7/
```

###返回结果

```json
{
    "id": 7,
    "name": "voucher_issue_notice",
    "title": "代金券发放通知",
    "content": "您获得一张价值为 {{ voucher.total_balance }} 元的礼品卡 {{ voucher.title }}",
    "is_enabled": true,
    "allow_tags": [
        "{{ voucher.total_balance }}",
        "{{ voucher.title }}",
        "{{ shop.domain }}"
    ]
}
```

{% endmethod %}

