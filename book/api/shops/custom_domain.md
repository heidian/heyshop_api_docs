#自定义域名

{% method -%}

##查询自定义域名列表

```
GET /api/shops/custom_domain/
```

所需 scope: `shops:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/custom_domain/?page=1
```

###返回结果

```json
{
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 79,
            "domain": "ssl.taoxx123.cn",
            "beian_number": "沪ICP备17013301号",
            "is_default": false,
            "is_active": true,		//是否可用
            "ssl": true,
            "ssl_cert": "-----BEGIN CERTIFICATE-----\ntest\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\ntest=\n-----END CERTIFICATE-----\n",
            "ssl_cert_key": "-----BEGIN PRIVATE KEY-----\ntest\n-----END PRIVATE KEY-----\n"
        },
       ...
         }
    ]
}
```

{% endmethod %}


{% method -%}

##查询指定自定义域名

```
GET /api/shops/custom_domain/[id]/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/custom_domain/79/
```

###返回结果

```json
{
    "id": 79,
    "domain": "ssl.taoxx123.cn",
    "beian_number": "沪ICP备17013301号",
    "is_default": false,
    "is_active": true,		//是否可用
    "ssl": true,
    "ssl_cert": "-----BEGIN CERTIFICATE-----\ntest\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\ntest=\n-----END CERTIFICATE-----\n",
    "ssl_cert_key": "-----BEGIN PRIVATE KEY-----\ntest\n-----END PRIVATE KEY-----\n"
}
```

{% endmethod %}


{% method -%}

##创建自定义域名

```
POST /api/shops/custom_domain/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"domain": "test123.cn"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/custom_domain/
```

###返回结果

同查询指定自定义域名接口 返回完整的域名信息

{% endmethod %}



{% method -%}

##更新自定义域名

```
PATCH /api/shops/custom_domain/[id]/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"domain": "test321.cn"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shops/custom_domain/79/
```

###返回结果

同查询指定自定义域名 返回完整的域名信息

{% endmethod %}



{% method -%}

##删除自定义域名

```
DELETE /api/shops/custom_domain/[id]/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/shops/custom_domain/79/
```

###返回结果

```json
http 204
```

{% endmethod %}
