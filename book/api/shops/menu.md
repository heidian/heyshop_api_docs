#主菜单

{% method -%}

##查询菜单列表

```
GET /api/shops/menu/
```

所需 scope: `shops:read` 或 `read`

### Query 参数

```
id: 1001
id__in: 1001,1002
name: test1
name__in: test1, test2
created_at: 2017-02-01 00:00:00
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/menu/?page=1
```

###返回结果

```json
[
    {
        "name": "main",
        "title": "主菜单",
        "permanent": true,
        "links": [
            {
                "url": "/",
                "target": "",
                "title": "主页"
            },
            {
                "url": "/search",
                "target": "",
                "title": "全部商品"
            },
            {
                "url": "/blog",
                "target": "",
                "title": "博客文章"
            },
            {
                "url": "/collections",
                "target": "",
                "title": "专题商品"
            },
            {
                "url": "/page/about",
                "target": "",
                "title": "关于",
                "links": [
                    {
                        "url": "/page/about",
                        "target": "",
                        "title": "关于我们"
                    },
                    {
                        "url": "/page/contact",
                        "target": "",
                        "title": "联系我们"
                    },
                    {
                        "url": "/page/guides",
                        "target": "",
                        "title": "购买指南"
                    }
                ]
            }
        ],
        "items": [
            {
                "id": 182056,
                "menu_type": "page",
                "parent_id": null,
                "page_id": 55292,
                "position": 1,
                "title": "主页",
                "url": "/",
                "target": "",
                "created_at": "2017-09-06T15:04:23.119731+08:00",
                "updated_at": "2017-09-06T15:04:23.119780+08:00",
                "children": []
            },
            {
                "id": 182057,
                "menu_type": "page",
                "parent_id": null,
                "page_id": 55293,
                "position": 2,
                "title": "全部商品",
                "url": "/search",
                "target": "",
                "created_at": "2017-09-06T15:04:23.139333+08:00",
                "updated_at": "2017-09-06T15:04:23.139372+08:00",
                "children": []
            },
            ...
                ]
            }
        ],
        "created_at": "2017-09-06T15:04:09.404947+08:00",
        "updated_at": "2017-09-06T15:04:09.404980+08:00"
    },
    ...
    }
]
```

{% endmethod %}



{% method -%}

##查询指定菜单

```
GET /api/shops/menu/[name]/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/menu/mobile/
```

###返回结果

```json
{
    "name": "mobile",
    "title": "手机导航",
    "permanent": true,
    "links": [
        {
            "url": "/",
            "target": "",
            "title": "店铺"
        },
        {
            "url": "/search",
            "target": "",
            "title": "商品"
        },
        {
            "url": "/cart",
            "target": "",
            "title": "购物车"
        },
        {
            "url": "/account",
            "target": "",
            "title": "我的"
        }
    ],
    "items": [
        {
            "id": 182064,
            "menu_type": "page",
            "parent_id": null,
            "page_id": 55292,
            "position": 1,
            "title": "店铺",
            "url": "/",
            "target": "",
            "created_at": "2017-09-06T15:04:23.311114+08:00",
            "updated_at": "2017-09-06T15:04:23.311166+08:00",
            "children": []
        },
        {
            "id": 182065,
            "menu_type": "page",
            "parent_id": null,
            "page_id": 55293,
            "position": 2,
            "title": "商品",
            "url": "/search",
            "target": "",
            "created_at": "2017-09-06T15:04:23.334749+08:00",
            "updated_at": "2017-09-06T15:04:23.334816+08:00",
            "children": []
        },
        {
            "id": 182066,
            "menu_type": "page",
            "parent_id": null,
            "page_id": 55304,
            "position": 3,
            "title": "购物车",
            "url": "/cart",
            "target": "",
            "created_at": "2017-09-06T15:04:23.358033+08:00",
            "updated_at": "2017-09-06T15:04:23.358085+08:00",
            "children": []
        },
        {
            "id": 182067,
            "menu_type": "link",
            "parent_id": null,
            "page_id": null,
            "position": 4,
            "title": "我的",
            "url": "/account",
            "target": "",
            "created_at": "2017-09-06T15:04:23.373076+08:00",
            "updated_at": "2017-09-06T15:04:23.373150+08:00",
            "children": []
        }
    ],
    "created_at": "2017-09-06T15:04:09.410436+08:00",
    "updated_at": "2017-09-06T15:04:09.410479+08:00"
}
```

{% endmethod %}



{% method -%}

##创建菜单

```
POST /api/shops/menu/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
    "title": "123456789",
    "permanent": false, 
    "links": [], 
    "items": []
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/menu/
```

###返回结果

```json
{
    "name": "e52590",
    "title": "123456789",
    "permanent": false,
    "links": [],
    "items": [],
    "created_at": "2017-10-17T18:20:35.134755+08:00",
    "updated_at": "2017-10-17T18:20:35.134802+08:00"
}
```

{% endmethod %}



{% method -%}

##更新菜单

```
PATCH /api/shops/menu/[name]/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
    "title": "123456789",
    "permanent": false, 
    "links": [], 
    "items": []
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shops/menu/mobile/
```

###返回结果

同查询指定菜单接口 返回完整的菜单信息

{% endmethod %}



{% method -%}

##删除菜单

```
DELETE /api/shops/menu/[name]/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/shops/menu/mobile/
```

###返回结果

同查询指定菜单接口 返回完整的菜单信息

{% endmethod %}



{% method -%}

##查询商店菜单项列表

```
GET /api/shops/menu/[name]/item/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/menu/mobile/item/
```

###返回结果

```json
[
    {
        "id": 182064,
        "menu_type": "page",
        "parent_id": null,
        "page_id": 55292,
        "position": 1,
        "title": "店铺",
        "url": "/",
        "target": "",
        "created_at": "2017-09-06T15:04:23.311114+08:00",
        "updated_at": "2017-09-06T15:04:23.311166+08:00"
    },
    {
        "id": 182065,
        "menu_type": "page",
        "parent_id": null,
        "page_id": 55293,
        "position": 2,
        "title": "商品",
        "url": "/search",
        "target": "",
        "created_at": "2017-09-06T15:04:23.334749+08:00",
        "updated_at": "2017-09-06T15:04:23.334816+08:00"
    },
    {
        "id": 182066,
        "menu_type": "page",
        "parent_id": null,
        "page_id": 55304,
        "position": 3,
        "title": "购物车",
        "url": "/cart",
        "target": "",
        "created_at": "2017-09-06T15:04:23.358033+08:00",
        "updated_at": "2017-09-06T15:04:23.358085+08:00"
    },
    {
        "id": 182067,
        "menu_type": "link",
        "parent_id": null,
        "page_id": null,
        "position": 4,
        "title": "我的",
        "url": "/account",
        "target": "",
        "created_at": "2017-09-06T15:04:23.373076+08:00",
        "updated_at": "2017-09-06T15:04:23.373150+08:00"
    }
]
```

{% endmethod %}



{% method -%}

##查询指定菜单项

```
GET /api/shops/menu/[name]/item/[id]/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/menu/mobile/item/182064/
```

###返回结果

```json
{
    "id": 182064,
    "menu_type": "page",
    "parent_id": null,
    "page_id": 55292,
    "position": 1,
    "title": "店铺",
    "url": "/",
    "target": "",
    "created_at": "2017-09-06T15:04:23.311114+08:00",
    "updated_at": "2017-09-06T15:04:23.311166+08:00"
}
```

{% endmethod %}



{% method -%}

##创建菜单项

```
POST /api/shops/menu/[name]/item/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"title": "test",
	"url": "www.test.com"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/menu/mobile/item/
```

###返回结果

同查询指定菜单项接口 返回完整的菜单项信息

{% endmethod %}



{% method -%}

##更新菜单项

```
PATCH /api/shops/menu/[name]/item/[id]/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"title": "test123",
	"url": "www.test.com"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shops/menu/mobile/item/182064/
```

###返回结果

同查询指定菜单项接口 返回完整的菜单项信息

{% endmethod %}



{% method -%}

##删除菜单项

```
DELETE /api/shops/menu/[name]/item/[id]/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/shops/menu/mobile/item/182064/
```

###返回结果

```josn
http 204
```

{% endmethod %}

