#店铺前端页面

{% method -%}

##查询店铺页面列表

```
GET /api/shops/page/
```

所需 scope: `shops:read` 或 `read`

### Query 参数

```
id: 1001
id__in: 1001,1002
name: test1
name__in: test1, test2店铺
created_at: 2017-02-01 00:00:00
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/page/?page=1
```

###返回结果

```json
{
    "count": 24,
    "next": "http://demo.heidianer.com/api/shops/page/?page=2",
    "previous": null,
    "results": [
        {
            "id": 262,
            "page_type": "home",
            "body_html": "",
            "permanent": true,
            "name": "C94A8D",
            "url": "/",
            "title": "首页",
            "description": "",
            "created_at": "2016-06-13T23:46:40+08:00",
            "updated_at": "2016-07-29T16:31:28+08:00",
            "meta_title": "首页",
            "meta_description": "",
            "meta_image": "",
            "pageconfig": {
                "id": 141193,
                "theme": 5548,
                "page": {
                    "id": 262,
                    "page_type": "home",
                    "permanent": true,
                    "name": "C94A8D"
                },
                "locked": true,
                "template_name": "",
                "settings_data": {
                    "components": [
                        {
                            "name": "blocks/header",
                            "settings_data": {
                                "text_color_affix": "rgba(255,255,255,1)",
                                "overlap_next_block": true,
                                "nav_position": "center",
                                "nav_has_dot": true,
                                "text_color": "rgba(255,255,255,1)",
                                "logo_centered": false,
                                "header_image_height": 50,
                                "cart_hidden": false,
                                "bg_colorstyle": "customize",
                                "two_rows": false,
                                "collapsed": false,
                                "bg_color": "rgba(0,0,0,0)",
                                "bg_color_affix": "rgba(0,0,0,1)",
                                "nav_has_underline": true
                            }
                        },
                        {
                            "name": "blocks/carousel",
                            "settings_data": {
                                "carousel_container": false,
                                "navigation_style": "none",
                                "animation": false,
                                "customize_ratio": true,
                                "effect": "fade",
                                "interval": 5000,
                                "carousel": [
                                    {
                                        "btn_text": {
                                            "value": "查看详情"
                                        },
                                        "btn_text_color": "rgba(255,255,255,1)",
                                        "btn_bg_color": "rgba(255,255,255,0)",
                                        "btn_text_color_hover": "rgba(0,0,0,1)",
                                        "image": {
                                            "src": "https://up.img.heidiancdn.com/o_1b3cf01agpjo1il514hc1b5r12m40banner1.jpg"
                                        },
                                        "btn_style": "rectangle",
                                        "image_small": {
                                            "metafield": {},
                                            "src": ""
                                        },
                                        "btn_bg_color_hover": "rgba(255,255,255,1)",
                                        "border_width": 2,
                                        "text_align": "updown",
                                        "show_text": false,
                                        "link": {
                                            "value": "/search"
                                        },
                                        "carousel_title": {
                                            "value": "花语",
                                            "style": {
                                                "color": "rgba(252,252,252,1)"
                                            }
                                        },
                                        "carousel_description": {
                                            "value": "物道 | 武夷山里的神奇树叶"
                                        },
                                        "show_btn": false,
                                        "colorstyle": "customize"
                                    },
                                    {
                                        "btn_text": {
                                            "value": "查看详情"
                                        },
                                        "btn_text_color": "rgba(255,255,255,1)",
                                        "btn_bg_color": "rgba(255,255,255,0)",
                                        "btn_text_color_hover": "rgba(0,0,0,1)",
                                        "image": {
                                            "src": "https://up.img.heidiancdn.com/o_1b3clnv8n2ip1v081eelasc11s3000c150b638db87a00ef7179bad678f52dada0323.jpg"
                                        },
                                        "btn_style": "rectangle",
                                        "image_small": {
                                            "metafield": {},
                                            "src": ""
                                        },
                                        "btn_bg_color_hover": "rgba(255,255,255,1)",
                                        "border_width": 2,
                                        "text_align": "updown",
                                        "show_text": false,
                                        "link": {
                                            "value": "/search"
                                        },
                                        "carousel_title": {
                                            "value": "花非花",
                                            "style": {
                                                "color": "rgba(255,252,252,1)"
                                            }
                                        },
                                        "carousel_description": {
                                            "value": "花非花 / 雾非雾"
                                        },
                                        "show_btn": false,
                                        "colorstyle": "customize"
                                    }
                                ],
                                "ratio": 0.5,
                                "min_height": 300,
                                "speed": 800,
                                "style": {
                                    "backgroundRepeat": "no-repeat",
                                    "paddingTop": "0",
                                    "backgroundSize": "cover",
                                    "paddingBottom": "0.00",
                                    "backgroundPosition": "center",
                                    "backgroundImage": ""
                                },
                                "arrow_style": "circle"
                            }
                        },
                        {
                            "name": "blocks/featured_articles",
                            "settings_data": {
                                "column_count": 3,
                                "article_style": "swiper",
                                "container_name": "container",
                                "style": {
                                    "paddingTop": "30.00",
                                    "paddingBottom": "30.00"
                                },
                                "articles": {
                                    "page_size": "4",
                                    "order_by": "-published_at"
                                }
                            }
                        },
                        {
                            "name": "blocks/featured_collections",
                            "settings_data": {
                                "collections": {
                                    "page_size": "4"
                                },
                                "collection_style": "4col",
                                "hover_title": true
                            }
                        },
                        {
                            "name": "blocks/featured_collections",
                            "settings_data": {
                                "collections": {
                                    "page_size": "4"
                                },
                                "collection_style": "slider",
                                "hover_title": true
                            }
                        },
                        {
                            "name": "blocks/title",
                            "settings_data": {
                                "subtitle_align": "center",
                                "divider_style": "devider_none",
                                "show_link": true,
                                "link_align": "center",
                                "title_align": "center",
                                "section_title": {
                                    "value": "品牌制造商"
                                },
                                "is_container": true,
                                "two_rows": false,
                                "textlink": {
                                    "href": "/search",
                                    "value": "更多品牌商"
                                },
                                "deco_link": "double-arrow",
                                "style": {
                                    "paddingTop": "0.00",
                                    "paddingBottom": "0.00"
                                },
                                "section_subtitle": {
                                    "value": "工厂直达消费者，剔除品牌溢价"
                                }
                            }
                        },
                        {
                            "name": "blocks/featured_products",
                            "settings_data": {
                                "hover_style": "multi_images",
                                "column_count": 4,
                                "container_name": "container",
                                "badge_text_color": "rgba(255, 255, 255, 1)",
                                "ratio": "100",
                                "column_count_mobile": 2,
                                "badge_bg_color": "rgba(0, 0, 0, 1)",
                                "products": {
                                    "page_size": "8"
                                }
                            }
                        },
                        {
                            "name": "blocks/logos",
                            "settings_data": {
                                "is_container": false,
                                "nowrap": false,
                                "bgColor": "rgba(0,0,0,0.7)",
                                "icons": [
                                    {
                                        "link": {
                                            "value": ""
                                        },
                                        "image": {
                                            "metafield": {},
                                            "src": "https://up.img.heidiancdn.com/o_1b3cncqf92ce1cqtpvsea91ico0amazon.png?imageMogr2/thumbnail/!71p|imageMogr2/crop/!177x52a0a0"
                                        },
                                        "name": {
                                            "value": ""
                                        }
                                    },
                                    {
                                        "link": {
                                            "value": ""
                                        },
                                        "image": {
                                            "metafield": {},
                                            "src": "https://up.img.heidiancdn.com/o_1b3cncqfa3s9157jepmo7t1as10disnep.png?imageMogr2/thumbnail/!71p|imageMogr2/crop/!153x63a0a0"
                                        },
                                        "name": {
                                            "value": ""
                                        }
                                    },
                                    {
                                        "link": {
                                            "value": ""
                                        },
                                        "image": {
                                            "metafield": {},
                                            "src": "https://up.img.heidiancdn.com/o_1b3cncqfc1n1qklj1daj87v1enp0zhaoshangyinhang.png?imageMogr2/thumbnail/!71p|imageMogr2/crop/!216x56a0a0"
                                        },
                                        "name": {
                                            "value": ""
                                        }
                                    },
                                    {
                                        "link": {
                                            "value": ""
                                        },
                                        "image": {
                                            "metafield": {},
                                            "src": "https://up.img.heidiancdn.com/o_1b3cncqfatumpe012911bpu1c0s0lenovo.png?imageMogr2/thumbnail/!71p|imageMogr2/crop/!211x47a0a0"
                                        },
                                        "name": {
                                            "value": ""
                                        }
                                    },
                                    {
                                        "link": {
                                            "value": ""
                                        },
                                        "image": {
                                            "metafield": {},
                                            "src": "https://up.img.heidiancdn.com/o_1b3cnh90h188a6b932jvr1p360qiaqia.png?imageMogr2/thumbnail/!71p|imageMogr2/crop/!143x64a0a0"
                                        },
                                        "name": {
                                            "value": ""
                                        }
                                    }
                                ],
                                "icon_margin": 20,
                                "icon_height": 25,
                                "width_percent": 90,
                                "section_title": {
                                    "value": "我们的合作伙伴",
                                    "style": {
                                        "color": "rgba(255,255,255,1)"
                                    }
                                },
                                "style": {
                                    "backgroundRepeat": "no-repeat",
                                    "paddingTop": "0.00",
                                    "backgroundSize": "cover",
                                    "backgroundPosition": "center",
                                    "backgroundAttachment": "fixed",
                                    "backgroundImage": "https://up.img.heidiancdn.com/o_1b3cf120gjsgt3qg5h1hmkaf40%E6%88%91%E4%BB%AC%E7%9A%84%E5%90%88%E4%BD%9C%E5%95%86%E6%88%B7.jpg"
                                }
                            }
                        },
                        {
                            "name": "blocks/footer",
                            "settings_data": {}
                        }
                    ]
                }
            }
        },
        ...
      ]
}
```

{% endmethod %}



{% method -%}

##查询指定店铺页面信息

```
GET /api/shops/page/[id]/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/page/263/
```

###返回结果

```json
{
    "id": 263,
    "page_type": "search",
    "body_html": "",
    "permanent": true,
    "name": "C7D21B",
    "url": "/search",
    "title": "商品",
    "description": "",
    "created_at": "2016-06-13T23:46:40+08:00",
    "updated_at": "2016-07-29T16:31:29+08:00",
    "meta_title": "商品",
    "meta_description": "",
    "meta_image": "",
    "pageconfig": {
        "id": 141195,
        "theme": 5548,
        "page": {
            "id": 263,
            "page_type": "search",
            "permanent": true,
            "name": "C7D21B"
        },
        "locked": true,
        "template_name": "",
        "settings_data": {
            "components": [
                {
                    "name": "blocks/header",
                    "settings_data": {
                        "logo_centered": false,
                        "text_color_affix": "rgba(255,255,255,1)",
                        "overlap_next_block": false,
                        "nav_position": "center",
                        "nav_has_dot": true,
                        "text_color": "rgba(255,255,255,1)",
                        "two_rows": false,
                        "header_image_height": 50,
                        "cart_hidden": false,
                        "bg_colorstyle": "dark",
                        "collapsed": false,
                        "bg_color": "rgba(0,0,0,1)",
                        "bg_color_affix": "rgba(0,0,0,1)",
                        "style": {
                            "paddingTop": "0.00",
                            "paddingBottom": "0.00"
                        },
                        "nav_has_underline": true
                    }
                },
                {
                    "name": "blocks/product_list",
                    "settings_data": {
                        "hover_style": "badges",
                        "column_count": 3,
                        "badge_text_color": "rgba(255, 255, 255, 1)",
                        "order_by": "default",
                        "ratio": "100",
                        "column_count_mobile": 2,
                        "badge_bg_color": "rgba(0, 0, 0, 1)",
                        "auto_scroll": true
                    }
                },
                {
                    "name": "blocks/footer",
                    "settings_data": {}
                }
            ]
        }
    }
}
```

{% endmethod %}



{% method -%}

##创建新店铺页面

```
POST /api/shops/page/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"name": "test",
    "title": "test",
    "description": "测试页面",
    "meta_title": "test",
    "meta_description": "1234",
    "meta_image": "1234"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/page/
```

###返回结果

同查询指定店铺页面信息 返回完整的页面信息

{% endmethod %}



{% method -%}

##更新店铺页面

```
PATCH /api/shops/page/[id]/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"name": "test",
    "title": "test",
    "description": "测试页面",
    "meta_title": "test",
    "meta_description": "1234",
    "meta_image": "1234"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shops/page/263/
```

###返回结果

同查询指定店铺页面信息 返回完整的页面信息

{% endmethod %}



{% method -%}

##删除店铺页面

```
DELETE /api/shops/page/[id]/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/shops/page/263/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##批量删除店铺页面

```
DELETE /api/shops/page/
```

所需 scope: `shops:write` 或 `write`

### Query 参数

```
id: 1001
id__in: 1001,1002
name: test1
name__in: test1, test2
created_at: 2017-02-01 00:00:00
page: 1
page_size: 10
```


{% sample lang="http" -%}

###发送请求

```
DELETE /api/shops/page/?page=1
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##复制店铺页面

```
POST /api/shops/page/[id]/duplicate/
```

所需 scope: `shops:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/page/263/duplicate/
```

###返回结果

同查询指定店铺页面信息 返回完整的页面信息

{% endmethod %}