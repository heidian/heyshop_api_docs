#店铺信息

{% method -%}

##查询店铺信息（无需shop权限）

```
GET /api/shopfront/shop/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopfront/shop/
```

###返回结果

```json
{
    "permissions": {
        "remove_shopfront_ads": false
    },
    "page": null,
    "shop": {
        "id": 3744,
        "name": "ywu18",
        "title": "综合商城",
        "slogan": "",
        "description": "",
        "logo": "https://up.img.heidiancdn.com/o_1b3cevpmddit1q01k8cqnrdmt0Logo-white.png",
        "favicon": "https://up.img.heidiancdn.com/o_1b3cevpmbgelqf6jdp66gjpp0Logo-dark.png",
        "domain": "test124.cn",
        "company": "ywu18",
        "website": "",
        "address": "",
        "email": "731xxxxx1@163.com",
        "mobile": "139xxxxx923",
        "language": "zh",
        "customer_email": "73xxxx61@163.com",
        "customer_phone": "139xxxx923",
        "weixin_qr": "",
        "weibo_id": "",
        "beian_number": "",
        "version": 255,
        ...

 			/*
 				该接口返回数据过多，用户可自行调用查看
 			*/
}
```

{% endmethod %}



{% method -%}

##查询店铺设置信息（需要shop权限）

```
GET /api/shops/shop_settings/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/shop_settings/
```

###返回结果

```json
{
    "id": 3744,
    "name": "ywu18",
    "title": "综合商城",
    "slogan": "",
    "description": "",
    "logo": "https://up.img.heidiancdn.com/o_1b3cevpmddit1q01k8cqnrdmt0Logo-white.png",
    "favicon": "https://up.img.heidiancdn.com/o_1b3cevpmbgelqf6jdp66gjpp0Logo-dark.png",
    "domain": "test124.cn",
    "company": "ywu18",
    "website": "",
    "address": "",
    "email": "731xxxxx61@163.com",
    "mobile": "139xxxx923",
    "language": "zh",
    "customer_email": "73xxxx061@163.com",
    "customer_phone": "139xxxxx923",
    "weixin_qr": "",
    "weibo_id": "",
    "beian_number": "",
    "version": 255,
    ...

    	/*
 			该接口返回数据过多，用户可自行调用查看
 		*/
}
```

{% endmethod %}


{% method -%}

##新手引导

```
GET /api/shops/onboarding/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/onboarding/
```

###返回结果

```json
{
    "has_payment_backend": false,		//配置收款
    "has_coupon": true,					//创建优惠活动
    "is_selected_theme": true,		//选择主题
    "has_product": true,				//添加商品
    "has_social_account": false,		//链接社交媒体
    "is_uploaded_logo": true,			//上传Logo
    "has_custom_domain": false,		//使用自定义域名
    "has_product_category": false,	//管理商品分类
    "has_navigation": false,			//设置导航菜单
    "has_shipping_method": true		//设置运费模版

}
```

{% endmethod %}



{% method -%}

##设置新手引导

```
PATCH /api/shops/onboarding/
```

所需 scope: `shops:read` 或 `read`

###数据格式

```json
{
    "has_payment_backend": false,		//配置收款
    "has_coupon": true,					//创建优惠活动
    "is_selected_theme": true,		//选择主题
    "has_product": true,				//添加商品
    "has_social_account": false,		//链接社交媒体
    "is_uploaded_logo": true,			//上传Logo
    "has_custom_domain": false,		//使用自定义域名
    "has_product_category": false,	//管理商品分类
    "has_navigation": false,			//设置导航菜单
    "has_shipping_method": true		//设置运费模版
}
```


{% sample lang="http" -%}

###发送请求

```
PATCH /api/shops/onboarding/
```

###返回结果

```json
http 204
```

{% endmethod %}