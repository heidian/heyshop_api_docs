#身份验证

{% method -%}

##查询身份验证

```
GET /api/shops/identification/
```

所需 scope: `shops:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shops/identification/
```

###返回结果

```json
{
    "id": 551,	//若没有经过认证，返回结果为{}
    "identification_type": "personal",
    "status": "pending",
    "full_name": "test",
    "identification_number": "111",
    "identification_photo": "",
    "mobile": "12345678901",
    "alipay": "",
    "representative": "",
    "representative_id_photo": "",
    "representative_id_photo2": "",
    "business_scopes": "",
    "pingxx_available_channels": [],
    "created_at": "2017-10-17T16:47:34.784919+08:00",
    "updated_at": "2017-10-17T16:47:35.015829+08:00"
}
```

{% endmethod %}


{% method -%}

##申请身份验证

```
POST /api/shops/identification/
```

所需 scope: `shops:write` 或 `write`

###数据格式

```json
{
	"identification_type":"personal",
	"full_name": "test",
    "identification_number": "111",
    "identification_photo": "",
    "mobile": "12345678901"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shops/identification/
```

###返回结果

```json
{
    "id": 551,
    "identification_type": "personal",
    "status": "pending",
    "full_name": "test",
    "identification_number": "111",
    "identification_photo": "",
    "mobile": "12345678901",
    "alipay": "",
    "representative": "",
    "representative_id_photo": "",
    "representative_id_photo2": "",
    "business_scopes": "",
    "pingxx_available_channels": [],
    "created_at": "2017-10-17T16:47:34.784919+08:00",
    "updated_at": "2017-10-17T16:47:35.015829+08:00"
}
```

{% endmethod %}




