
# 发票

{% method -%}

## 创建发票

```
POST /api/pricing/invoice/
```

所需 scope: `pricing:write` 或 `write`

### 数据格式
```json
{
    "amount": "2000",
    "title": "个人",
    "mobile": "13412345678",
    "full_name": "然叔",
    "address": "上海市静安区。。。"
}
```

{% sample lang="http" -%}

###发送请求 

```
POST /api/pricing/invoice/
```

### 返回结果

```json
{
    "id": 1,
    "status": "pending",
    "amount": "2000.00",
    "title": "个人",
    "mobile": "13412345678",
    "full_name": "然叔",
    "address": "上海市静安区。。。",
    "tracking_company": "",
    "tracking_number": ""
}
```

{% endmethod %}


{% method -%}

## 获取发票列表

```
GET /api/pricing/invoice/
```

所需 scope: `pricing:read` 或 `read`

### Query 参数
```
page: 1
page_size: 10
```


{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/invoice/?page=1
```

### 返回结果
```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results":[
        {
            "id": 1,
            "status": "pending", // "approved" "rejected"
            "amount": "2000.00",
            "title": "个人",
            "mobile": "13412345678",
            "full_name": "然叔",
            "address": "xxxxxxxxx",
            "tracking_company": "申通快递",
            "tracking_number": "227718100591"
        }
    ]
}
```

{% endmethod %}



{% method -%}

## 获取可开票金额

```
GET /api/pricing/invoice/balance/
```

所需 scope: `pricing:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/invoice/balance/
```

### 返回结果
```json
{
    "balance": 1990.5  // 可开票金额
}
```

{% endmethod %}




