#发起付款请求

你可以通过调用 `url:` `/api/pingxx/pay_for_order/` 发起 `POST`请求，服务器将返回付款的html页面，向准备购买的商品发起付款。一个 `pay_for_order`对象包含下列属性。
  

|    属性   |    属性说明   | 
|   :-------    |  :---------   |
|  order_token  |   订单对象的Token，可以在 `购物车／确认订单／支付订单` 页面的URL中找到，为 `POST` 请求的必填字段  |  
|       范例        |  42f54386e00d7651d85b9b36d2077c8b34acf455   |
|    channel    |   支付方式，嘿店支持支付宝 `alipay_pc_direct`, `alipay_wap` 和微信 `wx_pub ` 三种收款方式，非必填字段  | 
|       范例        |   wx_pub  |
|  success_url  |   订单成功付款后，页面跳转到订单详情页 |
|       范例     |   `http://demo.heidianer.com/order/42f54386e00d7651d85b9b36d2077c8b34acf455`  |
|  cancel_url   |   订单因故未能成功付款后，页面跳转到订单详情页  |
|       范例     |   `http://demo.heidianer.com/order/42f54386e00d7651d85b9b36d2077c8b34acf455`  | 
|	voucher_ids  |	代金券ID，由店主创建代金券时生成 |
|       范例     |	NULL	|



