#订单测试

{% method -%}

##生成测试订单

```
POST /api/pingxx/test_order/
```

所需 scope：`pingxx:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/pingxx/test_order/
```

###返回结果

```json
{
    "id": 25748,
    "order_number": "384520171013112112549157",
    "token": "b53671ad742d0e4f76a6cc50143f2a2fb67536ab",
    "cart_token": "",
    "requires_shipping": false,
    "full_name": "测试订单",
    "email": "",
    "mobile": "",
    "buyer_accepts_marketing": true,
    "total_line_items_price": "0.00",
    "total_discounts": "0.00",
    "shipping_cost": "0.00",
    "total_weight": "0.00",
    "total_price": "0.01",
    "order_status": "checkout",
    "fulfillment_status": "pending",
    "financial_status": "pending",
    "note": "",
    "metafield": {},
    "created_at": "2017-10-13T11:21:12.551666+08:00",
    "updated_at": "2017-10-13T11:21:12.551709+08:00",
    "shipping_address": null,
    "lines": [],
    "discounts": [],
    "customer": null,
    "is_test": true,
    "is_fulfillment_silent": false,
    "fulfilled_at": null
}
```

{% endmethod %}



{% method -%}

##查询测试订单状态

```
GET /api/pingxx/test_order/status/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/test_order/status/
```

###返回结果

```json
{
    "count": 3,
    "results": [
        {
            "channel": "alipay_pc_direct",
            "status": null       //未测试
        },
        {
            "channel": "wx_pub",
            "status": "failure"    //测试失败
        },
        {
            "channel": "alipay_wap",
            "status": "success"      //测试成功
        }
    ]
}
```

{% endmethod %}