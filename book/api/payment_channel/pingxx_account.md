#Ping++账户

**此部分为旧版本接口，今后不再使用**

{% method -%}

##查询Ping++配置信息

```
GET /api/pingxx/config/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/config/
```

###返回结果

```json
{
    "api_live_key": "xxxxxx",
    "api_test_key": "",
    "app_id": "app_Duvxxxxxx9KKOCKK",
    "api_public_key": "-----BEGIN PUBLIC KEY-----xxxxxxxx-----END PUBLIC KEY-----",
    "available_channels": [
        "wx_pub",
        "alipay_wap",
        "alipay_pc_direct"
    ],
    "is_bound": false,
    "identifier_key": "partner_8b9xxxxxxxx1cdada023b0",
    "enabled_channel": [
        "alipay",
        "wx",
        "wx_pub_qr",
        "alipay_qr",
        "alipay_wap",
        "alipay_pc_direct"
    ],
    "is_signed": true,
    "wx_app_id": "wx3b0f9xxxxxfc3ab10",
    "wx_key": "8da9c41fbbxxxxxx63576e43f6d1354"
}
```

{% endmethod %}



{% method -%}

##更新Ping++配置信息

```
PATCH /api/pingxx/config/
```

所需 scope：`pingxx:write` 或 `write`

###数据结构

```json
{
    "available_channels": [
        "wx_pub",
        "alipay_wap",
        "alipay_pc_direct"
    ]
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/pingxx/config/
```

###返回结果

同查询Ping++配置信息接口 返回完整的配置信息

{% endmethod %}