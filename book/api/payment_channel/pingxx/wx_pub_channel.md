#微信支付

{% method -%}

##查询微信支付配置信息

```
GET /api/pingxx/sub_app/channel/wx_pub/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/sub_app/channel/wx_pub/
```

###返回结果

```json
{
    "wx_pub_operator_id": "dxdspirits@xxxxxxx",
    "wx_pub_app_id": "wx3b0xxxxx5fc3ab10",
    "wx_pub_client_cert": "-----BEGIN CERTIFICATE-----\nMIIEaDCCA9GgAwIBAgIDKsqIMA0GCSqGSIb3DQEBBQUAMIGKMQswCQYDVQQGEwJD\nTjESMBAGA1UECBMJR3Vhbmdkb25nMREwDwYDVQQHEwhTaGVuemhlbjEQMA4GA1UE\nChMHVGVuY2VudDEMMAoGA1UECxMDV1hHMRMwEQYDVQQDEwpNbXBheW1jaENBMR8w\nHQYJKoZIhvcNAQkBFhBtbXBheW1jaEB0ZW5jZW50MB4XDTE2MDYwMzA1NDAyMloX\nDTI2MDYwMTA1NDAyMlowgZgxCzAJBgNVBAYTAkNOMRIwEAYDVQQIEwlHdWFuZ2Rv\nbmcxETAPBgNVBAcTCFNoZW56aGVuMRAwDgYDVQQKEwdUZW5jZW50MQ4wDAYDVQQL\nEwVNTVBheTEtMCsGA1UEAxQk5LiK5rW36aOO5Yiw572R57uc56eR5oqA5pyJ6ZmQ\n5YWs5Y+4MREwDwYDVQQEEwgxMjY2NTM3NzCCASIwDQYJKoZIhvcNAQEBBQADggEP\nADCCAQoCggEBANgdtR/OyZ5Usi93maqehEeQsLf0GwLB3vCRm21WhaKP/DH2SaUZ\n1mIqeLhZU1+y0rvoxUihh2CxjFg0yKC05IHljbzpxr/u27e3xfIJu7SAlnYM/6/2\nPUZyxEbICWQW8eDTyVC6iT8PxhCdgrTaZV3ApF8oXS8RlcCMZE+4JP8N/IPhMmpD\nfDpSDVGkLNb1FQJucQTN3zCLzBB/t+fRzSSVrgtC+82cf7sew9UNR6iRkPhe0F3p\nyBpXqXyjSJtcl5p+LoDf91GAYwOLN+d++gcociufeFSZVGMTpXaV7R/1NwxuU/VI\nfDJhHTwsSp9IRfLfk24WbGmBC4tSaywUPzMCAwEAAaOCAUYwggFCMAkGA1UdEwQC\nMAAwLAYJYIZIAYb4QgENBB8WHSJDRVMtQ0EgR2VuZXJhdGUgQ2VydGlmaWNhdGUi\nMB0GA1UdDgQWBBRNM27iZqufhMnlrEILAkj6kI+bFDCBvwYDVR0jBIG3MIG0gBQ+\nBSb2ImK0FVuIzWR+sNRip+WGdKGBkKSBjTCBijELMAkGA1UEBhMCQ04xEjAQBgNV\nBAgTCUd1YW5nZG9uZzERMA8GA1UEBxMIU2hlbnpoZW4xEDAOBgNVBAoTB1RlbmNl\nbnQxDDAKBgNVBAsTA1dYRzETMBEGA1UEAxMKTW1wYXltY2hDQTEfMB0GCSqGSIb3\nDQEJARYQbW1wYXltY2hAdGVuY2VudIIJALtUlyu8AOhXMA4GA1UdDwEB/wQEAwIG\nwDAWBgNVHSUBAf8EDDAKBggrBgEFBQcDAjANBgkqhkiG9w0BAQUFAAOBgQBYuyR2\nYEyVeZzMlW2dvcTOpgTA1Tz6wqzqOk72xDIcj1njlRXz54K0fTFgHq5VPqzvh6CO\nsOTHadJYxJxltTtJF7ee2lb3RbbRHsNcBJp8xV3svyYzA7eDNt1gceD9x060vA6d\nMNq2kB3tTBaagXGDBXqa+mE2DgcXkuKlMoGggg==\n-----END CERTIFICATE-----",
    "wx_pub_client_key": "-----BEGIN PRIVATE KEY-----\nMIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDYHbUfzsmeVLIv\nd5mqnoRHkLC39BsCwd7wkZttVoWij/wx9kmlGdZiKni4WVNfstK76MVIoYdgsYxY\nNMigtOSB5Y286ca/7tu3t8XyCbu0gJZ2DP+v9j1GcsRGyAlkFvHg08lQuok/D8YQ\nnYK02mVdwKRfKF0vEZXAjGRPuCT/DfyD4TJqQ3w6Ug1RpCzW9RUCbnEEzd8wi8wQ\nf7fn0c0kla4LQvvNnH+7HsPVDUeokZD4XtBd6cgaV6l8o0ibXJeafi6A3/dRgGMD\nizfnfvoHKHIrn3hUmVRjE6V2le0f9TcMblP1SHwyYR08LEqfSEXy35NuFmxpgQuL\nUmssFD8zAgMBAAECggEBAMRWQN2TzOF1yqeK/WTfPAeUMKf+MafQn4xojA7KIv/r\n1U9Ck+dVUMnAEgpQxIjVedPTBaAVd1rmEj10PqyZiltkQ99TdweZpa0DXOal5wx2\nIbs/1e5zQ3nD/ALd6oIuwjHh1ivsO5G579PadQt+ucZxpCL7FaUUUbsrEFrITXwT\n+cxK2QxpS2xZDFRlietgtCctEGzpxByMHu7bVJogkncIFfnPNDKITcuKyLWgBSZj\nKNKy9BU5TUiByT19iLzrXHQYQnwpRUa6Gutm6k0q6N9xVDFZw8JmarEuPWA/4487\n+OfYTzY5Eku1O2iwxZJmz7U4bxg4rRyi6395cAhi4xECgYEA+DA9jqkzbAqiuGxW\n4qPXS1uxz6kA23TxFFCB1QvXJB4ygmCwk2JB4FzYgINA0dQlsX5kfYZ7KmFIIg5i\nYS345WkBcbXXFhoEEGRtRr4M5bl8Z1g+1gQwgQYFRpCdPBPUYXcPjcD1cgpdsNqL\nN5q+Y+GCDady/C9xLY3pueaRm3kCgYEA3usL0es//Ql0Rs7XQMeFsA9cTkejgmr8\n/V3WRWKQOieOpi2c+OP3k9UzpgJDkTri4i59KQJLTx7wATOM+BUYOMa+e1LzB/UC\ntiLQMjLncISCc+y5cr7rb/OpgGdfyVB6dOsYxUGUQndWGPjFhZC7qiDplYKRB6k2\npUVTeKQR2QsCgYEAjAz8z6IfSFqAFD3ekaoO9ZFat2oQ98X3vJTpjsVNtC3uJrBV\nM/YAYpFCAtMOItnqI0zFfCgFHJck00L2tQvyJdKpY/dO1kP8AxwIpiDcuk1/K/oa\nCz719/jjxTwZ3ir6b71143H9j0rWL9RDNy91PHumnfnd4v0KisBIIgVAKVECgYEA\nl8qRh+Wa8uCHLtgVtuSl4dbz0dHyQxI7JwPIrgKJly/scLfKlagVo7KfssED5Val\nu8JQM6Z+RWgZoZfKu/sRPUgH2M4I8zghGojmH+iNtA5nDiz5smrSwq62ktrRXLHS\nUN7tRRR8si20kNiFF4gQYa57n/UCcKcGsM9eIV4HUTUCgYEAyMWcayTbA4UX7jLX\nMSiAOYCDnjPjM6vfgogB0U5sS67KvcKvjXv6fuDxq/Bd9RTAsiyLltrteuG2siiP\n1cutqPS2QpeVXH+EDBemoQKGmtVacOxyOSptVTmV3BKCx6hO9EP/VhJXtqmnc0DL\njcxxBpWWT8G6tpa6YJJA1rNVtnk=\n-----END PRIVATE KEY-----",
    "wx_pub_app_secret": "8da9c41fbb31787d563576e43f6d1354",
    "wx_pub_mch_id": "1350593201",
    "wx_pub_key": "0d3e0ede14242de1014bd6156b1afe3c"
}
```
{% endmethod %}



{% method -%}

##查询微信支付配置信息

```
PATCH /api/pingxx/sub_app/channel/wx_pub/
```

所需 scope：`pingxx:write` 或 `write`

###数据格式

```json
{
    "wx_pub_operator_id": "dxdspirits@x135xxxxxx01",
    "wx_pub_app_id": "wx3b0xxxxxxxxxab10",
    "wx_pub_client_cert": "-----BEGIN CERTIFICATE-----xxxxxxxxx-----END CERTIFICATE-----",
    "wx_pub_client_key": "-----BEGIN PRIVATE KEY-----xxxxxxxxx-----END PRIVATE KEY-----",
    "wx_pub_app_secret": "8da9c41fbxxxxxxxxxxxx6e43f6d1354",
    "wx_pub_mch_id": "135xxxx201",
    "wx_pub_key": "0d3e0edexxxxxxxxxxxbd6156b1afe3c"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/pingxx/sub_app/channel/wx_pub/
```

###返回结果

```json
http204
```

{% endmethod %}
