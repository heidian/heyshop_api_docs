#支付宝（PC端）

{% method -%}

##查询手机端支付宝配置信息

```
GET /api/pingxx/sub_app/channel/alipay_pc_direct/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/sub_app/channel/alipay_pc_direct/
```

###返回结果

```json
{
    "alipay_account": "pay@test.com",
    "alipay_security_key": "6kbavog1xxxxxxxxxjexgfasx1cl1x",
    "alipay_pid": "2088xxxxx40749",
    "alipay_private_key": null,
    "alipay_refund_nopwd": false,
    "alipay_public_key": null
}
```


{% endmethod %}



{% method -%}

##更新手机端支付宝配置信息

```
PATCH /api/pingxx/sub_app/channel/alipay_pc_direct/
```

所需 scope：`pingxx:write` 或 `write`

###数据格式

```json
{
    "alipay_account": "pay@test.com",
    //字符串长度限制为32位
    "alipay_security_key": "6kbavog1w8gcpdxxxxxxxxxfasx1cl1x",	
    //id长度限制为16位	
    "alipay_pid": "20886xxxxxx40749"，	

    "alipay_private_key": "-----BEGIN RSA PRIVATE KEY-----xxxxx-----END RSA PRIVATE KEY-----",
    "alipay_refund_nopwd": false,
    "alipay_public_key": "-----BEGIN PUBLIC KEY-----xxxxx-----END PUBLIC KEY-----"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/pingxx/sub_app/channel/alipay_pc_direct/
```

###返回结果

```json
http204
```

{% endmethod %}
