#Ping++子账户

{% method -%}

##查询Ping++子账户配置信息

```
GET /api/pingxx/sub_app/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/sub_app/
```

###返回结果

```json
{
    "sub_app_id": "app_bfH0KOmT8GePnTiP",
    "user": "shop_1345_1",
    "params": {
        "alipay_refund_nopwd": false,
        "alipay_app_id": "",
        "alipay_security_key": "6kbavog1w8gcpd18rdijexgfasx1cl1x",
        "alipay_version": 1,
        "alipay_mer_wap_private_key": "-----BEGIN RSA PRIVATE KEY-----\nMIICXQIBAAKBgQD1yq2XfULUFKkjJY4evxy0YRku21FCQC761igi4zElXYvffT76\npxlxnkorXZXOnUx311qhtmG6mNN5l7aZ9D0cIIUKmwk527f5/hpCHbpP+pI1LgmJ\n23DOIAhEOKikMnK+FvLScPtZ9mS5s1zvQLmc7SaPr/Y8eEocaJ3vt5uilwIDAQAB\nAoGADji78pf81eicILx7jKviLU2w91UZTowIK9tYVyxuG4teHIBStI4Wmsxd3kR7\n9oiDdCuxwsmavgE5uz9cAWxwXKM9U/0lP2AO3bvH3jXjE9hR4ouqm1lposwZ2+pc\nZ64pODm2+9hMHkdIWa/sPqQujcJywprzRNYCiROJry5N5IECQQD9tqY8qSG9SV4Q\nU6zUT55fnmwrZlb9bBgLs0j7a22TX/xwfP9zMvv+X0wLKyEecNYqZKzFjltoBgaI\nQl+SFsulAkEA+AHAjlwCKEW2NtlaB3CEjIX0YeJ0fHd3CtYPej88Y5ptMu+CBvdz\nV7VuLbwp62KRw81tVPInum9amOFSCaHQiwJBAKKBb/7gshyktpwsjEG88Yr7VPuD\nZGqT5t7vqepoB9pim7WNSq2F1YQ7k+B/cool6fdjXop3QTLi0SJLqR1QfoUCQQDG\nJ0g9tl/243QqMNGQ/iK50FGpX/FofV885jmpmBrdDkbPmrQOcunSm2Cz5BKuQqxq\ngJkpdO2vNUdyvRz6GzLvAkBGF9sHfpReHtsaDUI0npmz2rns92lXaJzoe+h0x7vu\nLKUVy1BUM9qn7x7fq8sKil/ns6+4Mb4VChnSKfQXRx/U\n-----END RSA PRIVATE KEY-----",
        "alipay_account": "pay@wedfairy.com",
        "alipay_wap_public_key": "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgkludQaaaNi+MXQp6DILcMTf1\ngIJbeoYQo6OM EcIwo0IROtiJ6eljQwj9l7RldxrI1qDvgI9hwfYTRQ4EyGZI5Sw\nD9Mfq7eqQGecGJ3zo8JAM/sS4 ts/PtacZm+hsflQ/hF150LBvO7SOpmVvgN4CYW\nsdEtzQk1Xzo7Pntc1vuQIDAQAB\n-----END PUBLIC KEY-----",
        "alipay_pid": "2088612364840749"
    },
    "available_channels": [
        "alipay_wap"
    ],
    "enabled_channels": [
        "alipay_wap"
    ]
}
```

{% endmethod %}



{% method -%}

##更新Ping++子账户配置信息

```
PACTCH /api/pingxx/sub_app/
```

所需 scope：`pingxx:write` 或 `write`

###数据格式

```json
{
		"available_channels": [
        "alipay_wap"
    ]
}
```

{% sample lang="http" -%}

###发送请求

```
PACTCH /api/pingxx/sub_app/
```

###返回结果

同查询Ping++配置信息接口 返回完整的配置信息

{% endmethod %}



{% method -%}

##查询Ping++配置信息

```
GET /api/pingxx/config/
```

所需 scope：`pingxx:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pingxx/config/
```

###返回结果

```json
{
    "api_live_key": "xxxxxx",
    "api_test_key": "",
    "app_id": "app_Duvxxxxxx9KKOCKK",
    "api_public_key": "-----BEGIN PUBLIC KEY-----xxxxxxxx-----END PUBLIC KEY-----",
    "available_channels": [
        "wx_pub",
        "alipay_wap",
        "alipay_pc_direct"
    ],
    "is_bound": false,
    "identifier_key": "partner_8b9xxxxxxxx1cdada023b0",
    "enabled_channel": [
        "alipay",
        "wx",
        "wx_pub_qr",
        "alipay_qr",
        "alipay_wap",
        "alipay_pc_direct"
    ],
    "is_signed": true,
    "wx_app_id": "wx3b0f9xxxxxfc3ab10",
    "wx_key": "8da9c41fbbxxxxxx63576e43f6d1354"
}
```

{% endmethod %}



{% method -%}

##更新Ping++配置信息

```
PATCH /api/pingxx/config/
```

所需 scope：`pingxx:write` 或 `write`

###数据结构

```json
{
    	"available_channels": [
        "wx_pub",
        "alipay_wap",
        "alipay_pc_direct"
    ]
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/pingxx/config/
```

###返回结果

同查询Ping++配置信息接口 返回完整的配置信息

{% endmethod %}