
# 用户管理


{% method -%}

## 获取用户列表

```
GET /api/crm/customer/
```

所需 scope: `crm:read` 或 `read`

### Query 参数
```
id: 1
id__in: 1,2
coupon: 222
coupon__exclude: 111
segment: 123
segment__exclude: 123
page: 1
page_size: 10
```


{% sample lang="http" -%}

###发送请求

```
GET /api/crm/customer/?page=1
```

### 返回结果
```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 2,
            "mobile": "13821761807",
            "full_name": "Xindong Ding",
            "gender": "male",
            "email": "dxd.spirits@gmail.com",
            "birthday": "1986-05-24",
            "note": "",
            "accepts_marketing": true,
            "tags": [],
            "addresses": [],
            "segments": [],
            "created_at": "2016-08-07T14:54:28Z"
        },
        // ...
    ]
}
```

{% endmethod %}


{% method -%}

## 获取用户信息

```
GET /api/crm/customer/[id]/
```

所需 scope: `crm:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/crm/customer/2/
```

### 返回结果
```json
{
    "id": 2,
    "mobile": "13821761807",
    "full_name": "Xindong Ding",
    "gender": "male",
    "email": "dxd.spirits@gmail.com",
    "birthday": "1986-05-24",
    "note": "",
    "accepts_marketing": true,
    "tags": [],
    "addresses": [],
    "segments": [],
    "created_at": "2016-08-07T14:54:28Z"
}
```

{% endmethod %}



{% method -%}

## 添加新用户

```
POST /api/crm/customer/
```

所需 scope: `crm:write` 或 `write`

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    mobile       |  str型，手机号    |
|    full_name       |  str型，用户名    |
|    gender       |  str型，用户性别，可选项：'unknown', 'male', 'female'    |
|    email       |  str型，用户邮箱    |
|    birthday       |  用户生日    |
|    note       |  str型，用户备注    |
|    accepts_marketing       |  Boolean型，是否需要快递    |
|    referral_code       |  str型，用户推荐码   |

### 数据格式

```json
{
    "mobile": 13000000000,
    "full_name": "test",
    "gender": "male",
    "email": "xxxxxxxxx@163.com",
    "birthday": null,
    "note": "test",
    "accepts_marketing": true,
    "referral_code": "",
    "tags": []
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/crm/customer/
```

### 返回结果
```json
{
    "id": 282696,
    "multipass_identifier": null,
    "mobile": "13000000000",
    "full_name": "test",
    "gender": "male",
    "email": "xxxxxxxxx@163.com",
    "birthday": null,
    "avatar": "",
    "note": "test",
    "accepts_marketing": true,
    "created_at": "2019-07-05T17:17:28.533279+08:00",
    "updated_at": "2019-07-05T17:27:10.550541+08:00",
    "referral_code": "",
    "referrer": null,
    "tags": [],
    "addresses": [],
    "segments": [],
    "segment_ids": [],
    "statistics": {
        "last_order_id": null,
        "last_order_paid_at": null,
        "abandoned_checkouts": 0,
        "orders": 0,
        "paid_orders": 0,
        "total_spent": "0.00",
        "average_order_price": "0.00",
        "visits": 0,
        "visits_per_day": "0.00",
        "customers_referred": 0,
        "total_spent_of_referred": "0.00",
        "preferred_device": "",
        "location": ""
    },
    "profile": {
        "source_name": "unknown",
        "campaign_name": "",
        "campaign_source": "",
        "campaign_medium": "",
        "campaign_content": "",
        "campaign_term": ""
    },
    "wechat_nickname": "用户没有通过微信授权登录",
    "level": null,
    "total_points": 0,
    "points": 0,
    "metafields": []
}
```

{% endmethod %}



{% method -%}

## 更新用户信息

```
PUT /api/crm/customer/[id]/
```

所需 scope: `crm:write` 或 `write`

### 数据格式

```json
{
    "mobile": 13000000000,
    "full_name": "test",
    "gender": "male",
    "email": "xxxxxxxxx@163.com",
    "birthday": null,
    "note": "test",
    "accepts_marketing": true,
    "referral_code": "",
    "tags": []
}
```

{% sample lang="http" -%}

###发送请求

```
PUT /api/crm/customer/282696/
```

### 返回结果

同获创建用户接口 返回完整的用户信息

{% endmethod %}



{% method -%}

## 删除用户

```
DELETE /api/crm/customer/[id]/
```

所需 scope: `crm:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/crm/customer/282696/
```

### 返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

## 创建 Segment

```
POST /api/crm/segment/
```

所需 scope: `crm:write` 或 `write`

### 数据格式

```json
{
    "title": "test_title"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/crm/segment/
```

### 返回结果
```json
{
    "id": 3,
    "title": "test_title",
    "customers": []
}
```

{% endmethod %}



{% method -%}

## 添加用户归入 Segment

```
POST /api/crm/segment/[id]/segment_customer/
```

所需 scope: `crm:write` 或 `write`

### 数据格式
```json
[
    {
        "customer_id": 2
    },
    //......
]
```


{% sample lang="http" -%}

###发送请求

```
POST /api/crm/segment/1/segment_customer/
```

### 返回结果
```json
{
    "id": 1,
    "title": "标签1",
    "customers": [
        {
            "id": 2,
            "mobile": "13821761807",
            "full_name": "Xindong Ding",
            "gender": "male",
            "email": "dxd.spirits@gmail.com",
            "birthday": "1986-05-24",
            "note": "",
            "accepts_marketing": true,
            "tags": [],
            "addresses": [],
            "created_at": "2016-08-07T14:54:28Z"
        }
    ]
}

```

{% endmethod %}


{% method -%}

## 从 Segment 中移除用户

```
DELETE /api/crm/segment/[id]/segment_customer/
```

所需 scope: `crm:write` 或 `write`

### Query 参数
```
customer__in: 1,2,3
```


{% sample lang="http" -%}

###发送请求

```
DELETE /api/crm/segment/1/segment_customer/
```

### 返回结果
```json
{
    "id": 1,
    "title": "标签1",
    "customers": []
}
```

{% endmethod %}




{% method -%}

## 获取 Segment 列表

```
GET /api/crm/segment/
```

所需 scope: `crm:read` 或 `read`

### Query 参数
```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/crm/segment/?page=1
```

### 返回结果
```json
{
    "id": 1,
    "title": "标签1",
    "customers": [
        {
            "id": 2,
            "mobile": "13821761807",
            "full_name": "Xindong Ding",
            "gender": "male",
            "email": "dxd.spirits@gmail.com",
            "birthday": "1986-05-24",
            "note": "",
            "accepts_marketing": true,
            "tags": [],
            "addresses": [],
            "created_at": "2016-08-07T14:54:28Z"
        }
    ]
}
```

{% endmethod %}


{% method -%}

## 获取 Segment

```
GET /api/crm/segment/[id]/
```

所需 scope: `crm:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/crm/segment/1/
```

### 返回结果
```json
{
    "id": 1,
    "title": "标签1",
    "customers": [
        {
            "id": 2,
            "mobile": "13821761807",
            "full_name": "Xindong Ding",
            "gender": "male",
            "email": "dxd.spirits@gmail.com",
            "birthday": "1986-05-24",
            "note": "",
            "accepts_marketing": true,
            "tags": [],
            "addresses": [],
            "created_at": "2016-08-07T14:54:28Z"
        }
    ]
}
```

{% endmethod %}
