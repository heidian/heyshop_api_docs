#配置支付信息

下面为企业支付渠道 `channel` 属性值和其对应的支付渠道名称。


##支付宝（wap端）

|         序号    |    channel属性值    |    属性值说明   | 
|    :------ |    :-------    |    :---------   |   
|    1    |    alipay_pid    |    合作者身份（PID），对应商户申请的支付宝的partnerId    |  
|    2    |    alipay_account    |     支付宝企业账户（邮箱）   |    
|    3    |    alipay_security_key    |    安全校验码（Key）    |  
|    4    |    alipay_mer_wap_private_key    |    商户 RSA 私钥（非 PKCS8 编码）    |  
|    5    |    alipay_wap_public_key    |    商户支付宝公钥    |  
|    6    |    alipay_version    |    支付宝版本， 1:支付宝1.0接口， mapi ；2:支付宝2.0 接口 openapi    |  
|    7    |    alipay_app_id    |    APPID ，在参数 alipay_version 为 2.0 时，需要传入此参数    |  
|    8    |    alipay_refund_nopwd    |    是否启用免密接口，支付宝商家中心 - 我的产品 - 即时到账无密退款V3 若需开通，请访问蚂蚁商家在线服务或拨打支付宝商家服务热线    |  


##支付宝（PC端）

|         序号    |    channel属性值    |    属性值说明   | 
|    :------ |    :-------    |    :---------   |   
|    1    |    alipay_pid    |    合作者身份（PID），对应商户申请的支付宝的partnerId    |  
|    2    |    alipay_account    |    支付宝企业账户（邮箱）    |    
|    3    |    alipay_security_key    |    安全校验码（Key）    |  
|    4    |    alipay_private_key    |    商户 RSA 私钥（非 PKCS8 编码）    |  
|    5    |    alipay_public_key    |    支付宝公钥    |    
|    6    |    alipay_refund_nopwd    |    是否启用免密接口，支付宝商家中心 - 我的产品 - 即时到账无密退款V3 若需开通，请访问蚂蚁商家在线服务或拨打支付宝商家服务热线    | 


##微信

|         序号    |    channel属性值    |    属性值说明   | 
|    :------ |    :-------    |    :---------   |   
|    1    |    wx_pub_app_id    |    微信公众号AppID（应用 ID）,获取地址：微信公众平台 => 开发 => 基本配置    |  
|    2    |    wx_pub_mch_id    |    微信支付商户ID.微信公众号支付申请完成之后，在微信商户平台的通知邮件中获取，请确保邮件上的 AppID 与微信公众平台上的 AppID 一致。    |    
|    3    |    wx_pub_key    |    商户支付API 密钥，用于报文签名及验签。微信商户平台 => API 安全 => 设置密钥，将设置的 32 位密钥填入此处    |  
|    4    |    wx_pub_app_secret    |    应用密钥，用于获取openid。获取地址：微信公众平台 => 开发 => 基本配置    |  
|    5    |    wx_pub_operator_id    |    由商户添加的退款操作员ID，用于退款、退款查询。获取地址：微信商户平台 => 员工账号 => 新增员工账号，将员工的登录账号填入此处    |    
|    6    |    wx_pub_client_cert    |    微信客户端证书(pem格式,不包含秘钥)，用于退款、退款查询。微信商户平台 => API 安全 => API 证书 =>下载证书    | 
|    7    |    wx_pub_client_key    |    微信客户端证书秘钥(pem格式)，用于退款、退款查询。微信商户平台 => API 安全 => API 证书 =>下载证书    | 


