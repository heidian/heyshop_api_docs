#商店主题

{% method -%}

##查询已安装主题列表

```
GET /api/shopthemes/theme/
```

所需 scope: `shopthemes:read` 或 `read`

### Query 参数

```
name: default
scope:responsive		//responsive,webapp
is_current: true
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/?scope=responsive
```

###返回结果

```json
[
    {
        "id": 6989,
        "name": "mobile--default",
        "title": "Hugo's - 极速版主题",
        "scope": "webapp",
        "is_current": true,
        "version": "1.0",
        "screenshot": "https://up.img.heidiancdn.com/o_1b3fm4dkou2b1h3f1g1r49aiis0web.jpg",
        "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fm4dkmsi1k5gqb81l2pr780mobile.jpg",
        "private": false,
        "language": "zh",
        "settings_schema": {
            "__colors__": {
                "layout": "风格",
                "color-menu-bg": "菜单背景色",
                "color-menu-text": "菜单文字色",
                "color-highlight": "高亮色",
                "color-highlight-text": "高亮反色"
            }
        },
        "settings_data": {
            "splash_loading_icon": "square_lines",
            "colors": {
                "_color-text": "#353a36",
                ...
            },
            "tabs": [
                {
                    "href": "",
                    "text": "首页",
                    "show": true,
                    "icon": "https://up.img.heidiancdn.com/webapp/icon/home-thin.svg"
                },
                ...
                    ]
        },
        "private_settings_data": {},
        "assets": [
            "stylesheets/styles.css.njk"
        ],
        "metafield": {
            "assets_prefix": "https://theme.heidiancdn.com/1507544142531/mobile/theme/assets/"
        },
        "presets": [
            {
                "id": 293114,
                "name": "default",
                "title": "Hugo's",
                "description": "简约大气的布局\n极具质感的内容展示\n与生活追求的最佳融合\n完美地适用于家居花园等行业",
                "color": "#FFF",
                "screenshot": "https://up.img.heidiancdn.com/o_1b3fm4dkou2b1h3f1g1r49aiis0web.jpg",
                "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fm4dkmsi1k5gqb81l2pr780mobile.jpg",
                "assets": [
                    "stylesheets/styles.css.njk"
                ],
                "settings_data": {
                    "splash_loading_icon": "square_lines",
                    "colors": {
                        "_color-text": "#353a36",
                        "color-highlight": "rgba(197, 176, 65, 1)",
                        "color-menu-text": "#000",
                        "layout": "light",
                        "_color-text-darker": "#262626",
                        "_color-bg": "#f3f3f3",
                        "_color-menu-bg": "#fff",
                        "color-highlight-text": "#fff",
                        "color-menu-bg": "#fff",
                        "_color-text-lighter": "#969aa1"
                    },
                    ...
	},
   ...		//返回数据过多，用户可自行调用查看
]
```

{% endmethod %}



{% method -%}

##查询指定已安装主题

```
GET /api/shopthemes/theme/[id]/
```

所需 scope: `shopthemes:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shopthemes/theme/6989/
```

### 返回结果

```json
{
    "id": 6989,
    "name": "mobile--default",
    "title": "Hugo's - 极速版主题",
    "scope": "webapp",
    "is_current": true,
    "version": "1.0",
    "screenshot": "https://up.img.heidiancdn.com/o_1b3fm4dkou2b1h3f1g1r49aiis0web.jpg",
    "screenshot_mobile": "https://up.img.heidiancdn.com/o_1b3fm4dkmsi1k5gqb81l2pr780mobile.jpg",
    "private": false,
    "language": "zh",
    "settings_schema": {
        "__colors__": {
            "layout": "风格",
            "color-menu-bg": "菜单背景色",
            "color-menu-text": "菜单文字色",
            "color-highlight": "高亮色",
            "color-highlight-text": "高亮反色"
        }
    },
    "settings_data": {
        "colors": {
            "_color-menu-bg": "#fff",
            ...
        },
        "tabs": [
            {
                "icon": "https://up.img.heidiancdn.com/webapp/icon/home-thin.svg",
                "text": "首页",
                "href": "",
                "show": true
            },
           ...            }
        ],
        "splash_loading_icon": "square_lines"
    },
    "private_settings_data": {},
    "assets": [
        "stylesheets/styles.css.njk"
    ],
    "metafield": {
        "assets_prefix": "https://theme.heidiancdn.com/1507544142531/mobile/theme/assets/"
    },
    ...	//返回数据过多，用户可自行调用查看

}
```

{% endmethod %}



{% method -%}

##更新已安装主题

```
PATCH /api/shopthemes/theme/[id]/
```

所需 scope: `shopthemes:write` 或 `write`

###数据格式

```json
{
	"title": "test"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shopthemes/theme/6989/
```

###返回结果

同查询指定已安装主题接口 返回完整的主题信息

{% endmethod %}



{% method -%}

##激活主题(通过主题id)

```
POST /api/shopthemes/theme/[id]/activate/
```

所需 scope: `shopthemes:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/6989/activate/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##激活主题(通过主题name)

```
POST /api/shopthemes/theme/activate_theme/
```

所需 scope: `shopthemes:write` 或 `write`

###数据格式

```json
{
	"name":"mobile--default"		//用户可以通过调用/api/shopthemes/theme/接口查找主题name
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/activate_theme/
```

###返回结果

同查询指定已安装主题接口 返回完整的主题信息

{% endmethod %}



{% method -%}

##安装主题

```
POST /api/shopthemes/theme/install_theme/
```

所需 scope: `shopthemes:write` 或 `write`

###数据格式

```json
{
    "name": "garden",	//可以通过调用/api/themes/theme/接口查看主题name和预设name
    "preset": "coffee"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/install_theme/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##禁用主题

```
POST /api/shopthemes/theme/[id]/deactivate/
```

所需 scope: `shopthemes:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/6989/deactivate/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##保存为预设

```
POST /api/shopthemes/theme/[id]/save_as_preset/
```

所需 scope: `shopthemes:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/6989/save_as_preset/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##更新主题文件夹

```
POST /api/shopthemes/theme/[id]/update_theme_files/
```

所需 scope: `shopthemes:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/6989/update_theme_files/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##将主题设为私有

```
POST /api/shopthemes/theme/[id]/make_private/
```

所需 scope: `shopthemes:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shopthemes/theme/6989/make_private/
```

###返回结果

```json
http 204
```

{% endmethod %}




