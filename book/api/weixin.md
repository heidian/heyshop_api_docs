#微信授权

本接口仅适用于配置微信收款的企业用户，使用嘿店代收服务的店主无法调用此接口。

{% method -%}

##获取微信授权页面跳转链接

```
POST /api/weixin/authorization/authorization_url/
```

### 数据格式
```json
{
  "success_url": "授权成功后跳转链接",
  "auth_type": "1表示授权公众号, 2表示授权小程序, 3表示任意"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/weixin/authorization/authorization_url/
```

###返回结果

```json
{
    "url": "https://heidianapi.com/api/weixin/jump/?url=https%3A%2F%2Fmp.weixin.qq.com%2Fcgi-bin%2Fcomponentloginpage%3Fcomponent_appid%3Dwx3d81ff707739d500%26pre_auth_code%3Dpreauthcode%2540%2540%2540frMdI38BRm2lcXSMuCTps4DKLi03f9YqfBlvAqq_2gfp9u4g3fkBnLhq4LgwC_hN%26redirect_uri%3Dhttps%253A%252F%252Fheidianapi.com%252Fapi%252Fweixin%252Fauthorization%252Ffebcdb76ad5e4eaa807d8c733924d305%252Fsuccess%252F",
    "_url": "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=wx3d81ff707739d500&pre_auth_code=preauthcode%40%40%40frMdI38BRm2lcXSMuCTps4DKLi03f9YqfBlvAqq_2gfp9u4g3fkBnLhq4LgwC_hN&redirect_uri=https%3A%2F%2Fheidianapi.com%2Fapi%2Fweixin%2Fauthorization%2Ffebcdb76ad5e4eaa807d8c733924d305%2Fsuccess%2F"
}
```

{% endmethod %}



{% method -%}

##微信授权结果查询

```
POST /api/weixin/authorization/check/
```

### 数据格式
```json
{
  "appid": "[公众号或小程序appid]"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/weixin/authorization/check/
```

###返回结果

```json
{
    "updated_at": "2016-12-08T18:46:33.871849Z",
    "created_at": "2016-12-08T18:46:33.276864Z",
    "authorizer_appid": "wx3b0f9xxxxfc3ab10"
}
```

{% endmethod %}