# 商品 SKU (Product Variant)

此接口可用于同步商品的库存信息，也可以根据product_variant代替下文所述的variant，两者用法相同，仅在变量名上有所区别

{% method -%}

## 获取 SKU 列表

```
GET /api/catalogue/product_variant/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数
```
sku: "DSHOE19"
product: 575
```

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product_variant/?sku=DSHOE19
```

###返回结果

```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
              "id": 30624,
            "barcode": "",
            "sku": "S20001",
            "title": "规格: 三个, 形状: 方形, 颜色: 赭石",
            "position": 1,
            "price": "10.00",
            "compare_at_price": null,
            "grams": "0.00",
            "image": {
                "id": 62360,
                "src": "https://up.img.heidiancdn.com/o_1b3cf0cq1173vg6c9fabj1e30img9.jpg",
                "position": 1,
                "metafield": {}
            },
            "productimage": null,
            "options": [
                {
                    "value": "三个",
                    "title": "规格"
                },
                {
                    "value": "方形",
                    "title": "形状"
                },
                {
                    "value": "赭石",
                    "title": "颜色"
                }
            ],
            "inventory_policy": "deny",
            "requires_shipping": true,
            "inventory_quantity": 100,
            "old_inventory_quantity": 0,
            "inventory_quantity_adjustment": 0,
            "metafield": {},
            "created_at": "2017-09-06T07:04:21.789637Z",
            "updated_at": "2017-09-06T07:04:21.789690Z",
            "product_id": 16514
    },
    ...
  ]
}
```

{% endmethod %}


{% method -%}

## 获取指定 SKU

```
GET /api/catalogue/product_variant/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product_variant/30624/
```

### 返回结果

```json
{
            "id": 30624,
            "barcode": "",
            "sku": "S20001",
            "title": "规格: 三个, 形状: 方形, 颜色: 赭石",
            "position": 1,
            "price": "10.00",
            "compare_at_price": null,
            "grams": "0.00",
            "image": {
                "id": 62360,
                "src": "https://up.img.heidiancdn.com/o_1b3cf0cq1173vg6c9fabj1e30img9.jpg",
                "position": 1,
                "metafield": {}
            },
            "productimage": null,
            "options": [
                {
                    "value": "三个",
                    "title": "规格"
                },
                {
                    "value": "方形",
                    "title": "形状"
                },
                {
                    "value": "赭石",
                    "title": "颜色"
                }
            ],
            "inventory_policy": "deny",
            "requires_shipping": true,
            "inventory_quantity": 100,
            "old_inventory_quantity": 0,
            "inventory_quantity_adjustment": 0,
            "metafield": {},
            "created_at": "2017-09-06T07:04:21.789637Z",
            "updated_at": "2017-09-06T07:04:21.789690Z",
            "product_id": 16514
}
```

{% endmethod %}



{% method -%}

## 更新 SKU

```
PATCH /api/catalogue/product_variant/[id]/
```

所需 scope: `catalogue:write` 或 `write`

|       字段     |      说明       | 
|      :------   |    :-------    |      
|    inventory_quantity        | int型，新的库存数据   | 
|     `old_inventory_quantity`   |  int型，旧的库存数据，上一次使用 GET 请求得到的这个 SKU 的库存数据，建议 PATCH 的时候带上这个值，保证数据库不会出现写入冲突  |

### 数据格式
```json
{
  "inventory_quantity": 8

  "old_inventory_quantity": 8
}
```
{% sample lang="http" -%}

###发送请求

```
PATCH /api/catalogue/product_variant/30624/
```

### 返回结果

同获取指定SKU接口 返回完整的SKU信息

{% endmethod %}
