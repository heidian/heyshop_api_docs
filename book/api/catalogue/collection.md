# 专题


{% method -%}

## 查询专题列表

```
GET /api/catalogue/collection/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数

```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    published        | 专题是否发布, boolean型   |
|    smart        |  智能专题，是否使用 rule 匹配 product，boolean型  |
|    rules        |  由多个规则对象生成的list  |
|    disjunctive        |  当值为`true`时，只需满足rules中的一个规则，当值为`flase`时，需要满足rules中的所有规则 |
|    is_running        |  当值为`true`时，后台正在处理商品专题，当值为`flase`时，商品才能上架  |

{% sample lang="http" -%}

### 发送请求

```
GET /api/catalogue/collection/?page=1
```

### 返回结果

```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 10,
            "name": "c1",
            "title": "test_title",
            "description": "test_content",
            "image": {
                "src": "test.jpg",
                "metafield": {}
            },
            "published": true,
            "published_at": "2016-12-07T08:12:55.226141Z",
            "body_html": "test_content",
            "order_by": "",
            "smart": false,
            "disjunctive": false,
            "rules": [],
            "metafield": {
                "description": "test_content"
            },
            "created_at": "2017-09-06T07:04:22.689358Z",
            "updated_at": "2017-09-06T07:04:22.689385Z",
            "meta_title": "test_meta_title",
            "meta_description": "test_content",
            "meta_image": "test.jpg",
            "is_running": true
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

## 查询指定专题

```
GET /api/catalogue/collection/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

### 发送请求

```
GET /api/catalogue/collection/6378/
```

### 返回结果

```json
{
    "id": 6378,
    "name": "c4",
    "title": "test_title",
    "description": "test",
    "image": {
        "src": "test.jpg",
        "metafield": {}
    },
    "published": true,
    "published_at": "2016-12-07T08:12:55.281588Z",
    "body_html": "test",
    "order_by": "",
    "smart": false,
    "disjunctive": false,
    "rules": [],
    "metafield": {
        "description": "test"
    },
    "created_at": "2017-09-06T07:04:22.805606Z",
    "updated_at": "2017-09-06T07:04:22.805631Z",
    "meta_title": "test",
    "meta_description": "test",
    "meta_image": "test.jpg",
    "is_running": true
}
```

{% endmethod %}



{% method -%}

## 创建专题

```
POST /api/catalogue/collection/
```

所需 scope: `catalogue:write` 或 `write`

### 数据格式
```json
{
    "name": "test",
    "title": "test",
    "description": "test",
    "image": {"src": ""},
    "published": true,
    "published_at": "2017-01-17T06:28:09.408520Z",
    "body_html": "test",
    "order_by": "name",
    "smart": true,          # 是否使用 rule 匹配 product
    "disjunctive": true     # rule 成立条件 or / and
}
```

{% sample lang="http" -%}

### 发送请求

```
POST /api/catalogue/collection/
```

### 返回结果

同查询指定专题借口 返回完整的专题信息

{% endmethod %}



{% method -%}

## 更新专题

```
PATCH /api/catalogue/collection/[id]/
```

所需 scope: `catalogue:write` 或 `write`

### 数据格式
```json
{
    "name": "test_1",
    "title": "test_1",
    "description": "test_1",
    "image": {"src": ""},
    "published": true,
    "published_at": "2017-01-17T06:28:09.408520Z",
    "body_html": "test",
    "order_by": "name",
    "smart": true,
    "disjunctive": true
}
```

{% sample lang="http" -%}

### 发送请求

```
PATCH /api/catalogue/collection/6446/
```

### 返回结果

同查询指定专题借口 返回完整的专题信息

{% endmethod %}



{% method -%}

## 删除专题

```
DELETE /api/catalogue/collection/[id]/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

### 发送请求

```
DELETE /api/catalogue/collection/6446/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

## 查询专题商品

```
GET /api/catalogue/collection/[id]/collect/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    TODO        | TODO   |


{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/collection/6446/collect/?page=1
```

###返回结果

```json
{
	"count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 13887,
            "product": {
                "id": 16511,
                "name": "p1",
                "title": "test",
                "description": "test",
                "body_html": "test",
                "body_html_mobile": "",
                "vendor": null,
                "image": {
                    "id": 62345,
                    "src": "test",
                    "position": 1,
                    "metafield": {}
                },
                "category_ids": [
                    17568,
                    17569
                ],
                "category_titles": [
                    "test1",
                    "tes2"
                ],
                "price": "10.00",
                "compare_at_price": null,
                "inventory_quantity": 100,
                "requires_shipping": true,
                "inventory_policy": "deny",
                "published": true,
                "published_at": "2016-12-07T08:12:54.777101Z",
                "created_at": "2017-09-06T07:04:20.136370Z",
                "updated_at": "2017-09-06T07:04:20.136398Z",
                "metafield": {},
                "meta_title": "test",
                "meta_description": "test",
                "meta_image": "test",
                "tags": [],
                "options": [],
                "variants": [
                    {
                        "id": 30619,
                        "barcode": "",
                        "sku": "S10001",
                        "title": "",
                        "position": 1,
                        "price": "10.00",
                        "compare_at_price": null,
                        "grams": "0.00",
                        "image": {
                            "id": 62345,
                            "src": "test.jpg",
                            "position": 1,
                            "metafield": {}
                        },
                        "productimage": null,
                        "options": [],
                        "inventory_policy": "deny",
                        "requires_shipping": true,
                        "inventory_quantity": 100,
                        "old_inventory_quantity": 0,
                        "inventory_quantity_adjustment": 0,
                        "metafield": {},
                        "created_at": "2017-09-06T07:04:20.395431Z",
                        "updated_at": "2017-09-06T07:04:20.395483Z",
                        "product_id": 16511
                    }
                ],
                "images": [
                    {
                        "id": 62345,
                        "src": "test.jpg",
                        "position": 1,
                        "metafield": {}
                    },
                    ...
                ],
                "sold_quantity": 0,
                "shipping_method_group_id": null,
                "voucher_product": null
            },
            "featured": false,
            "position": 0,
            "created_at": "2017-09-08T04:16:09.776825Z",
            "updated_at": "2017-09-08T04:16:09.776878Z"
        },
        ...
        ]
}
```

{% endmethod %}



{% method -%}

## 创建专题商品

```
POST /api/catalogue/collection/[id]/collect/
```

所需 scope: `catalogue:read` 或 `read`

###数据格式

```json
[
	{
	  "collection_id": 6447,
      "product_id": 16511
	}
]
```


{% sample lang="http" -%}

###发送请求

```
POST /api/catalogue/collection/6446/collect/
```

###返回结果

同查询专题商品接口，返回完整的专题商品信息


{% endmethod %}



{% method -%}

##删除专题商品

```
DELETE /api/catalogue/collection/[id]/collect/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/catalogue/collection/6446/collect/13887/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

## 获取专题商品规则列表

```
GET /api/catalogue/collection/[id]/rule/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数
```
page: 1
page_size: 10
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    field        | TODO   |
|    lookup        | TODO   |
|    value        | TODO   |
|    filter_kwargs        | TODO   |
|    negative        | TODO   |


field - lookup 对应关系:

```
{
    'id': ['equals', 'in', 'not_equals', 'not_in'],
    'name': ['equals', 'in', 'not_equals', 'not_in'],
    'title': ['equals', 'contains', 'not_equals', 'not contains'],
    'published': ['equals'],
    'requires_shipping': ['equals'],

    'inventory_quantity': ['equals', 'between'],
    'price': ['equals', 'between'],
    'published_at': ['equals', 'between'],
    'sold_quantity': ['equals', 'between'],

    'tags': ['equals', 'in', 'not_equals', 'not_in'],  # id
    'categories': ['equals', 'in', 'not_equals', 'not_in'],  # id
    'vendors': ['equals', 'in', 'not_equals', 'not_in']  # id
}
```

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/collection/6447/rule/
```

### 返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 197,
            "collection_id": 6447,
            "field": "price",
            "lookup": "between",
            "value": [
                10.8,
                20.5
            ],
            "filter_kwargs": {
                "price__range": [
                    10.8,
                    20.5
                ]
            },
            "negative": false,
            "metafield": {}
        },
        ...
    ]
}
```

{% endmethod %}





{% method -%}

## 获取指定专题商品规则

```
GET /api/catalogue/collection/[id]/rule/[id]/
```

所需 scope: `catalogue:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/collection/6447/rule/197/
```

### 返回结果

```json
{
    "id": 197,
    "collection_id": 6447,
    "field": "price",
    "lookup": "between",
    "value": [
        10.8,
        20.5
    ],
    "filter_kwargs": {
        "price__range": [
            10.8,
            20.5
        ]
    },
    "negative": false,
    "metafield": {}
}
```

{% endmethod %}




{% method -%}

## 创建专题商品规则

```
POST /api/catalogue/collection/[id]/rule/
```

所需 scope: `catalogue:write` 或 `write`

### 数据格式
```json
{
    "field": "price",
    "lookup": "between",
    "value": [10.80, 20.50]
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/catalogue/collection/6447/rule/
```

### 返回结果

同获取指定专题商品规则 返回完整的规则信息

{% endmethod %}



{% method -%}

##更新专题商品规则

```
PATCH /api/catalogue/collection/[id]/rule/[id]/
```

所需 scope: `catalogue:write` 或 `write`

### 数据格式
```json
{
    "field": "price",
    "lookup": "between",
    "value": [10.80, 20.50]
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/catalogue/collection/6447/rule/197/
```

### 返回结果

同获取指定专题商品规则 返回完整的规则信息

{% endmethod %}



{% method -%}

##删除专题商品规则

```
DELETE /api/catalogue/collection/[id]/rule/[id]/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/catalogue/collection/6447/rule/197/
```

##返回结果

```json
http 204
```

{% endmethod %}

