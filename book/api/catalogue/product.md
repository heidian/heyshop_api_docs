# 商品


{% method -%}

## 查询商品列表

```
GET /api/catalogue/product/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数

```
id: 1002
id__in: 1002,1005
name: shoe
name__in: shoe,shoe2
voucherproduct__is_enabled: true  // 是否为代金券商品
published_at__gte: 2017-02-01 00:00:00
published_at__lte: 2017-03-31 00:00:00
published: true
title: 测试商品鞋
price__gte: 10.00
price__lte: 22.55
collection: 22
collection__exclude: 22
category: 5
category__in: 5,6,7,10
coupons: 28
coupons__exclude: 28
q: 搜索关键词
order_by: id (id / created_at / name / inventory_quantity / title / price / published_at)
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product/?page=1
```

### 返回结果

```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 575,
      "name": "datuxiaoshangp",
      "title": "大图小商品"

      /*
       * 其他字段参见下方的获取商品接口
       */

    },
    {
      "id": 529,
      "name": "shoe",
      "title": "一双很好看的鞋子！"
    }
  ]
}
```

{% endmethod %}


{% method -%}

## 查询指定商品

```
GET /api/catalogue/product/[id]/
```

所需 scope: `catalogue:read` 或 `read`

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    category_ids        | int型，由卖家创建商品和商品规则后生成   |
|    category_titles        | string型，由卖家创建商品和商品规则后生成   |
|    price        | int型，第一个 SKU 的价格   |
|    compare_at_price        | int型，第一个 SKU 的原价，值可以为空   |
|    inventory_quantity		|	int型，所有 SKU 的库存总和     |
|    inventory_policy		|	缺货是否可以继续购买 continue: 是，deny: 否 |
|	  published			   |	发布状态，发布后的商品才可以在店铺中看到   |
|	  options			|	细分属性，用于区分 SKU  |
|	  variants		|	细分商品，即该商品下的 SKU  |
|	  voucher_product	|	代金券商品信息，默认 is_enabled 为 false，即为普通商品  |


{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product/529/
```

### 返回结果


```json
{
  "id": 529,

  "name": "shoe",
  "title": "test",
  "body_html": "test",
  "vendor": "",
  "image": "http://up.img.heidiancdn.com/o_1ar8usd4t139mks15imb82r6d0IMG_8613.JPG",

  "category_ids": [
    550,
    553
  ],

  "category_titles": [
    "男鞋",
    "男装"
  ],

  "price": 10,

  "compare_at_price": null,

  "inventory_quantity": 38,
  "requires_shipping": true,

  "inventory_policy": "deny",

  "published": true,

  "published_at": "2016-08-25T05:26:59Z",
  "created_at": "2016-08-25T05:26:59Z",
  "updated_at": "2016-11-15T17:23:57.739528Z",
  "metafield": {},
  "tags": [],

  "options": [
    {
      "id": 331,

      "title": "颜色",

      "values": [
        "白色",
        "红色"
      ]
    },
    {
      "id": 332,
      "title": "尺码",
      "values": [
        "大号",
        "小号"
      ]
    }
  ],

  "variants": [
    {
      "id": 787,
      "barcode": "",

      "sku": "A9938",
      "title": "颜色: 红色, 尺码: 大号",
      "position": 1,

      "price": "10.00",

     "compare_at_price": null,

      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-08-25T05:26:59Z",

     "options": [
        {
          "title": "颜色",
          "value": "红色"
        },
        {
          "title": "尺码",
          "value": "大号"
        }
      ],

      "inventory_policy": "deny",

      "inventory_quantity": 20
    },
    {
      "id": 788,
      "barcode": "",
      "sku": "A39C50",
      "title": "颜色: 白色, 尺码: 小号",
      "position": 2,
      "price": "11.00",
      "compare_at_price": null,
      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-08-25T05:26:59Z",
      "options": [
        {
          "title": "颜色",
          "value": "白色"
        },
        {
          "title": "尺码",
          "value": "小号"
        }
      ],
      "inventory_policy": "deny",
      "inventory_quantity": 10
    },
    {
      "id": 789,
      "barcode": "",
      "sku": "BEDD70",
      "title": "颜色: 白色, 尺码: 大号",
      "position": 3,
      "price": "12.00",
      "compare_at_price": null,
      "grams": "0.00",
      "image": "",
      "requires_shipping": true,
      "metafield": {},
      "created_at": "2016-08-25T05:26:59Z",
      "updated_at": "2016-10-28T09:21:08.029140Z",
      "options": [
        {
          "title": "颜色",
          "value": "白色"
        },
        {
          "title": "尺码",
          "value": "大号"
        }
      ],
      "inventory_policy": "deny",
      "inventory_quantity": 8
    }
  ],
  "images": [
    {
      "id": 1676,
      "src": "http://up.img.heidiancdn.com/o_1ar8usd4t139mks15imb82r6d0IMG_8613.JPG",
      "position": 1,
      "metafield": {}
    },
    {
      "id": 1677,
      "src": "http://up.img.heidiancdn.com/o_1ap2b9eo9ghoum6tqm1rf8vr60粉色眼影.jpg",
      "position": 2,
      "metafield": {}
    }
  ],

  "voucher_product": {
      "id": 1,
      "product_id": 529,
      "is_enabled": false,
      "white_product_ids": [],
      "black_product_ids": [],
      "expire_days": 0
  }
}
```

建议 ERP 系统使用 SKU 编号来同步商品的库存信息

{% endmethod %}



{% method -%}

##创建商品

```
POST /api/catalogue/product/
```

所需 scope: `catalogue:write` 或 `write`

###发送请求

```
POST /api/catalogue/product/
```

###数据格式

```json
{
    "name": "test_products_3",
    "variants": [
    				{
						"compare_at_price": 1.1,
						"grams": 1,
						"inventory_quantity": 10,
						"options": [],
						"price": 1.00,
						"sku": "128g"
    				}
        ]
}
```

{% sample lang="http" -%}

###返回结果

```json
{
	"id": 16716,
    "name": "test_products_3",
    "title": "商品标题",
    "description": "",
    "body_html": "",
    "body_html_mobile": "",
    "vendor": null,
    "image": {
        "id": null,
        "src": "",
        "position": 0,
        "metafield": {}
    },
    "category_ids": [],
    "category_titles": [],
    "price": "1.00",
    "compare_at_price": 1.1,
    "inventory_quantity": 10,
    "requires_shipping": true,
    "inventory_policy": "deny",
    "published": false,
    "published_at": null,
    "created_at": "2017-09-11T04:13:13.139412Z",
    "updated_at": "2017-09-11T04:13:13.139458Z",
    "metafield": {},
    "meta_title": "商品标题",
    "meta_description": "",
    "meta_image": "",
    "tags": [],
    "options": [],
    "variants": [
        {
            "id": 31386,
            "barcode": "",
            "sku": "128g",
            "title": "",
            "position": 1,
            "price": "1.00",
            "compare_at_price": "1.10",
            "grams": "1.00",
            "image": {
                "id": null,
                "src": "",
                "position": 0,
                "metafield": {}
            },
            "productimage": null,
            "options": [],
            "inventory_policy": "deny",
            "requires_shipping": true,
            "inventory_quantity": 10,
            "old_inventory_quantity": 0,
            "inventory_quantity_adjustment": 0,
            "metafield": {},
            "created_at": "2017-09-11T04:13:13.261184Z",
            "updated_at": "2017-09-11T04:13:13.261236Z",
            "product_id": 16716
        }
    ],
    "images": [],
    "sold_quantity": 0,
    "shipping_method_group_id": null,
    "voucher_product": null
}
```

{% endmethod %}



{% method -%}

## 更新商品

```
PATCH /api/catalogue/product/[id]/
```

所需 scope: `catalogue:write` 或 `write`

### 数据格式
```json
{
    "name": "test_products_5",
    "title": "update_product_name",
    "description": "123",
    "variants": [
        {
           //当id存在时，可传id值，否则系统将默认创建新id
           "id": 31386,
           "compare_at_price": 1.1,
			 "grams": 1,
			 "inventory_quantity": 10,
		 	 "options": [],
			 "price": 1.00,
			 "sku": "256g"
        }
    ]
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/catalogue/product/16716/
```

### 返回结果

同获取商品接口
返回完整的商品信息

{% endmethod %}



{% method -%}

## 删除商品

```
DELETE /api/catalogue/product/[id]/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/catalogue/product/16716/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##商品复制

```
POST /api/catalogue/product/[id]/duplicate/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/catalogue/product/16717/duplicate/
```

###返回结果

同查询指定商品接口 返回完整的商品信息

{% endmethod %}



{% method -%}

##商品计数

```
GET /api/catalogue/product/count/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product/count/
```

###返回结果

```json
[
    {
        "value": "true",
        "title": "出售中",
        "count": 4
    },
    {
        "value": "false",
        "title": "仓库中",
        "count": 2
    }
]
```
{% endmethod %}



{% method -%}

##查询商品类型列表

```
GET /api/catalogue/producttype/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/producttype/
```

###返回结果

```json
[
    {
        "id": 1,
        "name": "clothing-and-bags",
        "title": "服饰与箱包"
    },
    {
        "id": 2,
        "name": "electronics",
        "title": "电子商品"
    },
    {
        "id": 3,
        "name": "jewelry-and-accessories",
        "title": "珠宝与配件"
    },
    {
        "id": 4,
        "name": "home-and-graden",
        "title": "家居与花园"
    },
    ...
]
```

{% endmethod %}



{% method -%}

##查询指定商品类型

```
GET /api/catalogue/producttype/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/producttype/1/
```

###返回结果

```json
{
    "id": 1,
    "name": "clothing-and-bags",
    "title": "服饰与箱包"
}
```

{% endmethod %}


{% method -%}

##查询供应商列表

```
GET /api/catalogue/vendor/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/vendor/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 625,
            "title": "test"
        },
        {
            "id": 626,
            "title": "test2"
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定供应商

```
GET /api/catalogue/vendor/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/vendor/625/
```

###返回结果

```json
{
    "id": 625,
    "title": "test"
}
```

{% endmethod %}



{% method -%}

##创建供应商

```
POST /api/catalogue/vendor/
```

###数据格式

```json
{
 "title": "test_vendor_name"
}
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/catalogue/vendor/
```

###返回结果

```json
{
    "id": 632,
    "title": "test_vendor_name"
}
```

{% endmethod %}



{% method -%}

##更新供应商

```
PATCH /api/catalogue/vendor/[id]/
```

###数据格式

```json
{
 "title": "new_test_vendor_name"
}
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
PATCH /api/catalogue/vendor/632/
```

###返回结果

```json
{
    "id": 632,
    "title": "new_test_vendor_name"
}
```

{% endmethod %}



{% method -%}

##删除供应商

```
DELETE /api/catalogue/vendor/[id]/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/catalogue/vendor/632/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询可用标签列表

```
GET /api/catalogue/tag/
```

所需 scope: `catalogue:read` 或 `read`

### Query 参数

```
title: tag_name
```

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/tag/?title=a
```

###返回结果

```json
[
    {
        "id": 14,
        "title": "a"
    },
    {
        "id": 278,
        "title": "b"
    },
    {
        "id": 172,
        "title": "yeah!"
    },
    {
        "id": 187,
        "title": "没有啥标签啊"
    }
]
```

{% endmethod %}



{% method -%}

##查询指定可用标签

```
GET /api/catalogue/tag/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/tag/14/
```

###返回结果

```json
{
    "id": 14,
    "title": "a"
}
```

{% endmethod %}



{% method -%}

##查询商品meta field

```
GET /api/catalogue/product/[id]/metafields/
```

所需 scope: `catalogue:read` 或 `read`

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    key        | 关键值,只能包含字母，数字，下划线或者中划线，key值不可重复，不可更改    |
|    namespace        | 命名空间,只能包含字母，数字，下划线或者中划线，namespace值不可更改    |
|    value        | 属性值,只能包含字母，数字，下划线或者中划线    |

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product/18925/metafields/
```

###返回结果

```json
[
    {
        "id": 193,
        "namespace": "123",
        "key": "123",
        "value_type": "string",
        "value": "test",
        "description": ""
    },
    {
        "id": 194,
        "namespace": "123",
        "key": "1234",
        "value_type": "string",
        "value": "test",
        "description": "test"
    }
]
```

{% endmethod %}



{% method -%}

##查询指定商品meta field

```
GET /api/catalogue/product/[id]/metafields/[id]/
```

所需 scope: `catalogue:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/catalogue/product/18925/metafields/193/
```

###返回结果

```json
{
    "id": 193,
    "namespace": "123",
    "key": "123",
    "value_type": "string",
    "value": "test",
    "description": "test"
}
```

{% endmethod %}



{% method -%}

##创建商品meta field

```
POST /api/catalogue/product/[id]/metafields/
```

所需 scope: `catalogue:write` 或 `write`

###数据格式

```json
{
    "namespace": "123",
    "key": "1234",
    "value": "test",
    "description": "test"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/catalogue/product/18925/metafields/
```

###返回结果

同查询指定商品meta field接口 返回完整的meta信息

{% endmethod %}



{% method -%}

##更新商品meta field

```
PATCH /api/catalogue/product/[id]/metafields/[id]/
```

所需 scope: `catalogue:write` 或 `write`

###数据格式

```json
{
    "value": "test2",
    "description": "test2"
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/catalogue/product/18925/metafields/193/
```

###返回结果

同查询指定商品meta field接口 返回完整的meta信息

{% endmethod %}



{% method -%}

##删除商品meta field

```
DELETE /api/catalogue/product/[id]/metafields/[id]/
```

所需 scope: `catalogue:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/catalogue/product/18925/metafields/193/
```

###返回结果

```json
http 204
```

{% endmethod %}






