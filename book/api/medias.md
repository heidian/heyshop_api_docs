# 媒体文件


{% method -%}

##查询文件列表

```
GET /api/medias/file/
```  

所需 scope: `medias:read` 或 `read`

### Query 参数

```
id: 1234
id__in: 10,11,12
file_type: image
file_type__in: _folder,image,video,audio,icon,other
parent: 12345
path:　abc/der
order_by: updated_at (created_at/title/title_pinyin/path/file_type)
```


{% sample lang="http" -%}

###发送请求

```
GET /api/medias/file/?file_type=image
```  

###返回结果

```json
    "count": 37,
    "next": "http://127.0.0.1:8000/api/medias/file/?page=2",
    "previous": null,
    "results": [
        {
            "id": 1554,
            "file_type": "image",
            "origin_file_type": "image/png",
            "parent": null,
            "title": "哈哈",
            "description": "",
            "src": "http://up.img.heidiancdn.com/o_1anhopi101adlha91s5jma6ka90.png",
            "file_size": 0,
            "orientation": "",
            "image_ave": "",
            "width": 0,
            "height": 0,
            "title_pinyin": "haha",
            "metafield": {},
            "created_at": "2016-07-13T09:16:24Z",
            "updated_at": "2016-08-14T07:55:24.453730Z",
            "children_count": 0
        },
        ...
```

{% endmethod %}



{% method -%}

##查询指定文件

```
GET /api/medias/file/[id]/
```

所需 scope: `medias:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/medias/file/1553/
```

###返回结果

```json
{
    "id": 1553,
    "file_type": "image",
    "origin_file_type": "image/png",
    "parent": null,
    "title": "o_1anhon7unen214vsu0mudm1fjq0.png",
    "description": "",
    "src": "http://up.img.heidiancdn.com/o_1anhon7unen214vsu0mudm1fjq0.png",
    "file_size": 0,
    "orientation": null,
    "image_ave": null,
    "width": 0,
    "height": 0,
    "title_pinyin": "_o_1anhon7unen214vsu0mudm1fjq0.png",
    "metafield": {},
    "created_at": "2016-07-13T09:15:08Z",
    "updated_at": "2016-08-14T07:55:24.450985Z",
    "children_count": 0
}
```

{% endmethod %}



{% method -%}  

##创建新文件

```
POST /api/medias/file/
```

所需 scope: `medias:write` 或 `write`

###数据格式  

```json
{ 
   "file_type": "image", 
	"path": "paraent_title_name",  
	"origin_file_type": "test_type",   
	"title": "file_name",
   "description": "This is a XXX file"   
}
```

{% sample lang="http" -%}  

###发送请求

```
POST /api/medias/file/
```

###返回结果

```json
{
	"id": 82978,
    "file_type": "image",
    "origin_file_type": null,
    "parent": 80770,
    "title": "file_name",
    "description": "",
    "src": null,
    "file_size": 0,
    "orientation": null,
    "image_ave": null,
    "width": 0,
    "height": 0,
    "title_pinyin": "file_name",
    "metafield": {},
    "created_at": "2017-09-12T10:08:48.301239Z",
    "updated_at": "2017-09-12T10:08:48.301289Z",
    "children_count": 0
}
```  

{% endmethod %}  



{% method -%}

##修改文件

```
PATCH /api/medias/file/[id]/
```

所需 scope: `medias:write` 或 `write`

###数据格式

```json
{ 
    "file_type": "other", 
	"path": "/aaa/",  
	"origin_file_type": "test_1_type",   
	"title": "new_file_name",
    "description": "This is a XX file",  
    "src": "http://example.com/image/xxx.pdf"  
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/medias/file/11/
```

###返回结果

```json
{
	"id": 11,  
    "file_type": "other", 
	"path": "/aaa/",  
	"origin_file_type": "test_1_type",   
	"title": "new_file_name",
    "description": "This is a XX file",  
    "src": "http://example.com/image/xxx.pdf",
	 "file_size": 0,
    "orientation": null,
    "image_ave": null,
    "width": 0,
    "height": 0,
    "title_pinyin": "_o_1anhon7unen214vsu0mudm1fjq0.png",
    "metafield": {},
    "created_at": "2016-07-13T09:15:08Z",
    "updated_at": "2016-08-14T07:55:24.450985Z",
    "children_count": 0
}
```

{% endmethod %}



{% method -%}

##删除文件

```
DELETE /api/medias/file/[id]/
```

所需 scope: `medias:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
DELETE /api/medias/file/11/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询素材列表

```
GET /api/medias/material/
```

所需 scope: `medias:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/material/
```

###返回结果

```json
{
	"count": 636,
    "next": "http://demo.heidianer.com/api/medias/material/?page=2",
    "previous": null,
    "results": [
        {
            "id": 2362,
            "file_type": "_folder",
            "origin_file_type": null,
            "parent": 34232,
            "title": "01服饰",
            "description": "",
            "src": null,
            "file_size": 0,
            "orientation": null,
            "image_ave": null,
            "width": 0,
            "height": 0,
            "title_pinyin": "01fushi",
            "metafield": {},
            "created_at": "2016-08-01T05:18:32Z",
            "updated_at": "2016-08-14T08:50:11Z",
            "children_count": 47
        },
        ...
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定素材

```
GET /api/medias/material/[id]/
```

所需 scope: `medias:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/material/2363/
```

###返回结果

```json
{
          "id": 2363,
          "file_type": "_folder",
          "origin_file_type": null,
          "parent": 34232,
          "title": "02家具",
          "description": "",
          "src": null,
          "file_size": 0,
          "orientation": null,
          "image_ave": null,
          "width": 0,
          "height": 0,
          "title_pinyin": "02jiaju",
          "metafield": {},
          "created_at": "2016-08-01T05:18:50Z",
          "updated_at": "2016-08-14T08:50:11Z",
          "children_count": 42. 
			"children_count": 
}
```

{% endmethod %}



{% method -%}

##查询文件夹

```
GET /api/medias/all_folders/
```

所需 scope: `medias:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/all_folders/
```

###返回结果

```json
{
	"results": [
        {
            "id": 80774,
            "file_type": "_folder",
            "parent": 80770,
            "title": "456",
            "title_pinyin": "456",
            "metafield": {},
            "created_at": "2017-09-06T07:05:22.623524Z",
            "updated_at": "2017-09-06T07:05:22.623571Z",
            "children_count": 0
        },
		...
                ] 
}
```

{% endmethod %}



{% method -%}

##查询指定文件夹

```
GET /api/medias/all_folders/[id]/
```

所需 scope: `medias:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/all_folders/80773/
```

###返回结果

```json
{
            "id": 80773,
            "file_type": "_folder",
            "parent": 80770,
            "title": "123",
            "title_pinyin": "123",
            "metafield": {},
            "created_at": "2017-09-06T07:05:17.740110Z",
            "updated_at": "2017-09-06T07:05:17.740159Z",
            "children_count": 0
}
```
{% endmethod %}



{% method -%}

##查询素材文件夹

```
GET /api/medias/all_material_folders/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/all_material_folders/
```

###返回结果

```json
{
	"results": [
        {
            "id": 2362,
            "file_type": "_folder",
            "parent": 34232,
            "title": "01服饰",
            "title_pinyin": "01fushi",
            "metafield": {},
            "created_at": "2016-08-01T05:18:32Z",
            "updated_at": "2016-08-14T08:50:11Z",
            "children_count": 47
        },
        ...
        ]
}
```


{% endmethod %}



{% method -%}

##查询指定素材文件夹

```
GET /api/medias/all_material_folders/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/medias/all_material_folders/2363/
```

###返回结果

```json
{
    "id": 2363,
    "file_type": "_folder",
    "parent": 34232,
    "title": "02家具",
    "title_pinyin": "02jiaju",
    "metafield": {},
    "created_at": "2016-08-01T05:18:50Z",
    "updated_at": "2016-08-14T08:50:11Z",
    "children_count": 42
}
```


{% endmethod %}

