#物流

{% method -%}

##查询物流模板

```
GET /api/shippings/shippingmethodgroup/
```

所需 scope: `shippings:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/shippingmethodgroup/?page=1
```

###返回结果

```json
{
    "count": 5,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1507,
            "is_default": true,
            "title": "全场统一运费",
            "rate_type": "fixed",
            "shipping_methods": [
                {
                    "id": 3244,
                    "rate_type": "fixed",
                    "is_enabled": true,
                    "title": "快递",
                    "normal_price": "0.00",
                    "normal_unit": "0.00",
                    "over_price": "0.00",
                    "over_unit": "0.00",
                    "free_shipping_policy": "nope",
                    "free_shipping_price": "0.00",
                    "group_id": 1507,
                    "metafield": {},
                    "updated_at": "2017-09-06T07:04:09.959247Z",
                    "shipping_zones": [],
                    "unsold_place_codes": []
                },
                {
                    "id": 3245,
                    "rate_type": "fixed",
                    "is_enabled": false,
                    "title": "自提",
                    "normal_price": "0.00",
                    "normal_unit": "0.00",
                    "over_price": "0.00",
                    "over_unit": "0.00",
                    "free_shipping_policy": "nope",
                    "free_shipping_price": "0.00",
                    "group_id": 1507,
                    "metafield": {},
                    "updated_at": "2017-09-06T07:04:09.962617Z",
                    "shipping_zones": [],
                    "unsold_place_codes": []
                }
            ]
        },
        ...
    ]
}
```




{% endmethod %}


{% method -%}

##查询指定物流模板

```
GET /api/shippings/shippingmethodgroup/[id]/
```

所需 scope: `shippings:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/shippingmethodgroup/1507/
```

###返回结果

```json
{
    "id": 1507,
    "is_default": true,
    "title": "全场统一运费",
    "rate_type": "fixed",
    "shipping_methods": [
        {
            "id": 3244,
            "rate_type": "fixed",
            "is_enabled": true,
            "title": "快递",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1507,
            "metafield": {},
            "updated_at": "2017-09-06T07:04:09.959247Z",
            "shipping_zones": [],
            "unsold_place_codes": []
        },
        {
            "id": 3245,
            "rate_type": "fixed",
            "is_enabled": false,
            "title": "自提",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1507,
            "metafield": {},
            "updated_at": "2017-09-06T07:04:09.962617Z",
            "shipping_zones": [],
            "unsold_place_codes": []
        }
    ]
}
```

{% endmethod %}



{% method -%}

##创建物流模板

```
POST /api/shippings/shippingmethodgroup/
```

所需 scope: `shippings:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/shippings/shippingmethodgroup/
```

###返回结果

```json
{
	"id": 1517,
    "is_default": false,
    "title": "",
    "rate_type": "fixed",
    "shipping_methods": [
        {
            "id": 3264,
            "rate_type": "fixed",
            "is_enabled": true,
            "title": "快递",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1517,
            "metafield": {},
            "updated_at": "2017-09-07T07:34:55.295548Z",
            "shipping_zones": [],
            "unsold_place_codes": []
        },
        {
            "id": 3265,
            "rate_type": "fixed",
            "is_enabled": false,
            "title": "自提",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1517,
            "metafield": {},
            "updated_at": "2017-09-07T07:34:55.306418Z",
            "shipping_zones": [],
            "unsold_place_codes": []
        }
    ]
}
```

{% endmethod %}



{% method -%}

##更新物流模板

```
PATCH /api/shippings/shippingmethodgroup/[id]/
```

所需 scope: `shippings:write` 或 `write`

###数据格式

```json
{
    "title": "test_goods",
    "rate_type": "fixed“ 
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shippings/shippingmethodgroup/1520/
```

###返回结果

```json
{
    "id": 1520,
    "is_default": false,
    "title": "test_goods",
    "rate_type": "fixed",
    "shipping_methods": []
}
```

{% endmethod %}



{% method -%}

##删除物流模板

```
DELETE /api/shippings/shippingmethodgroup/[id]/
```

所需 scope: `shippings:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/shippings/shippingmethodgroup/1520/
```

###返回结果

```json  
http 204
```

{% endmethod %}




{% method -%}

##查询配送方式

```
GET /api/shippings/shippingmethod/
```

所需 scope: `shippings:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/shippingmethod/?page=1
```

###返回结果

```json
	 "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 3244,
            "rate_type": "fixed",
            "is_enabled": true,
            "title": "快递",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1507,
            "metafield": {},
            "updated_at": "2017-09-06T07:04:09.959247Z",
            "shipping_zones": [],
            "unsold_place_codes": []
        },
        ...
       ]
```


{% endmethod %}


{% method -%}

##查询指定配送方式

```
GET /api/shippings/shippingmethod/[id]/
```

所需 scope: `shippings:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/shippingmethod/3244/
```

###返回结果

```json
{
            "id": 3244,
            "rate_type": "fixed",
            "is_enabled": true,
            "title": "快递",
            "normal_price": "0.00",
            "normal_unit": "0.00",
            "over_price": "0.00",
            "over_unit": "0.00",
            "free_shipping_policy": "nope",
            "free_shipping_price": "0.00",
            "group_id": 1507,
            "metafield": {},
            "updated_at": "2017-09-06T07:04:09.959247Z",
            "shipping_zones": [
            						...
            						],
            "unsold_place_codes": []
}
```


{% endmethod %}


{% method -%}

##更新配送方式

```
PATCH /api/shippings/shippingmethod/[id]/
```

所需 scope: `shippings:write` 或 `write`

###数据格式

```json
{
	"rate_type": "fixed",
   "is_enabled": true,
   "title": "快递_test",
   "normal_price": "1.00",
   "normal_unit": "1.00",
   "over_price": "1.00",
   "over_unit": "1.00",
   "free_shipping_policy": "nope",
   "free_shipping_price": "1.00",
   "group_id": 1507
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/shippings/shippingmethod/3244/
```

###返回结果

```json
{
	"id": 3244,
   "rate_type": "fixed",
   "is_enabled": true,
   "title": "快递_test",
   "normal_price": "1.00",
   "normal_unit": "1.00",
   "over_price": "1.00",
   "over_unit": "1.00",
   "free_shipping_policy": "nope",
   "free_shipping_price": "1.00",
   "group_id": 1507,
   "metafield": {},
   "updated_at": "2017-09-06T07:04:09.959247Z",
   "shipping_zones": [
            			...
            			  ],
   "unsold_place_codes": []
}
```


{% endmethod %}



{% method -%}

##查询地区

```
GET /api/shippings/place/
```

所需 scope: `shippings:read` 或 `read`

### Query 参数

```json
parent_code=110000
```

{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/place/
```

###返回结果

```json

		//结果中code值为该地区邮政编码。
{
	 {
        "id": 1,
        "name": "北京市",
        "code": "110000",
        "parent": null,
        "area": "华北",
        "short_name": "北京",
        "pinyin": "BeiJing"
    },
    ...
}
```

{% endmethod %}



{% method -%}

##查询指定地区

```
GET /api/shippings/place/[id]/
```

所需 scope: `shippings:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/place/100/
```

###返回结果

```json
{
	"id": 100,
    "name": "肥乡县",
    "code": "130428",
    "parent": 88,
    "area": "",
    "short_name": "肥乡县",
    "pinyin": "FeiXiangXian"
}
```

{% endmethod %}



{% method -%}

##	查询对应区域

```
GET /api/shippings/place/for_area/
```

所需 scope: `shippings:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/place/for_area/
```

###返回结果

```json
{
"results": [
        {
            "id": 1,
            "name": "北京市",
            "code": "110000",
            "parent": null,
            "area": "华北",
            "short_name": "北京",
            "pinyin": "BeiJing"
        },
        ...
        ]
}
```

{% endmethod %}



{% method -%}

##查询全地区

```
GET /api/shippings/all_place/
```

所需 scope: `shippings:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/all_place/
```

###返回结果

```json
{
	"name": "北京市",
    "pinyin": "BeiJing",
    "short_name": "北京",
    "code": "110000",
    "children": [
           {
                "name": "市辖区",
                "pinyin": "District",
                "short_name": "市辖区",
                "code": "110100",
                "children": [
                    {
                        "name": "东城区",
                        "pinyin": "DongChengQu",
                        "short_name": "东城区",
                        "code": "110101",
                        "children": []
                    },
                    {
                        "name": "西城区",
                        "pinyin": "XiChengQu",
                        "short_name": "西城区",
                        "code": "110102",
                        "children": []
                    },
                    {
                        "name": "朝阳区",
                        "pinyin": "ZhaoYangQu",
                        "short_name": "朝阳区",
                        "code": "110105",
                        "children": []
                    },
                    {
                        "name": "丰台区",
                        "pinyin": "FengTaiQu",
                        "short_name": "丰台区",
                        "code": "110106",
                        "children": []
                    },
                    {
                        "name": "石景山区",
                        "pinyin": "ShiJingShanQu",
                        "short_name": "石景山区",
                        "code": "110107",
                        "children": []
                    },
                    {
                        "name": "海淀区",
                        "pinyin": "HaiDianQu",
                        "short_name": "海淀区",
                        "code": "110108",
                        "children": []
                    },
                    {
                        "name": "门头沟区",
                        "pinyin": "MenTouGouQu",
                        "short_name": "门头沟区",
                        "code": "110109",
                        "children": []
                    },
                    {
                        "name": "房山区",
                        "pinyin": "FangShanQu",
                        "short_name": "房山区",
                        "code": "110111",
                        "children": []
                    },
                    {
                        "name": "通州区",
                        "pinyin": "TongZhouQu",
                        "short_name": "通州区",
                        "code": "110112",
                        "children": []
                    },
                    {
                        "name": "顺义区",
                        "pinyin": "ShunYiQu",
                        "short_name": "顺义区",
                        "code": "110113",
                        "children": []
                    },
                    {
                        "name": "昌平区",
                        "pinyin": "ChangPingQu",
                        "short_name": "昌平区",
                        "code": "110114",
                        "children": []
                    },
                    {
                        "name": "大兴区",
                        "pinyin": "DaXingQu",
                        "short_name": "大兴区",
                        "code": "110115",
                        "children": []
                    },
                    {
                        "name": "怀柔区",
                        "pinyin": "HuaiRouQu",
                        "short_name": "怀柔区",
                        "code": "110116",
                        "children": []
                    },
                    {
                        "name": "平谷区",
                        "pinyin": "PingGuQu",
                        "short_name": "平谷区",
                        "code": "110117",
                        "children": []
                    }
                ]
            },
            ...
}
```

{% endmethod %}



{% method -%}

##查询指定区域的管辖区域

```
GET /api/shippings/all_place/[id]/
```

所需 scope: `shippings:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/shippings/all_place/39/
```

###返回结果

```json
{
	"name": "河北省",
    "pinyin": "HeBei",
    "short_name": "河北",
    "code": "130000",
    "children": [
        {
            "name": "石家庄市",
            "pinyin": "ShiJiaZhuang",
            "short_name": "石家庄",
            "code": "130100",
            "children": [
                {
                    "name": "市辖区",
                    "pinyin": "District",
                    "short_name": "市辖区",
                    "code": "130101",
                    "children": []
                },
                {
                    "name": "长安区",
                    "pinyin": "ChangAnQu",
                    "short_name": "长安区",
                    "code": "130102",
                    "children": []
                },
                {
                    "name": "桥西区",
                    "pinyin": "QiaoXiQu",
                    "short_name": "桥西区",
                    "code": "130104",
                    "children": []
                },
                {
                    "name": "新华区",
                    "pinyin": "XinHuaQu",
                    "short_name": "新华区",
                    "code": "130105",
                    "children": []
                },
                {
                    "name": "井陉矿区",
                    "pinyin": "JingXingKuangQu",
                    "short_name": "井陉矿区",
                    "code": "130107",
                    "children": []
                },
                {
                    "name": "裕华区",
                    "pinyin": "YuHuaQu",
                    "short_name": "裕华区",
                    "code": "130108",
                    "children": []
                },
                {
                    "name": "藁城区",
                    "pinyin": "GaoChengQu",
                    "short_name": "藁城区",
                    "code": "130109",
                    "children": []
                },
                {
                    "name": "鹿泉区",
                    "pinyin": "LuQuanQu",
                    "short_name": "鹿泉区",
                    "code": "130110",
                    "children": []
                },
                {
                    "name": "栾城区",
                    "pinyin": "LuanChengQu",
                    "short_name": "栾城区",
                    "code": "130111",
                    "children": []
                },
                {
                    "name": "井陉县",
                    "pinyin": "JingXingXian",
                    "short_name": "井陉县",
                    "code": "130121",
                    "children": []
                },
                {
                    "name": "正定县",
                    "pinyin": "ZhengDingXian",
                    "short_name": "正定县",
                    "code": "130123",
                    "children": []
                },
                {
                    "name": "行唐县",
                    "pinyin": "XingTangXian",
                    "short_name": "行唐县",
                    "code": "130125",
                    "children": []
                },
                {
                    "name": "灵寿县",
                    "pinyin": "LingShouXian",
                    "short_name": "灵寿县",
                    "code": "130126",
                    "children": []
                },
                {
                    "name": "高邑县",
                    "pinyin": "GaoYiXian",
                    "short_name": "高邑县",
                    "code": "130127",
                    "children": []
                },
                {
                    "name": "深泽县",
                    "pinyin": "ShenZeXian",
                    "short_name": "深泽县",
                    "code": "130128",
                    "children": []
                },
                {
                    "name": "赞皇县",
                    "pinyin": "ZanHuangXian",
                    "short_name": "赞皇县",
                    "code": "130129",
                    "children": []
                },
                {
                    "name": "无极县",
                    "pinyin": "WuJiXian",
                    "short_name": "无极县",
                    "code": "130130",
                    "children": []
                },
                {
                    "name": "平山县",
                    "pinyin": "PingShanXian",
                    "short_name": "平山县",
                    "code": "130131",
                    "children": []
                },
                {
                    "name": "元氏县",
                    "pinyin": "YuanShiXian",
                    "short_name": "元氏县",
                    "code": "130132",
                    "children": []
                },
                {
                    "name": "赵县",
                    "pinyin": "ZhaoXian",
                    "short_name": "赵县",
                    "code": "130133",
                    "children": []
                },
                {
                    "name": "晋州市",
                    "pinyin": "JinZhouShi",
                    "short_name": "晋州市",
                    "code": "130183",
                    "children": []
                },
                {
                    "name": "新乐市",
                    "pinyin": "XinLeShi",
                    "short_name": "新乐市",
                    "code": "130184",
                    "children": []
                }
            ]
        },
        ...
        ]
}
```

{% endmethod %}