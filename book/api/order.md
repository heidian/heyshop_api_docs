# 订单


{% method -%}

## 获取订单列表

```
GET /api/orders/order/
```

所需 scope: `orders:read` 或 `read`

### Query 参数

```
id: 1002
id__in: 1002,1005
customer: 1001
customer__mobile: 13912345678
order_number: 902120170216180721014779
order_number__in: 902120170216180721014779,433220170204145333437189
order_status: open
order_status__in: open,closed,cancelled
financial_status: paid
financial_status__in: pending,partially_paid,paid
fulfillment_status: fulfilled
fulfillment_status__in: pending,partial,fulfilled
created_at__gte: 2000-01-01T00:00:00.000Z
created_at__lte: 2000-01-01T00:00:00.000Z
updated_at__gte: 2000-01-01T00:00:00.000Z
updated_at__lte: 2000-01-01T00:00:00.000Z
paid_at__gte: 2000-01-01T00:00:00.000Z
paid_at__lte: 2000-01-01T00:00:00.000Z
order_by: id / created_at / -created_at / updated_at / -updated_at / paid_at / -paid_at
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/?page=1
```

### 返回结果

```json
{
  "count": 2,
  "next": "http://neiwai.heidianer.com/api/orders/order/?financial_status=paid&page=2&page_size=2",
  "previous": null,
  "results": [
    {
      // 订单 id，用于更新和查询订单
      "id": 2894,
      "order_number": "3096201701031645115932861",

      /*
       * 订单其他字段参见下方的订单查询接口
       */

    },
    {
      "id": 2895,
      "order_number": "3096201701031645115932861"
    }
  ]
}

```
{% endmethod %}


{% method -%}

## 获取指定订单

```
GET /api/orders/order/[id]/
```

所需 scope: `orders:read` 或 `read`

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    order_number       |  str型，订单编号    |
|    requires_shipping       |  布尔型，是否需要物流    |
|    total_line_items_price        |   所有商品价格综合   |
|    shipping_cost       |    str型，运费  |
|    total_discounts       |   str型，所有折扣金额，包含商品和运费折扣  |
|    total_price       |   str型，订单总价，最终金额 (total_price = total_line_items_price +  shipping_cost - total_discounts)  |
|    order_status       |  订单状态: open: 尚未支付或者尚未发货；cancelled: 3天未支付或者人工取消；closed: 已经支付并且发货    |
|    financial_status       |  支付状态: pending: 未支付；partially_paid: 已支付；paid: 已支付；refunded: 已退款    |
|    fulfillment_status       |   发货状态: pending: 未发货；partial: 部分商品已发货；fulfilled: 已发货   |
|    closed_at      |   订单完成时间   |
|    shipping_address       |   物流信息   |
|    tracking_info       |   物流追踪状态   |
|    tracking_number       |   物流编号   |
|    tracking_url      |   物流状态查询地址   |
|    kd100_data       |   快递100返回的物流历史   |
|    lines       |   订单内商品列表   |
|    price       |   价格，商品价格可能会在下单后变动，该字段不受影响，下单时的商品价格已这个字段为准   |
|    variant       |   SKU 信息（细分商品信息）   |
|    product       |   商品基本信息   |
|    timeline      |   订单操作跟踪   |

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/
```

### 返回结果

```json
{
  "id": 2894,

  "order_number": "3096201701031645115932861",
  "token": "d01f8de68eb3a5bccd3b5dc1a945a35ef41349ea",
  "cart_token": "ac3c9e8a9309bbe1c55653ad222a3c09355949de",

  "requires_shipping": true,

  "full_name": "鬼骨孖",
  "email": "xd@heidianer.com",
  "mobile": "13817337277",
  "buyer_accepts_marketing": true,
  "total_line_items_price": "0.01",
  "total_discounts": "0.00",
  "shipping_cost": "0.00",
  "total_weight": "0.00",
  "total_price": "0.01",
  "order_status": "open",
  "financial_status": "paid",
  "fulfillment_status": "fulfilled",
  "cancelled_at": null,
  "cancel_reason": "",
  "closed_at": null,

  "referring_platform": "web",
  "note": "没有备注！",
  "metafield": {},
  "created_at": "2017-01-03T08:45:11.595964Z",
  "updated_at": "2017-01-04T18:20:39.588614Z",

  "shipping_address": {
    "id": 1113,
    "address1": "泰兴路89号",
    "address2": "3楼裸心社",
    "city": "市辖区",
    "district": "静安区",
    "province": "上海市",
    "full_name": "Xindong Ding",
    "mobile": "18621761807",
    "place": {
      "id": 865,
      "name": "静安区",
      "code": "310106",
      "parent": 861,
      "area": "",
      "short_name": "静安区"
    },
    "customer_address_id": 44,
    "metafield": {},
    "created_at": "2017-01-03T08:45:14.633666Z",
    "updated_at": "2017-01-03T08:45:14.633715Z"
  },
  "fulfillments": [
    {
      "id": 286,
      "status": "pending",
      "tracking_info": "",
      "tracking_company": "不需要物流",
      "tracking_number": "--",
      "tracking_url": "",
      "kd100_data": []
    }
  ],

  "lines": [
    {
      "id": 3204,
      "image": "http://up.img.heidiancdn.com/o_1ap28uqth14kg1dvvi471rj11qhn024.jpeg",
      "title": "大图小商品 - 颜色: 红色, 尺寸: 小号, 材质: 咦",
      "requires_shipping": true,
      "product_title": "大图小商品",
      "variant_title": "颜色: 红色, 尺寸: 小号, 材质: 咦",
      "quantity": 1,
      "price": "0.01",
      "total_price": "0.01",

      "variant": {
        "id": 866,
        "title": "颜色: 红色, 尺寸: 小号, 材质: 咦",
        "image": "http://up.img.heidiancdn.com/o_1ap28uqth14kg1dvvi471rj11qhn024.jpeg",

        "sku": "1501DB",
        "price": "0.01",
        "grams": "0.00",
        "options": [
          {
            "title": "颜色",
            "value": "红色"
          },
          {
            "title": "尺寸",
            "value": "小号"
          },
          {
            "title": "材质",
            "value": "棉"
          }
        ]
      },

      "product": {
        "id": 575,
        "name": "datuxiaoshangp",
        "title": "大图小商品"
      }
    }
  ],

  "discounts": [],
  "timeline": []
}
```
{% endmethod %}



{% method -%}

##刷新支付状态

```
GET /api/orders/order/[id]/check_paid／
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/check_paid／
```

###返回结果

```json
http204
```

{% endmethod %}



{% method -%}

##刷新退款状态

```
GET /api/orders/order/[id]/check_refund/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###返回结果

```json
http204
```

{% endmethod %}


{% method -%}

##退款

```
POST /api/orders/order/[id]/refund/
```

所需 scope: `orders:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/refund/
```

###返回结果

同获取指定订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##取消订单

```
POST /api/orders/order/[id]/cancel/
```

所需 scope: `orders:write` 或 `write`

###数据格式

```json
{
	"reason": "test"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/cancel/
```

###返回结果

同获取指定订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##完成订单

```
POST /api/orders/order/[id]/close/
```

**未完成支付的订单无法调用此接口**

所需 scope: `orders:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/close/
```

###返回结果

同获取指定订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##重新开启订单

```
POST /api/orders/order/[id]/reopen/
```


所需 scope: `orders:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/reopen/
```

###返回结果

同获取指定订单接口 返回完整的订单信息

{% endmethod %}



{% method -%}

##标记订单为已付款状态

```
POST /api/orders/order/[id]/manually_marked_as_paid/
```

所需 scope: `orders:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/manually_marked_as_paid/
```

###返回结果

同获取指定订单接口 返回完整的订单信息

{% endmethod %}


{% method -%}

##修改订单总价

```
POST /api/orders/order/[id]/modify_price/
```

所需 scope: `orders:write` 或 `write`

###数据格式

```json
{
	"total_price_adjustment": "-20.00"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/modify_price/
```

###返回结果

```json
{
    "total_price_adjustment": "2.00"
}
```

{% endmethod %}



{% method -%}

## 商家针对订单留言

```
POST /api/orders/order/[id]/add_comment/
```

所需 scope: `orders:write` 或 `write`

### 数据格式

```js
{
  "comment": "我们会尽快为您发货"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/add_comment/
```

### 返回结果

```json
http 204
```

{% endmethod %}




{% method -%}

## 买家针对订单留言

```
POST /api/customers/order/[id]/add_comment/
```

所需 Authorization: `customertoken`

### 数据格式

```js
{
  "comment": "我已下单，请尽快帮我发货"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/customers/order/2894/add_comment/
```

### 返回结果

```json
http204
```

{% endmethod %}



{% method -%}

##卖家更新评论

```
PATCH /api/orders/order/[id]/update_comment/
```

所需 scope: `orders:write` 或 `write`

###数据格式

```json
{
	"id": 23144,
	"comment": "test",
	"metafield": {}
}
```

{% sample lang="http" -%}

###发送请求

```
PATCH /api/orders/order/2894/update_comment/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##订单状态统计

```
GET /api/orders/order/count/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/count/
```

###返回结果

```json
[
    {
        "title": "全部",
        "count": 9,
        "value": "count"
    },
    {
        "title": "Pending Payment",
        "count": 2,
        "value": "pending_payment"
    },
    {
        "title": "Pending Fulfillment",
        "count": 2,
        "value": "pending_fulfillment"
    },
    {
        "title": "Fulfilled",
        "count": 0,
        "value": "fulfilled"
    },
    {
        "title": "Cancelled",
        "count": 2,
        "value": "cancelled"
    },
    {
        "title": "Closed",
        "count": 3,
        "value": "closed"
    }
]
```

{% endmethod %}


{% method -%}

##查询商品交易进程

```
GET /api/orders/order/[id]/transaction/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/transaction/
```

##返回结果

```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
     {
        "id": 9496,
        "order_id": 25326,
        "transaction_number": "59820171010183422081",
        "amount": "0.05",
        "transaction_type": "sale",
        "status": "pending",
        "payment_backend": "heyshop_delegated",
        "delegated": true,
        "channel": "wx_pub",
        "refund_id": null,
        "refund_url": "",
        "refund_msg": "",
        "voucher_id": 0,
        "refundable_amount": "0.05",
        "issue": "",
        "expires_at": "2017-10-11T10:34:22.081401Z",
        "succeeds_at": null,
        "created_at": "2017-10-10T10:34:22.083955Z"
    },
      ...
  ]
}
```

{% endmethod %}



{% method -%}

##查询指定交易进程

```
GET /api/orders/order/[id]/transaction/[id]/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/transaction/9496/
```

###返回结果

```json
{
    "id": 9496,
    "order_id": 25326,
    "transaction_number": "59820171010183422081",
    "amount": "0.05",
    "transaction_type": "sale",
    "status": "pending",
    "payment_backend": "heyshop_delegated",
    "delegated": true,
    "channel": "wx_pub",
    "refund_id": null,
    "refund_url": "",
    "refund_msg": "",
    "voucher_id": 0,
    "refundable_amount": "0.05",
    "issue": "",
    "expires_at": "2017-10-11T10:34:22.081401Z",
    "succeeds_at": null,
    "created_at": "2017-10-10T10:34:22.083955Z"
}
```

{% endmethod %}



{% method -%}

##手动标记付款

```
POST /api/orders/order/[id]/transaction/
```

所需 scope: `orders:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/transaction/
```

###返回结果

```json
{
	"id": 9496,
    "order_id": 25326,
    "transaction_number": "59820171010183422081",
    "amount": "0.05",
    "transaction_type": "sale",
    "status": "success",
    "payment_backend": "heyshop_delegated",
    "delegated": true,
    "channel": "wx_pub",
    "refund_id": null,
    "refund_url": "",
    "refund_msg": "",
    "voucher_id": 0,
    "refundable_amount": "0.05",
    "issue": "",
    "expires_at": "2017-10-11T10:34:22.081401Z",
    "succeeds_at": null,
    "created_at": "2017-10-10T10:34:22.083955Z"
}
```

{% endmethod %}
