#套餐

{% method -%}

##查询套餐信息

```
GET /api/pricing/plan/
```

所需 scope: `pricing:read` 或 `read`


{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/plan/
```

###返回结果

```json
{
	"id": 48,
    "name": "trial",
    "period": "",
    "title": "Trial Plan ",
    "starts_at": "2016-12-14T00:00:00Z",
    "ends_at": "2019-12-28T00:00:00Z",
    "is_expired": false,
    "can_publish_products": -1,
    "days_left": 842,
    "active_billing": {
        				"id": 1,
        				"billing_type": "renew",
        				"plan_name": "basic",
        				"plan_period": "monthly",
        				"title": "Renew Basic Plan  - Billed monthly",
        				"amount": "500000.00",
        				"starts_at": "2017-05-04T05:47:00Z",
        				"ends_at": "2022-05-04T05:47:00Z",
        				"status": "success",
        				"order_number": "",
        				"order_token": null
    },
    "next_billing": null
    ...
}
```

{% endmethod %}



{% method -%}

##查询指定套餐信息

```
GET /api/pricing/plan/[id]/
```

所需 scope: `pricing:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/plan/3644/
```

###返回结果

```json
{
  	"id": 3644,
    "name": "trial",
    "period": "",
    "title": "test_套餐 ",
    "starts_at": "2017-09-05T16:00:00Z",
    "ends_at": "2017-09-19T16:00:00Z",
    "is_expired": false,
    "can_publish_products": -1,
    "days_left": 13,
    "active_billing": {
        				"id": 10,
        				"billing_type": "renew",
        				"plan_name": "basic",
        				"plan_period": "monthly",
        				"title": "Renew Basic Plan  - Billed monthly",
        				"amount": "10.00",
        				"starts_at": "2017-05-04T05:47:00Z",
        				"ends_at": "2022-05-04T05:47:00Z",
        				"status": "success",
        				"order_number": "",
        				"order_token": null
    },
    "next_billing": null
}
```

{% endmethod %}



{% method -%}

##购买套餐

```
POST /api/pricing/plan/renew/
```

所需 scope: `pricing:write` 或 `write`

###数据格式

```json
{
	"plan_name": "basic", //(basic/standard/advanced)
	"plan_period": "monthly". //(monthly/annual)
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/pricing/plan/renew/
```

###返回结果

```json
{
	"id": 678,
    "billing_type": "renew",
    "plan_name": "advanced",
    "plan_period": "annual",
    "title": "续费高级 套餐  - 按年支付",
    "amount": "6990.00",
    "starts_at": "2017-09-06T16:00:00Z",
    "ends_at": "2018-09-07T16:00:00Z",
    "status": "pending",
    "order_number": "736020170907122617750051",
    "order_token": "f60fe7fd40942629284c14531e6d99faf7b59335"
}
```

{% endmethod %}


{% method -%}

##升级套餐

```
POST /api/pricing/plan/upgrate/
```

所需 scope: `pricing:write` 或 `write`

###数据格式

```json
{
	"plan_name": "standard" // (standard/advanced) 当套餐为最高级时无法继续升级套餐
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/pricing/plan/upgrate/
```

###返回结果

```json
{
    "id": 678,
    "billing_type": "renew",
    "plan_name": "advanced",
    "title": "续费高级 套餐  - 按年支付",
    "amount": "6990.00",
    "starts_at": "2017-09-06T16:00:00Z",
    "ends_at": "2018-09-07T16:00:00Z",
    "status": "pending",
    "order_number": "736020170907122617750051",
    "order_token": "f60fe7fd40942629284c14531e6d99faf7b59335"

}
```

{% endmethod %}



{% method -%}

##查询套餐账单

```
GET /api/pricing/plan_billing/
```

所需 scope: `pricing:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/plan_billing/?page=1
```

### 返回结果

```json
{
	 "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 678,
            "billing_type": "renew",
            "plan_name": "advanced",
            "plan_period": "annual",
            "title": "续费高级 套餐  - 按年支付",
            "amount": "6990.00",
            "starts_at": "2017-09-06T16:00:00Z",
            "ends_at": "2018-09-07T16:00:00Z",
            "status": "pending",
            "order_number": "736020170907122617750051",
            "order_token": "f60fe7fd40942629284c14531e6d99faf7b59335"
        },
        ...
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定套餐账单

```
GET /api/pricing/plan_billing/[id]/
```

所需 scope: `pricing:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/plan_billing/678/
```

###返回结果

```json
{
	"id": 678,
    "billing_type": "renew",
    "plan_name": "advanced",
    "plan_period": "annual",
    "title": "续费高级 套餐  - 按年支付",
    "amount": "6990.00",
    "starts_at": "2017-09-06T16:00:00Z",
    "ends_at": "2018-09-07T16:00:00Z",
    "status": "pending",
    "order_number": "736020170907122617750051",
    "order_token": "f60fe7fd40942629284c14531e6d99faf7b59335"
}
```

{% endmethod %}



{% method -%}

##取消所有未付账单

```
POST /api/pricing/plan_billing/cancel_all_pending/
```

所需 scope: `pricing:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/pricing/plan_billing/cancel_all_pending/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询发票

```
GET /api/pricing/invoice/
```

所需 scope: `pricing:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/invoice/?page=1
```

### 返回结果

```
{
	 "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "category": "personal",
            "status": "pending",
            "amount": "2000.00",
            "title": "个人",
            "taxpayer_num": "",
            "company_bank": "",
            "company_bank_card": "",
            "company_address": "",
            "company_phone_num": "",
            "mobile": "13412345678",
            "full_name": "test",
            "address": "xxxxxxxxx",
            "tracking_company": "",
            "tracking_number": "",
            "reject_reason": ""
        },
        ...
}
```

{% endmethod %}



{% method -%}

##查询指定发票

```
GET /api/pricing/invoice/[id]/
```

所需 scope: `pricing:read` 或 `read`

{% sample lang="http" -%}

###发送请求


```
GET /api/pricing/invoice/1/
```

###返回结果

```json
{
    "id": 1,
    "category": "personal",
    "status": "pending",
    "amount": "2000.00",
    "title": "个人",
    "taxpayer_num": "",
    "company_bank": "",
    "company_bank_card": "",
    "company_address": "",
    "company_phone_num": "",
    "mobile": "13412345678",
    "full_name": "然叔",
    "address": "xxxxxxxxx",
    "tracking_company": "",
    "tracking_number": "",
    "reject_reason":
}
```


{% endmethod %}



{% method -%}

##当前可开票金额

```
GET  /api/pricing/invoice/balance/
```

所需 scope: `pricing:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET  /api/pricing/invoice/balance/
```

###返回结果

```json
{
    "balance": 494000.5
}
```

{% endmethod %}



{% method -%}

##查询代金券

```
GET /api/pricing/checkout/voucher/
```

所需 scope: `pricing:read` 或 `read`

### Query 参数

```
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/pricing/checkout/voucher/?page=1
```

###返回结果

```json
{
	"count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 93,
            "code": "D46D81566D6B0059",
            "title": "Voucher",
            "total_balance": "10.00",
            "balance": "0.01",
            "status": "available",
            "starts_at": null,
            "ends_at": null,
            "white_product_ids": [],
            "black_product_ids": [],
            "minimum_amount": "0.00",
            "expire_days": 0,
            "activate_expires_at": null
        },
        ...
    ]
}
```

{% endmethod %}

