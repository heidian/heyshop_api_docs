
# 帮助中心



{% method -%}

## 获取帮助文章列表

```
GET /api/supports/user_guide/
```

### Query 参数
```
language: zh
language__in: zh,en
kind: catalogue
kind__in: catalogue,order
id: 123
id__in: 123,135
keyword: xxx
```

{% sample lang="http" -%}

###发送请求

```
GET /api/supports/user_guide/?page=1
```

### 返回结果
```json
{
    [
    {
        "id": 2,
        "title": "导航设置",
        "kind": "navigation",
        "language": "zh",
        "tags": [],
        "sections": [
            {
                "id": 4,
                "guide_id": 2,
                "title": "1.默认导航",
                "image": null,
                "content": "默认有三个导航。<br/>\r\n“主菜单”默认应用于PC端网站头部；<br/>\r\n“手机导航”默认应用于手机端；<br/>\r\n“底部菜单”默认应用于PC端网站底部。",
                "position": 1
            },
            {
                "id": 5,
                "guide_id": 2,
                "title": "2.编辑导航",
                "image": null,
                "content": "用户可根据需要对导航进行新建、编辑、删除自定义操作。",
                "position": 2
            },
            {
                "id": 76,
                "guide_id": 2,
                "title": "3.导航应用",
                "image": null,
                "content": "点击管理后台底部的“编辑网站”，进入网站编辑器进行导航应用地方更改。",
                "position": 3
            }
        ]
    },
    ...
    ]
```

{% endmethod %}



{% method -%}

## 获取帮助文章详情

```
GET /api/supports/user_guide/[id]/
```

{% sample lang="http" -%}

###发送请求 

```
GET /api/supports/user_guide/3/
```

### 返回结果

```json
{
    "id": 3,
    "title": "访问链接",
    "kind": "navigation",
    "language": "zh",
    "tags": [],
    "sections": [
        {
            "id": 182,
            "guide_id": 3,
            "title": "1.新增菜单",
            "image": null,
            "content": "1) 点击添加一或二级菜单<br>\r\n2) 选择超链接<br>\r\n3) 输入该页面的链接地址<br>（若该页面地址为http:mysite.heidianer.com/page/products，则输入/page/products即可）<br>\r\n保存后即可实现点击菜单跳转到该页面。",
            "position": 1
        },
        {
            "id": 183,
            "guide_id": 3,
            "title": "2.页面地址",
            "image": null,
            "content": "页面地址是访问该页面的唯一链接。<br>\r\n选择左侧菜单“店铺”-“页面”，可查看对应页面的“页面地址”。",
            "position": 2
        }
    ]
}
```

{% endmethod %}



{% method -%}

## 获取帮助文章标签列表

```
GET /api/supports/user_guide_tag/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/supports/user_guide_tag/
```

### 返回结果

```json
{
    [
        {
            "id": 1,
            "title": "tg1"
        },
        {
            "id": 2,
            "title": "tg2"
        }
    ]
}
```

{% endmethod %}



{% method -%}

##获取帮助文章标签详情

```
GET /api/supports/user_guide_tag/[id]/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/supports/user_guide_tag/7/
```

###返回结果

```json
{
    "id": 7,
    "title": "tg5"
}
```

{% endmethod %}



