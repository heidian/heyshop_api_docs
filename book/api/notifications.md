#消息通知

{% method -%}

##查询消息分类

```
GET /api/notifications/category/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
kind: test_category1
kind__in: test_category1, test_category2
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/category/?page=1
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "category_id": 1,
            "dashboard_enabled": true,
            "email_enabled": false,
            "sms_enabled": false,
            "wechat_enabled": true,
            "name": "name",
            "kind": "测试类型",
            "title": "测试标题",
            "description": "你好好",
            "is_popup": false,
            "allow_dashboard_enable": true,
            "allow_email_enable": false,
            "allow_sms_enable": false,
            "allow_wechat_enable": false
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

##允许获取邮件通知

```
POST /api/notifications/category/[id]/email_enable/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/email_enable/
```

###返回结果
 
```json
{
	"enable":"true"
}
```

{% endmethod %}



{% method -%}

##禁止获取邮件通知

```
POST /api/notifications/category/[id]/email_disable/
```

所需 scope: `notification:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/email_disable/
```

###返回结果

```json
{
	"ebable": "false"
}
```
{% endmethod %}



{% method -%}

##允许手机短信通知

```
POST /api/notifications/category/[id]/sms_enable/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/sms_enable/
```

###返回结果

```json
{
	"enable": "true"
}
```

{% endmethod %}



{% method -%}

##禁止手机短信通知

```
POST /api/notifications/category/[id]/sms_disable/
```

所需 scope: `notifications:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/sms_disable/
```

###返回结果

```json
{
	"enable": "false"
}
```

{% endmethod %}



{% method -%}

##允许Dashboard通知

```
POST /api/notifications/category/[id]/dashboard_enable/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/dashboard_enable/
```

###返回结果

```json
{
	"enable":"true"
}
```

{% endmethod %}



{% method -%}

##禁止Dashboard通知

```
POST /api/notifications/category/[id]／dashboard_disable/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/dashboard_disable/
```

###返回结果

```json
{
	"enable":"false"
}
```

{% endmethod %}



{% method -%}
 
##允许微信通知

```
POST /api/notifications/category/[id]/wechat_enable/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/wechat_enable/
```

###返回结果

```json
{
	"enable":"true"
}
```

{% endmethod %}



{% method -%}
 
##禁止微信通知

```
POST /api/notifications/category/[id]/wechat_disable/
```

所需 scope: `notifications:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/category/1/wechat_disable/
```

###返回结果

```json
{
	"enable":"false"
}
```

{% endmethod %}



{% method -%}

##查询商店消息通知

```
GET /api/notifications/shop_notification/
```

所需 scope: `notifications:read` 或 `read`

### Query 参数

```
is_read: false
kind: account
kind__in: test_account, test_account2  
```

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/shop_notification/?id_read=false
```

###结果返回

```json
{
  "count": 5,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 20450,
            "category": 7,
            "kind": "account",
            "content": "欢迎加入嘿店",
            "detail": "test_content",
            "is_read": false,
            "created_at": "2017-09-06T07:04:10.093383Z"
        },
        ...
    ]
}
```

{% endmethod %}


{% method -%}

##查询指定商店消息通知

```
GET  /api/notifications/shop_notification/[id]/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET  /api/notifications/shop_notification/20450/
```

###结果返回

```json
{
   "id": 20450,
   "category": 7,
   "kind": "account",
   "content": "欢迎加入嘿店",
   "detail": "test_content",
   "is_read": false,
   "created_at": "2017-09-06T07:04:10.093383Z"
}
```

{% endmethod %}


{% method -%}

##查询商店消息汇总

```
GET /api/notifications/shop_notification/summary/
```

所需 scope: `notifications:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/notifications/shop_notification/summary/
```

###结果返回

```json
{
	{
        "unread_count": 1,
        "kind": "",
        "last": {
            "id": 20450,
            "category": 7,
            "kind": "account",
            "content": "欢迎加入嘿店",
            "detail": "test_content",
            "is_read": false,
            "created_at": "2017-09-06T07:04:10.093383Z"
        }
    },
    {
        "unread_count": 1,
        "kind": "account",
        "last": {
            "id": 20450,
            "category": 7,
            "kind": "account",
            "content": "欢迎加入嘿店",
            "detail": "test_content",
            "is_read": false,
            "created_at": "2017-09-06T07:04:10.093383Z"
        }
    },
    {
        "unread_count": 0,
        "kind": "transaction"
    }
}
```

{% endmethod %}



{% method -%}

##将所有消息设为已读

```
POST /api/notifications/shop_notification/read_all/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"id__in": "20450,20451,20452",
	"category_id__in": "1,2,3",
 	"kind":"account"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/shop_notification/read_all/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##将指定消息设为已读

```
POST /api/notifications/shop_notification/[id]/read/
```

所需 scope: `notifications:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/shop_notification/20450/read/
```

###返回结果

```json
http 204
```


{% endmethod %}



{% method -%}

##删除所有消息

```
POST /api/notifications/shop_notification/delete_all/
```

所需 scope: `notifications:write` 或 `write`

###数据格式

```json
{
	"id__in": "20450,20451,20452",
	"category_id__in": "1,2,3",
 	"kind":"account"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/shop_notification/delete_all/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##删除指定消息

```
POST /api/notifications/shop_notification/[id]/delete/
```

所需 scope: `notifications:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/notifications/shop_notification/20450/delete/
```

###返回结果

```json
http 204
```

{% endmethod %}

