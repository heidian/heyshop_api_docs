#结算（Checkout）

{% method -%}
## 创建结算 checkout

```
POST /api/checkout/
```

### Headers

**<font color=red size=3>注意：该请求需要在request header中添加Accept: application/json; version=2.0</font>**

```
headers: { accept: 'application/json;version=2.0' }
```
### 数据格式
```
cart_token: [cartToken], // 购物车token, 可选
lines: [
    {
        variant_id: [variantId],  // 商品规格 ID
        quantity: [quantity] // 商品规格数量
    }
]
```

### 返回响应说明
|       字段     |      说明       |
|      :------   |    :-------    |
|    token        | string 型，checkout 的 token   |
|    polling        | boolean 型，服务器是否正在处理订单  |

{% sample lang="http" -%}

###发送请求

```
POST /api/checkout/
HEADERS {
    accept: 'application/json;version=2.0'
}
PAYLOAD {
    lines: [
        {
            variant_id: 12695,
            quantity: 1
        }
    ]
}
```

### 返回结果
```json
{
    "token": "43cb957cb01589d420241c173877760abd157712",
    "polling": true
}
```


{% endmethod %}


{% method -%}

## 获取结算订单信息

```
GET /api/checkout/[token]/
```


{% sample lang="http" -%}

### 发送请求

```
GET /api/checkout/43cb957cb01589d420241c173877760abd157712/
```

### 返回结果

在创建checkout 之后立即获取 /checkout/[token]/ 的话，服务器可能还在进行处理，这个时候返回的状态码是 `202`，根据状态码来判断是否继续轮询该 checkout，直到报错或者获取到 checkout 信息

1. status.code: 202
```json
{
    "detail": "Order is being processed Expected available in 3 seconds.",
    "code": "throttled"
}
```
2. checkout 处理完成，返回详细信息
```json
{
  "token": "37ca40ab6fd71b8cb7e9815efa5ae95b6f3c22af",
  "customer": null,
  "requires_shipping": true,
  "full_name": "",
  "email": "",
  "mobile": "",
  "buyer_accepts_marketing": true,
  "total_line_items_price": "0.01",
  "total_discounts": "0.00",
  "shipping_cost": "0.10",
  "total_weight": "0.00",
  "total_price": "0.11",
  "attributes": {},
  "note": "",
  "created_at": "2021-05-12T15:40:58.770985+08:00",
  "updated_at": "2021-05-12T15:40:58.948522+08:00",
  "shipping_address": null,
  "lines": [
    {
      "id": 6485722,
      "image": "",
      "title": "aasaaa - 名称: 50.00 元代金券",
      "requires_shipping": true,
      "product_title": "aasaaa",
      "variant_title": "名称: 50.00 元代金券",
      "sku": "DC771C",
      "barcode": "",
      "quantity": 1,
      "price": "0.01",
      "total_price": "0.01",
      "total_discount": "0.0000",
      "variant": {
        "id": 12695,
        "sku": "DC771C",
        "barcode": "",
        "title": "名称: 50.00 元代金券",
        "image": {
          "src": "",
          "metafield": {}
        },
        "price": "0.01",
        "compare_at_price": null,
        "grams": "0.00",
        "options": [
          {
            "title": "名称",
            "value": "50.00 元代金券"
          }
        ],
        "created_at": "2017-05-19T16:21:17.752352+08:00",
        "updated_at": "2021-04-21T18:40:28.261188+08:00"
      },
      "product": {
        "id": 7574,
        "name": "FD1B6B",
        "image": {
          "src": "",
          "metafield": {}
        },
        "title": "aasaaa",
        "description": "",
        "price": "0.01",
        "compare_at_price": null,
        "published": true,
        "published_at": "2020-12-22T14:02:36.323000+08:00",
        "created_at": "2017-05-19T16:21:17.673717+08:00",
        "updated_at": "2021-04-30T14:31:50.794708+08:00"
      },
      "fulfillment_status": "pending",
      "shipping_method_group_id": 858,
      "selectable_shipping_methods": [
        {
          "id": 1946,
          "rate_type": "fixed",
          "is_enabled": true,
          "title": "普通快递",
          "normal_price": "0.10",
          "normal_unit": "0.00",
          "over_price": "0.00",
          "over_unit": "0.00",
          "free_shipping_policy": "nope",
          "free_shipping_price": "0.00",
          "group_id": 858
        }
      ],
      "shipping_method_id": 1946,
      "attributes": {}
    }
  ],
  "discounts": [],
  "coupon_codes": [],
  "is_fulfillment_silent": false,
  "shipping_cost_options": [
    {
      "shipping_method_group_id": 858,
      "shipping_methods": [
        {
          "shipping_method_id": 1946,
          "title": "普通快递",
          "cost": 0.1
        }
      ]
    }
  ],
  "redeem_points": 0,
  "points_price": "0.00"
}
```
{% endmethod %}

{% method -%}
## 绑定结算到买家账户
`和上面的「获取结算订单信息」基本一致，只需要添加 Authorization: customertoken xxxxxxx 的 Request Header`

```
GET /api/checkout/[token]/
```

```
HEADERS {
    Authorization: customertoken [custonerToken]
}
```

{% sample lang="http" -%}

### 发送请求

```
GET /api/checkout/43cb957cb01589d420241c173877760abd157712/
HEADERS {
    Authorization: customertoken 12345678932767627167261s67617167
}
```

### 返回结果
在创建checkout 之后立即获取 /checkout/[token]/ 的话，服务器可能还在进行处理，这个时候返回的状态码是 `202`，根据状态码来判断是否继续轮询该 checkout，直到报错或者获取到 checkout 信息

1. status.code: 202
```json
{
    "detail": "Order is being processed Expected available in 3 seconds.",
    "code": "throttled"
}
```
2. checkout 处理完成，返回详细信息
同获取结算订单信息接口 返回完整的订单信息，且订单信息里的 customer 是 customertoken 对应的用户

{% endmethod %}

{% method -%}
## 更新结算订单信息

```
PATCH /api/checkout/[token]/
```

###数据格式

```json
{
	"requires_shipping": true,
    "full_name": "测试123",
    "email": "test@test.com",
    "mobile": "12345678912",
    "note": "test"
}
```


{% sample lang="http" -%}

###发送请求

```
PATCH /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/
```

### 返回结果

同获取结算订单信息接口 返回完整的订单信息

{% endmethod %}



{% method -%}

## 删除结算订单

```
DElETE /api/checkout/[token]/
```

{% sample lang="http" -%}

###发送请求

```
DElETE /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/
```

### 返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

## 更新收获地址

```
PUT /api/checkout/[token]/update_address/
```

###数据格式

```json
{
	"customer_address_id": 1001		//买家地址信息的ID值
}
```

{% sample lang="http" -%}

###发送请求

```
PUT /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/update_address/
```

### 返回结果

同获取结算订单信息接口 返回完整的订单信息

{% endmethod %}


{% method -%}

## 查询可使用的优惠码

```
GET /api/checkout/[token]/available_coupon_codes/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/available_coupon_codes/
```

### 返回结果

```json
[
    {
        "id": 27502,
        "coupon": {
            "id": 988,
            "title": "test3",
            "verbose_title": "10% off",
            "description": "",
            "token": "54bc2b420c4612e1f35c",
            "code_prefix": "ArApJBDB",
            "discount_type": "percentage",
            "value": "10.00",
            "minimum_amount": "0.00",
            "starts_at": "2017-10-10T16:51:30+08:00",
            "ends_at": null,
            "is_forever": true,
            "quantity": 100,
            "redeem_count": 1,
            "created_at": "2017-10-10T16:52:01.141630+08:00"
        },
        "code": "ArApJBDBfa89f8",
        "status": "unused",
        "verbose_title": "10% off",
        "used_at": null
    },
    ...
]
```

{% endmethod %}

{% method -%}

## 绑定优惠券

```
POST /api/checkout/[token]/add_coupon/
```

###数据格式

```json
{
    "code_prefix": [优惠券兑换码],
    "id": [已兑换优惠券的ID],
	"code": [已兑换优惠券的优惠码 code ]
}
```
`code_prefix`/`id`/`code` 三选一

{% sample lang="http" -%}

###发送请求

```
POST /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/add_coupon/
PAYLOAD {
    code: "TEST1234567890TEST"
}
```

### 返回结果

同获取结算订单信息接口 返回完整的订单信息

{% endmethod %}



{% method -%}

## 移除折扣

```
POST /api/checkout/[token]/remove_coupons/
```

{% sample lang="http" -%}

###发送请求

```
POST /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/remove_coupons/
```

### 返回结果

同获取结算订单信息接口 返回完整的订单信息

{% endmethod %}




{% method -%}

## 绑定寄送方式

```
POST /api/checkout/[token]/bind_shipping_method/
```

###数据格式

```json
[
	{
		"orderline_id": 27472,				//订单条目的id值
		"shipping_method_id": 3244			//寄送方式的id值
	}
]
```

{% sample lang="http" -%}

###发送请求

```
POST /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/bind_shipping_method/
```

### 返回结果

同获取结算订单信息接口 返回完整的订单信息

{% endmethod %}


{% method -%}

## 生成订单（未支付、未发货）

```
POST /api/checkout/[token]/place_order/
```

{% sample lang="http" -%}

###发送请求

```
POST /api/checkout/06f7be87803779f056d51d41523b29131abd2c74/place_order/
```

### 返回结果
此时 checkout 已被生成订单 order

```json
{
    "id": 5155974,  // 订单 ID
    "token": "43cb957cb01589d420241c173877760abd157712"
}
```

{% endmethod %}


{% method -%}

## 查询可使用的代金券

```
GET /api/checkout/[token]/available_vouchers/
```

{% sample lang="http" -%}

###发送请求

```
GET /api/checkout/5bfa16225a258eb0b9a87f9bc10df6bb418e85c1/available_vouchers/
```

### 返回结果

```json
[
    {
        "id": 350,
        "customer": {
            "id": 11409,
            "mobile": "139xxxx2923",
            "full_name": "测试",
            "gender": "unknown",
            "email": "",
            "birthday": null,
            "note": "",
            "accepts_marketing": true,
            "created_at": "2017-09-11T15:14:52.940800+08:00",
            "updated_at": "2017-09-11T15:16:27.940411+08:00",
            "tags": [],
            "addresses": [
                {
                    "id": 5965,
                    "is_default": true,
                    "address1": "xxx",
                    "address2": "",
                    "district": "普陀区",
                    "city": "市辖区",
                    "province": "上海市",
                    "country": "",
                    "full_name": "测试",
                    "mobile": "13916712923",
                    "place_code": "310107",
                    "place": {
                        "id": 866,
                        "name": "普陀区",
                        "code": "310107",
                        "parent": 861,
                        "area": "",
                        "short_name": "普陀区",
                        "pinyin": "PuTuoQu"
                    }
                },
                ...
                }
            ],
            "segments": [],
            "last_order_id": null,
            "last_order_paid_at": null,
            "abandoned_checkouts": 4,
            "orders": 20,
            "paid_orders": 12,
            "total_spent": "1064.27",
            "average_order_price": "0.00",
            "visits": 0,
            "visits_per_day": "0.00",
            "customers_referred": 0,
            "total_spent_of_referred": "0.00"
        },
        "code": "ABC3AE42BEE5F706",
        "title": "Voucher",
        "total_balance": "100.00",
        "balance": "100.00",
        "status": "available",
        "white_product_ids": [],
        "black_product_ids": [],
        "minimum_amount": "0.00",
        "expire_days": 360,
        "activate_expires_at": "2017-10-10T16:32:38+08:00",
        "starts_at": "2017-10-10T16:32:46.450484+08:00",
        "ends_at": "2018-10-05T16:32:46.450484+08:00",
        "available_amount": 0.01
    },
    ...
]
```

{% endmethod %}

