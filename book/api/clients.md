#客户端

{% method -%}

##获取七牛Token

```
GET /api/clients/qiniu-uptoken/
```

### Query 参数

```
policy: 
```

{% sample lang="http" -%}

###发送数据

```
GET /api/clients/qiniu-uptoken/
```
lang="http"
###返回结果

```json
{
    "uptoken": "Dl9yzO06apYucr3q9s4IhRiohhHfWDjGi8XgcIU8:DYIla24GEt1BvA-1vMRd4sOs32k=:eyJzY29wZSI6ImhlaWRpYW51cCIsImRlYWRsaW5lIjoxNTM5NjczOTQzfQ==",
    "expires_in": 31536000
}
```

{% endmethod %}



{% method -%}

##获取微信JSAPITicket

```
GET /api/clients/wechat-jsapi-ticket/
```

{% sample lang="http" -%}

###发送数据

```
GET /api/clients/wechat-jsapi-ticket/
```

###返回结果

```json
{
    "appid": "wx3b0fxxxxxfc3ab10",
    "access_token": "XfAddL-0dLMRBb8DMrFjrY1fqmR3QSY7xxxxxxxxxxx7u0KBMuBaRgHA2obS3YRSN96rV3s1lENw14b8dN5viA1I3m4wDkV30XBn7iIc0AJNaAIARAE",
    "expires_in": 1068,
    "ticket": "kgt8ON7yVITDhtdwci0qeejUkZFLldqxxxxxxxxxxxrkywyf4Kc-6JkQCpOKvnplAU5AtlMOB-B7g"
}
```

{% endmethod %}



{% method -%}

##获取微信授权链接

```
POST /api/clients/wechat-auth-url/
```

{% sample lang="http" -%}

###发送数据

```
POST /api/clients/wechat-auth-url/
```

###返回结果

```json
{
    "url": "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b0f9bf95fc3ab10&redirect_uri=https%3A%2F%2Fheidianapi.com%2Fapi%2Fclients%2Fwechat-auth-redirect%2F&response_type=code&scope=snsapi_base&state=ec20a9b1d6&component_appid=wx3d81ff707739d500#wechat_redirect"
}
```

{% endmethod %}



{% method -%}

##获取商店私密数据

```
GET /api/clients/shop-secrets/
```

所需scope `secret`

{% sample lang="http" -%}

###发送数据

```
GET /api/clients/shop-secrets/
```

###返回结果

```json
{
    "multipass_secret": "17c6b897fxxxxxxxx43aa4dae8f8e",
    "webhook_secret": "6c97be4b4axxxxxxxx118db3528cbd4b"
}
```

{% endmethod %}