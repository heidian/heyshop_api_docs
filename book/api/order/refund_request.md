# 退款申请


{% method -%}

##查询退款申请列表

```
GET /api/orders/refund_request/
```

所需 scope: `orders:read` 或 `read`

### Query 参数

```
id: 98
order: 23160
status: pending|processing|rejected|closed
created_at__gte: 2000-01-01T00:00:00.000Z
created_at__lte: 2000-01-01T00:00:00.000Z
page: 1
page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/refund_request/
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 98,
            "order_id": 23160,
            "status": "pending",
            "request_reason": "与商家协商一致退款",
            "reject_reason": "",
            "note": "",
            "created_at": "2017-09-11T10:57:21.600619Z",
            "updated_at": "2017-09-11T10:57:21.600674Z"
        }
        ...
    ]
}
```

{% endmethod %}


{% method -%}

##查询指定订单的退款申请列表

```
GET /api/orders/order/[id]/refund_request/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/23161/refund_request/
```

###返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 98,
            "order_id": 23160,
            "status": "pending",
            "request_reason": "与商家协商一致退款",
            "reject_reason": "",
            "note": "",
            "created_at": "2017-09-11T10:57:21.600619Z",
            "updated_at": "2017-09-11T10:57:21.600674Z"
        }
        ...
    ]
}
```

{% endmethod %}


{% method -%}

##查询指定退款申请

```
GET /api/orders/order/[id]/refund_request/[id]/
```

所需 scope: `orders:read` 或 `read`

###status 说明

|    status      |   说明                   |
|    :------     |   :-------               |
|    pending     |   待处理                  |
|    processing  |   已处理, 沟通中, 退货中   |
|    rejected    |   已拒绝                 |
|    closed      |   已完成                 |

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/23161/refund_request/98/
```

###返回结果

```json
{
    "id": 98,
    "order_id": 23160,
    "status": "pending",
    "request_reason": "与商家协商一致退款",
    "reject_reason": "",
    "note": "",
    "created_at": "2017-09-11T10:57:21.600619Z",
    "updated_at": "2017-09-11T10:57:21.600674Z"
}
```

{% endmethod %}


{% method -%}

##处理退款申请 (通知买家)

```
POST /api/orders/order/[id]/refund_request/[id]/process/
```

所需 scope: `orders:write` 或 `write`

###数据格式

```json
{
    "note": "退款处理中, 请将商品寄回, 地址是 xxx"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/23161/refund_request/99/process/
```

###返回结果

```json
{
    "id": 99,
    "order_id": 23161,
    "status": "processing",
    "request_reason": "与商家协商一致退款",
    "reject_reason": "",
    "note": "退款处理中, 请将商品寄回, 地址是 xxx",
    "created_at": "2017-09-11T11:16:35.493681Z",
    "updated_at": "2017-09-11T11:16:35.493734Z"
}
```

{% endmethod %}


{% method -%}

##拒绝退款申请

```
POST /api/orders/order/[id]/refund_request/[id]/reject/
```

所需 scope: `orders:write` 或 `write`

###数据格式

```json
{
    "reject_reason": "商品已发货, 无法退款"
}
```

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/23161/refund_request/98/reject/
```

###返回结果

```json
{
    "id": 98,
    "order_id": 23160,
    "status": "rejected",
    "request_reason": "与商家协商一致退款",
    "reject_reason": "商品已发货, 无法退款",
    "note": "",
    "created_at": "2017-09-11T10:57:21.600619Z",
    "updated_at": "2017-09-11T11:11:20.756167Z"
}
```

{% endmethod %}



{% method -%}

##完成退款申请

```
POST /api/orders/order/[id]/refund_request/[id]/close/
```

所需 scope: `orders:write` 或 `write`


{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/23161/refund_request/99/close/
```

###返回结果

```json
{
    "id": 99,
    "order_id": 23161,
    "status": "closed",
    "request_reason": "与商家协商一致退款",
    "reject_reason": "",
    "note": "",
    "created_at": "2017-09-11T11:16:35.493681Z",
    "updated_at": "2017-09-11T11:16:35.493734Z"
}
```

{% endmethod %}
