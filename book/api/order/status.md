
## 订单状态说明

嘿店订单都包括三个状态字段，分别是 `order_status`, `financial_status` 和 `fulfillment_status`

---

### 订单状态 `order_status`

状态说明
    - open 活跃状态，买家可以完成付款，申请退款，卖家可以退款
    - closed 关闭状态，订单不可以进行任何操作，除非卖家重新打开订单，即 状态从 closed 变成 open
    - cancelled 取消状态，订单不可以进行任何操作，只有未付款或者退款成功的订单才可以取消

---

### 支付状态 `financial_status`

状态说明
    - pending
    - partially_paid
    - paid
    - refunded

---

### 发货状态 `fulfillment_status`

发货状态是指卖家发出了包裹，并且更新了

**发货状态不包含包裹的送达状态**

状态说明
    - pending 未发货
    - partially_fulfilled 部分发货
    - fulfilled 全部发货

---
