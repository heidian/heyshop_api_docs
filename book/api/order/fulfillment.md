# 订单发货


{% method -%}

## 创建订单发货

```
POST /api/orders/order/[id]/fulfillment/
```

所需 scope: `orders:write` 或 `write`

### 数据格式

```json
{
  "tracking_company": "顺丰速运",
  "tracking_number": 21837938274
}
```

#### 快递公司列表
```
顺丰速运
申通快递
圆通快递
中通快递
韵达快递
天天快递
百世快递
全峰快递
德邦物流
```
（tracking_company 参数建议从以下列表中选择一个，否则可能无法跟踪物流信息）

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/fulfillment/
```

### 返回结果

```json
{
  "id": 291,
  "status": "pending",
  "tracking_info": "",
  "tracking_company": "顺丰速运",
  "tracking_number": "21837938274",
  "tracking_url": "",
  "kd100_data": []
}
```

{% endmethod %}


{% method -%}

##查询订单发货列表

```
GET /api/orders/order/[id]/fulfillment/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/fulfillment/
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 3199,
            "status": "success",
            "tracking_info": "",
            "tracking_company": "",
            "tracking_number": "",
            "tracking_url": "",
            "kd100_data": [],
            "receipt": {
                "content": "test"
            },
            ...
        }
    ]
}
```

{% endmethod %}



{% method -%}

##查询指定id的订单发货信息

```
GET /api/orders/order/[id]/fulfillment/[fulfillment_id]/
```

所需 scope: `orders:read` 或 `read`

{% sample lang="http" -%}

###发送请求

```
GET /api/orders/order/2894/fulfillment/3199/
```

###返回结果

```json
{
    "id": 3199,
    "status": "success",
    "tracking_info": "",
    "tracking_company": "",
    "tracking_number": "",
    "tracking_url": "",
    "kd100_data": [],
    "receipt": {
        "content": "test"
    }
}
```

{% endmethod %}


{% method -%}

## 取消订单发货

```
POST /api/orders/order/[id]/fulfillment/[fulfillment_id]/cancel/
```

所需 scope: `orders:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/fulfillment/291/cancel/
```

### 返回结果

```json
{
  "id": 291,
  "status": "cancelled",
  "tracking_info": "",
  "tracking_company": "顺丰速运",
  "tracking_number": "21837938274",
  "tracking_url": "",
  "kd100_data": []
}
```

{% endmethod %}



{% method -%}

## 标记订单发货为已签收

系统会自动追踪订单发货的状态并标记为签收，也可以通过接口直接标记

```
POST /api/orders/order/[id]/fulfillment/[fulfillment_id]/close/
```

所需 scope: `orders:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
POST /api/orders/order/2894/fulfillment/291/close/
```

### 返回结果

```json
{
  "id": 291,
  "status": "closed",
  "tracking_info": "",
  "tracking_company": "顺丰速运",
  "tracking_number": "21837938274",
  "tracking_url": "",
  "kd100_data": []
}
```

{% endmethod %}
