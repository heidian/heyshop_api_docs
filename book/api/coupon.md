
# 优惠券

{% method -%}

##查询全部优惠券

```
GET /api/discounts/coupon/
```

所需 scope: `discounts:read` 或 `read`


### Query 参数

```
id: 1001  //筛选指定id的优惠券
id__in: 1001,1002,1003 //批量筛选指定id的优惠券
code_prefix: JxxxxY //兑换码
code_prefix__in: JxxxxX,JxxxxY,JxxxxZ //批量查找兑换码
can_redeem: true //是否可以通过优惠口令兑换
price_rule__value_type: fixed_amount //筛选优惠类型 percentage / fixed_amount 
price_rule__value_type__in: fixed_amount, percentage //批量筛选优惠类型
price_rule__title: test //筛选指定title的优惠券
price_rule__title__icontains: es //筛选title中含有指定字符的优惠券
created_at__lte: 筛选创建时间小于等于指定时间的优惠券
created_at__gte: 筛选创建时间大于等于指定时间的优惠券
page: 1  //页号
page_size: 100  //分页大小
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|    code_prefix       |  str型，兑换码    |
|    can_redeem       |  Boolean型，是否可以通过优惠口令兑换    |
|    used_count       |  int型，已被使用次数    |
|    redeem_count       |  int型，已被兑换次数    |
|    price_rule       |  Object，价格规则    |
|    allocation_limit       |  int型，分配限额，0表示无限制    |

{% sample lang="example" -%}

###示例请求

```
GET /api/discounts/coupon/?id=3077&code_prefix= JxxxxY&can_redeem=true
```

###返回结果

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 3077,
            "code_prefix": "JxxxxY",
            "can_redeem": true,
            "created_at": "2019-05-16T11:36:48.068390+08:00",
            "used_count": 0,
            "redeem_count": 0,
            "price_rule_id": 5527,
            "price_rule": {
                "id": 5527,
                "is_enabled": true,
                "priority": 0,
                "title": "showroom61满4件8折",
                "verbose_title": "满4件打8折",
                "usage_limit": 0,
                "once_per_customer": false,
                "target_selection": "entitled",
                "entitled_product_ids": [
                    ...
                ],
                "entitled_collection_ids": null,
                "entitled_category_ids": null,
                "entitled_vendor_titles": null,
                "starts_at": "2019-05-24T00:00:00+08:00",
                "ends_at": "2019-06-02T00:00:00+08:00",
                "automatic_discount": false,
                "target_type": "line_item",
                "allocation_method": "across",
                "value": "-20.00",
                "value_type": "percentage",
                "accumulate": false,
                "prerequisite_quantity_range": {
                    "greater_than_or_equal_to": "4"
                },
                "prerequisite_shipping_price_range": null,
            	"prerequisite_subtotal_range": null,
                "prerequisite_to_entitlement_quantity_ratio": null,
                "allocation_limit": 0
            },
            "legacy_fields": {}
        }
    ]
}     
```

{% endmethod %}



{% method -%}

##查询指定优惠券

```
GET /api/discounts/coupon/[id]/
```

所需 scope: `discounts:read` 或 `read`

{% sample lang="example" -%}

###示例请求

```
GET /api/discounts/coupon/986/
```

###返回结果

```json
{
    "id": 3077,
    "code_prefix": "JxxxxY",
    "can_redeem": true,
    "created_at": "2019-05-16T11:36:48.068390+08:00",
    "used_count": 0,
    "redeem_count": 0,
    "price_rule_id": 5527,
    "price_rule": {
        "id": 5527,
        "is_enabled": true,
        "priority": 0,
        "title": "showroom61满4件8折",
        "verbose_title": "满4件打8折",
        "usage_limit": 0,
        "once_per_customer": false,
        "target_selection": "entitled",
        "entitled_product_ids": [
            ...
        ],
        "entitled_collection_ids": null,
        "entitled_category_ids": null,
        "entitled_vendor_titles": null,
        "starts_at": "2019-05-24T00:00:00+08:00",
        "ends_at": "2019-06-02T00:00:00+08:00",
        "automatic_discount": false,
        "target_type": "line_item",
        "allocation_method": "across",
        "value": "-20.00",
        "value_type": "percentage",
        "accumulate": false,
        "prerequisite_quantity_range": {
            "greater_than_or_equal_to": "4"
        },
        "prerequisite_shipping_price_range": null,
        "prerequisite_subtotal_range": null,
        "prerequisite_to_entitlement_quantity_ratio": null,
        "allocation_limit": 0
    },
    "legacy_fields": {}
}
``` 
{% endmethod %}



{% method -%}

## 创建优惠券

```
POST /api/discounts/coupon/
```

所需 scope: `discounts:write` 或 `write`

### 数据格式
```json
{
  "title" : "test_title",
  "code_prefix" : "aaaaxxxx",
  "can_redeem": true,
  "minimum_amount" : 0,
  "price_rule": {
        "is_enabled": true,
        "priority": 0,
        "title": "test_price_rule_title",
        "verbose_title": "减-0元",
        "usage_limit": 0,
        "once_per_customer": false,
        "target_selection": "all",
        "entitled_product_ids": [1001,1002], //添加折扣的商品id，可以为null
        "starts_at": null,
        "ends_at": null,
        "automatic_discount": false,
        "target_type": "line_item",
        "allocation_method": "across",
        "value": "-10.00",
        "value_type": "fixed_amount",
        "accumulate": false
    }
}
```

{% sample lang="example" -%}

###示例请求

```
POST /api/discounts/coupon/
```

### 返回结果
```json
{
    "id": 3097,
    "code_prefix": "aaaaxxxx",
    "can_redeem": true,
    "created_at": "2019-05-24T12:37:21.270866+08:00",
    "used_count": 0,
    "redeem_count": 0,
    "price_rule_id": 5547,
    "price_rule": {
        "id": 5547,
        "is_enabled": true,
        "priority": 0,
        "title": "test_price_rule_title",
        "verbose_title": "减10元",
        "usage_limit": 0,
        "once_per_customer": false,
        "target_selection": "all",
        "entitled_product_ids": null,
        "entitled_collection_ids": null,
        "entitled_category_ids": null,
        "entitled_vendor_titles": null,
        "starts_at": null,
        "ends_at": null,
        "automatic_discount": false,
        "target_type": "line_item",
        "allocation_method": "across",
        "value": "-10.00",
        "value_type": "fixed_amount",
        "accumulate": false,
        "prerequisite_quantity_range": null,
        "prerequisite_shipping_price_range": null,
        "prerequisite_subtotal_range": null,
        "prerequisite_to_entitlement_quantity_ratio": null,
        "allocation_limit": 0
    },
    "legacy_fields": {}
}
```

{% endmethod %}


{% method -%}

## 删除优惠券

```
DELETE /api/discounts/coupon/[id]/
```

所需 scope: `discounts:write` 或 `write`

{% sample lang="example" -%}

###示例请求

```
DELETE /api/discounts/coupon/986/
```

###返回结果

```json  
http 204
```

{% endmethod %}



{% method -%}

##获取优惠码列表

```
GET /api/discounts/coupon_code/
```

所需 scope: `discounts:read` 或 `read`

### Query 参数

```
id: 1001
id__in: 1001,1002,1003
status: used
status__in: used, unused
customer: 1001    //参数值为customer的id值
coupon: 100 //某个指定的coupon的id值 
```

{% sample lang="example" -%}

###示例请求

```
GET /api/discounts/coupon_code/?status=available
```

###返回结果

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 27518,
            "customer": {
                "id": 123,
                "mobile": "15900960029",
                "full_name": "",
                "gender": "unknown",
                "email": null
            },
            "coupon": {
                "id": 987,
                "code_prefix": "jB7nNJPb",
                "can_redeem": true
         },
         ...
    ]
}
```
{% endmethod %}



{% method -%}

##将优惠码发放给指定用户

```
POST /api/discounts/coupon_code/
```

所需 scope: `discounts:write` 或 `write`

###数据格式

```json
[
	{
    	"customer_id": 123,
    	"coupon_id": 27518
	},
	{
	    "customer_id": 22750,
	    "coupon_id": 1335
	}	
]
```

{% sample lang="example" -%}

###示例请求

```
POST /api/discounts/coupon_code/
```

###返回结果

```json
{
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 27518,
            "customer": {
                "id": 123,
                "mobile": "15900960029",
                "full_name": "",
                "gender": "unknown",
                "email": null
            },
            "coupon": {
                "id": 987,
                "code_prefix": "jB7nNJPb",
                "can_redeem": true
            },
            "customer_id": 123,
            "coupon_id": 987,
            "code": "jB7nNJPbf16adc",
            "status": "unused",
            "used_at": null,
            "created_at": "2017-10-11T14:53:34.083556+08:00",
            "starts_at": "2017-10-10T15:34:29+08:00",
            "ends_at": null,
            "title": "test2",
            "verbose_title": "打5折"
        },
        ...
    ]
}
```

{% endmethod %}



{% method -%}

##更新优惠码

```
PUT /api/discounts/coupon_code/[id]/
```

所需 scope: `discounts:write` 或 `write`

###数据格式

```json
{
	"customer_id": 123,
	"coupon_id": 27518,
	"code": "jB7nNJPbf16adc",
    "status": "unused"
}
```

{% sample lang="example" -%}

###示例请求

```
PUT /api/discounts/coupon_code/27518/
```

###返回结果

```json
{
    "id": 27518,
    "customer": {
        "id": 123,
        "mobile": "15900960029",
        "full_name": "",
        "gender": "unknown",
        "email": null
    },
    "coupon": {
        "id": 987,
        "code_prefix": "jB7nNJPb",
        "can_redeem": true
    },
    "customer_id": 123,
    "coupon_id": 987,
    "code": "jB7nNJPbf16adc",
    "status": "unused",
    "used_at": null,
    "created_at": "2017-10-11T14:53:34.083556+08:00",
    "starts_at": "2017-10-10T15:34:29+08:00",
    "ends_at": null,
    "title": "test2",
    "verbose_title": "打5折"
}
```

{% endmethod %}



{% method -%}

##删除优惠码

```
DELETE /api/discounts/coupon_code/[id]/
```

所需 scope: `discounts:write` 或 `write`

{% sample lang="example" -%}

###示例请求

```
DELETE /api/discounts/coupon_code/993/
```

###返回结果

```json
http 204
```

{% endmethod %}



{% method -%}

##查询优惠券价格规则

```
GET /api/discounts/price_rule/
```

所需 scope: `discounts:read` 或 `read`


### Query 参数

```
id: 1001
id__in: 1001,1002
automatic_discount: true //是否自动应用到 checkout 上面, 只有 false 的才可以被关联到 coupon 上面
title: test
starts_at: 2017-10-10T16:51:30+08:00
ends_at: 2018-10-10T16:51:30+08:00
page: 1  //页号
page_size: 100  //分页大小
```

###部分字段说明

|       字段     |      说明       |
|      :------   |    :-------    |
|     is_enabled    |    Boolean型，是否可用   |
|     priority    |    int型，优先级 数字越大越优先 可为负数    |
|     title    |    str型，规则标题  |
|     verbose_title    |    str型，标题描述  |
|     usage_limit    |    int型，使用次数上限，0表示无限数量 |
|     once_per_customer    |    Boolean型，每个用户是否只能享用一次  |
|     value    |    str型，折扣价格，为负数  |
|     value_type    |    str型，折扣类型，百分比还是价格  |


{% sample lang="example" -%}

###示例请求

```
GET /api/discounts/price_rule/?page=1
```

###返回结果

```json
{
    "count": 13,
    "next": "https://xxxx.heidianer.com/api/discounts/price_rule/?page=2",
    "previous": null,
    "results": [
        {
            "id": 5389,
            "is_enabled": true,
            "priority": 0,
            "title": "test1",
            "verbose_title": "满3件打9.6折",
            "usage_limit": 9999,
            "once_per_customer": false,
            "target_selection": "all",
            "entitled_product_ids": null,
            "entitled_collection_ids": null,
            "entitled_category_ids": null,
            "entitled_vendor_titles": null,
            "starts_at": "2019-03-11T16:08:23.609673+08:00",
            "ends_at": null,
            "automatic_discount": false,
            "target_type": "line_item",
            "allocation_method": "across",
            "value": "-4.00",
            "value_type": "percentage",
            "accumulate": false,
            "prerequisite_quantity_range": {
                "greater_than_or_equal_to": "3"
            },
            "prerequisite_shipping_price_range": null,
            "prerequisite_subtotal_range": null,
            "prerequisite_to_entitlement_quantity_ratio": null,
            "allocation_limit": 0
        },
        ...
    ]
}
```

{% endmethod %}



{% method -%}

##创建优惠券价格规则

```
POST /api/discounts/price_rule/
```

所需 scope: `discounts:write` 或 `write`

### 数据格式
```json
{
    "is_enabled": true,
    "priority": 0,
    "title": "test_price_rule_title",
    "verbose_title": "减-10元",
    "usage_limit": 0,
    "once_per_customer": false,
    "target_selection": "all",
    "entitled_product_ids": null, //添加折扣的商品id，可以为null，target_selection 是 all 的时候不能设置 entitled ids
    "automatic_discount": false,
    "target_type": "line_item",
    "allocation_method": "across",
    "value": "-10.00",
    "value_type": "fixed_amount",
    "accumulate": false,
    "starts_at": " 2017-10-10T16:51:30+08:00", //日期格式YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]
    "ends_at": " 2019-10-10T16:51:30+08:00"
}
```

{% sample lang="example" -%}

###示例请求

```
POST /api/discounts/price_rule/
```

###返回结果

```json
{
    "id": 5549,
    "is_enabled": true,
    "priority": 0,
    "title": "test_price_rule_title",
    "verbose_title": "减10元",
    "usage_limit": 0,
    "once_per_customer": false,
    "target_selection": "all",
    "entitled_product_ids": null,
    "entitled_collection_ids": null,
    "entitled_category_ids": null,
    "entitled_vendor_titles": null,
    "starts_at": null,
    "ends_at": null,
    "automatic_discount": true,
    "target_type": "line_item",
    "allocation_method": "across",
    "value": "-10.00",
    "value_type": "fixed_amount",
    "accumulate": false,
    "prerequisite_quantity_range": null,
    "prerequisite_shipping_price_range": null,
    "prerequisite_subtotal_range": null,
    "prerequisite_to_entitlement_quantity_ratio": null,
    "allocation_limit": 0
}
```

{% endmethod %}



{% method -%}

##更新优惠券价格规则

```
PUT /api/discounts/price_rule/[id]/
```

所需 scope: `discounts:write` 或 `write`

### 数据格式
```json
{
    "is_enabled": true,
    "priority": 0,
    "title": "test_price_rule_title",
    "verbose_title": "减-10元",
    "usage_limit": 0,
    "once_per_customer": false,
    "target_selection": "all",
    "entitled_product_ids": null,
    "automatic_discount": false,
    "target_type": "line_item",
    "allocation_method": "across",
    "value": "-10.00",
    "value_type": "fixed_amount",
    "accumulate": false,
    "starts_at": "2017-10-10T16:51:30+08:00",
    "ends_at": "2019-10-10T16:51:30+08:00"
}
```

{% sample lang="example" -%}

###示例请求

```
PUT /api/discounts/price_rule/5549/
```

###返回结果

```json
{
    "id": 5549,
    "is_enabled": true,
    "priority": 0,
    "title": "test_price_rule_title",
    "verbose_title": "减10元",
    "usage_limit": 0,
    "once_per_customer": false,
    "target_selection": "all",
    "entitled_product_ids": null,
    "entitled_collection_ids": null,
    "entitled_category_ids": null,
    "entitled_vendor_titles": null,
    "starts_at": "2017-10-10T16:51:30+08:00",
    "ends_at": "2019-10-10T16:51:30+08:00",
    "automatic_discount": true,
    "target_type": "line_item",
    "allocation_method": "across",
    "value": "-10.00",
    "value_type": "fixed_amount",
    "accumulate": false,
    "prerequisite_quantity_range": null,
    "prerequisite_shipping_price_range": null,
    "prerequisite_subtotal_range": null,
    "prerequisite_to_entitlement_quantity_ratio": null,
    "allocation_limit": 0
}
```

{% endmethod %}



{% method -%}

## 删除优惠券价格规则

```
DELETE /api/discounts/price_rule/[id]/
```

所需 scope: `discounts:write` 或 `write`

{% sample lang="example" -%}

###示例请求

```
DELETE /api/discounts/price_rule/5549/
```

###返回结果

```json  
http 204
```

{% endmethod %}






