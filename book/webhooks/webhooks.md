# Webhook


##什么是 webhook

webhook 允许第三方应用，监听站点中特定事件，当这些事件发生时，嘿店将特定事件的参数，通过 HTTP 协议以 POST 方式通知指定的 URL。

通过 webhook 获取数据，可以极大降低开发成本。例如，应用要获取新增商品或新订单信息，可通过 webhook 接收通知信息，而无需轮询。

你可以创建多个 webhook，目前嘿店 webhook 支持以下事件:

|   事件  |  说明  |
|      :------   |    :-------    |
|   product_published  |  商品上架    |
|   order_paid  |  订单成功支付    |


##响应 webhook 通知

当事件被触发时，嘿店会立刻通过HTTP POST访问指定 URL。Webhook 通知是以 post 形式发送的 JSON ，放在请求的 body 里，内容是事件对应相关数据。

在接收到嘿店的 webhook 请求后，需要在 10 秒内返回 HTTP 200 的状态码。否则，嘿店会认为 webhook 通知失败，启用重试机制。


##webhook 重试机制

嘿店发送的 webhook 通知，如果在 10 秒内，没得到 HTTP 200 的状态码返回，该条通知，会启用重试机制，将在 5 分钟后在推送一次。


##验证 webhook 通知来源

在应用中，可以通过计算并核对数字签名，来验证 Webhook 的请求，是否来自嘿店。

来自嘿店的 webhook，都包含名为 `X-HeyShop-Sha256` 的 HTTP 头，它是使用店铺的 `webhook_secret`，对传输的数据进行 SHA256 加密后获得。

为避免第三方恶意伪造数据，强烈建议在应用中，强制检测 webhook 的真实性。具体方法如下：

1. 在 `/api/shops/shop_settings/` 接口中获取店铺的 `webhook_secret`
2. 将请求的 body 与 `webhook_secret` 进行字符串拼接
3. 将第 2 步中的结果使用 SHA256 算法进行加密，并且结果全部取大写字母
4. 将第 3 步中的结果与 header 中的 `X-HeyShop-Sha256` 比对，如果一致则是合法请求，否则为伪造的请求



##webhook header

完整的 webhook 请求的 header 中包含以下数据：

+ `X-HeyShop-Shop-Domain` 店铺域名
+ `X-HeyShop-Shop-Id` 店铺id
+ `X-HeyShop-Shop` 店铺名
+ `X-HeyShop-Event` 发生事件
+ `X-HeyShop-Sha256` 签名字符串



{% method %}
##创建一个 webhook

```
POST /api/webhooks/webhook/
```

所需 scope: `webhooks:write` 或 `write`

###数据格式

```json
{
  "event": "product_published",
  "url": "https:\\www.example.com/publish_product"
}
```


{% sample lang="http" -%}

###发送请求

```
POST /api/webhooks/webhook/
```

###返回结果

```json
{
    "id": 7,
    "application_id": 2,
    "event": "product_published",
    "url": "https://www.example.com/publish_product",
    "metafield": {},
    "created_at": "2017-09-06T09:12:30.732348Z",
    "updated_at": "2017-09-06T09:12:30.732404Z"
}
```

{% endmethod %}






{% method -%}

## 获取webhook列表

```
GET /api/webhooks/webhook/
```

所需 scope: `webhook:read` 或 `read`

### Query 参数

```
  page: 1
  page_size: 10
```

{% sample lang="http" -%}

###发送请求

```
GET /api/webhooks/webhook/?page=1
```

### 返回结果

```json
{
	"count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "application_id": 1,
            "event": "order_paid",
            "url": "http://123.com",
            "metafield": {},
            "created_at": "2017-08-04T08:39:33.563838Z",
            "updated_at": "2017-08-04T08:39:33.563931Z"
        },
        ...
               ]
}
```

{% endmethod %}



{% method %}
##修改指定webhook

```
PATCH /api/webhooks/webhook/[id]/
```

所需 scope: `webhooks:write` 或 `write`

###数据格式
```json
{
  "application_id": 20,
  "event": "product_published",
  "url": "https:\\www.example.com/publish_product_test",
  "metafield": {}

}
```
{% sample lang="http" -%}

###发送请求

```
PATCH /api/webhooks/webhook/7/
```

###返回结果

```json
{
    "id": 7,
    "application_id": 20,
    "event": "product_published",
    "url": "https:\\www.example.com/publish_product_test",
    "metafield": {},
    "created_at": "2017-09-06T09:12:30.732348Z",
    "updated_at": "2017-09-06T09:12:30.732404Z"
}

```

{% endmethod %}



{% method %}

##删除指定webhook

```
DELETE /api/webhooks/webhook/[id]/
```

所需 scope: `webhooks:write` 或 `write`

{% sample lang="http" -%}

###发送请求

```
DELETE /api/webhooks/webhook/7/
```

###返回结果

```json
http 204
```

{% endmethod %}



