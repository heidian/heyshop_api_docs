# HeyShopApp SDK简介

嘿店支持第三方开发者开发插件扩展嘿店后台管理面板，插件在后台管理工具页面中以嵌入iframe的方式存在。

通过App SDK，插件可以轻松地调用一些嘿店后台管理面板中的组件，使开发更加简化，也可以使插件与后台风格更加统一。

HeyShopApp SDK可以在插件前端通过script tag引入

```html
<script src="https://assets.heidiancdn.com/admin/static/js/HeyShopApp.js"></script>
```

可以将网站后台链接改为`/admin/apps/1?test={your-local-url}`，将{your-local-url}修改为插件调试地址，可以进行插件sdk的调试。

SDK主要分为Dialog， Navigator， Notification， Picker和Toolbar