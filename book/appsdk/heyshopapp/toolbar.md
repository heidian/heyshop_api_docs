# Toolbar

{% method -%}

## 设定插件显示标题
```
HeyShopApp.Toolbar.setTitle(title)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
title    | 是       |插件显示标题

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.setTitle(title)

```

{% endmethod %}

{% method -%}

## 设定插件浮动按钮
```
HeyShopApp.Toolbar.setActions(actions, callbacks)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
actions    | 是       |actions是一个数组，其中每一个元素是个对象，每个对象可以包含title和icon信息（可以参考[material design](https://material.io/icons)的icon名称）, 除此以外，对象可以有搜索选项，如果search: true，点击时会出现关键字搜索框
callbacks  | 是       |callbacks是一个数组，每一个元素是个函数，函数没有输入参数，可以设定特定按钮对应的特定功能

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.setActions([
  {icon: 'save', title: '保存'},
  {icon: 'queue', title: '复制'},
  {icon: 'search', title: '搜索', search: true}
], [
	function() {
		console.log('点击保存')
		HeyShopApp.Notification.success('保存成功')
	},
	function() {
		console.log('点击复制')
		HeyShopApp.Notification.error('保存失败')
	},
	function() {

	}
])
```

{% endmethod %}

{% method -%}

## 监听搜索词变化
```
HeyShopApp.Toolbar.searchWordListener(callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
callback   | 是       | 收到消息后的回调函数回调函数中有keyword这个属性

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.searchWordListener(
    (data) => {
      console.log('called', data)
      if (data && data.keyword !== undefined) {
        this.searchword = data.keyword
      }
    })
```

{% endmethod %}

{% method -%}

## 设定插件Icon
```
HeyShopApp.Toolbar.setIcon(url)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
url       | 是       | icon图片的url

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.setIcon('https://up.img.heidiancdn.com/o_1bpd6u1ss11et7k41n4g5kf1dvp0ollogo.png?imageView2/1/w/120/h/120/ignore-error/1')
```

{% endmethod %}

{% method -%}

## 设定Toolbar上的返回按钮
```
HeyShopApp.Toolbar.setReturnOn(url)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
url       | 否       | 使用url时点击按钮返回为那个url，不使用url时点击按钮为返回历史记录中的上一页。

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.setReturnOn()
```

{% endmethod %}

{% method -%}

## 隐藏Toolbar上的返回按钮
```
HeyShopApp.Toolbar.setReturnOff()
```

### 参数列表

无
### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Toolbar.setReturnOff()
```

{% endmethod %}
