# Picker

{% method -%}

## 打开媒体库选择图片

```
HeyShopApp.Picker.imagePick(callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
callback   | 是       |一个回调函数callback(e)，参数e在选择成功时为图片对象，选择失败时为null

### 返回结果

无返回值

{% sample lang="js" -%}
```js
var trigger = document.getElementById('triggerclick')
trigger.addEventListener('click', function() {
	HeyShopApp.Picker.imagePick(function(e) {
		if (e && e.src) {
			var img = document.getElementById('imageselected')
			img.style = `background-image: url('${e.src}')`
		}
	})
})

```

{% endmethod %}

{% method -%}

## 打开商品对话框进行选择

```
HeyShopApp.Picker.productPick(callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
callback   | 是       |一个回调函数callback(e)，参数e在选择成功时为商品id，选择失败时为null

### 返回结果

无返回值

{% sample lang="js" -%}
```js
var pickproduct = document.getElementById('pickproduct')
pickproduct.addEventListener('click', function() {
	HeyShopApp.Picker.productPick(function(e) {
		if (e !== null) {
			var pid = document.getElementById('productid')
			pid.innerText = e.toString()
		}
	})
})
```

{% endmethod %}