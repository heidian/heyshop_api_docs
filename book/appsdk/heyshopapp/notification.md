# Notification

{% method -%}

## 成功消息通知

```
HeyShopApp.Notification.success(message)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
message    | 是       |成功提示文字

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Notification.success(message)

```

{% endmethod %}

{% method -%}

## 失败消息通知

```
HeyShopApp.Notification.error(message)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
message    | 是       |失败提示文字

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Notification.error(message)

```

{% endmethod %}


{% method -%}

## 显示载入圆圈

```
HeyShopApp.Notification.loadingOn()
```

### 参数列表

无

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Notification.loadingOn()

```

{% endmethod %}

{% method -%}

## 关闭载入圆圈
```
HeyShopApp.Notification.loadingOff()
```

### 参数列表

无

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Notification.loadingOff()

```

{% endmethod %}
