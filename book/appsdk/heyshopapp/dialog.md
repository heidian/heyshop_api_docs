# Dialog

{% method -%}

## 打开危险警告dialog

```
HeyShopApp.Dialog.dangerDialog({title, text, callback})
```

### 参数列表

输入参数是一个对象，拥有以下成员

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
title      | 否      |对话框的标题
text       | 否      |对话框内容描述
callback   | 否      |回调函数，参数为true或false

### 返回结果

无返回值

{% sample lang="js" -%}
```js
var danger = document.getElementById('danger')
danger.addEventListener('click', function() {
	HeyShopApp.Dialog.dangerDialog({
		title: '我的滑板鞋',
		text: '时尚时尚最时尚',
		callback: (e)=> {
			console.log(e)
		}
	})
})

```

{% endmethod %}

{% method -%}

## 打开普通警告dialog

```
HeyShopApp.Dialog.warningDialog({title, text, callback})
```

### 参数列表

输入参数是一个对象，拥有以下成员

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
title      | 否      |对话框的标题
text       | 否      |对话框内容描述
callback   | 否      |回调函数，参数为true或false

### 返回结果

无返回值

{% sample lang="js" -%}
```js
var warn = document.getElementById('warn')
warn.addEventListener('click', function() {
	HeyShopApp.Dialog.warningDialog({
		title: '回家的路上',
		text: '我情不自禁',
		callback: (e)=> {
			console.log(e)
		}
	})
})

```

{% endmethod %}