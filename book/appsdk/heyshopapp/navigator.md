# Navigator

插件中不允许添加a标签或者跳转，url的变化需要通过API请求dashboard进行

{% method -%}

## 重定向页面

```
HeyShopApp.Navigator.redirect(url)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
url       | 是       |跳转的url

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Navigator.redirect(url)

```

注： 跳转链接前会跳出对话框提示用户

{% endmethod %}

{% method -%}

## 切换管理后台中的页面

```
HeyShopApp.Navigator.pushState(url)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
url       | 是       |管理后台中的某个页面的相对路径

### 返回结果

无返回值

{% sample lang="js" -%}
```js
HeyShopApp.Navigator.pushState(url)

```


{% endmethod %}
