# heyshopSDK

heyshopSDK会通过模版的layout引入，实现在shopfront项目中，模版前端可以访问HeyShop对象操作一些模版前端通用的功能。

##开始使用

在需要调用 JS SDK 的页面的 `<head>` 标签引入如下 js 文件:  

	{{ 'javascripts/heyshop.min.js' | global_asset_url }}
	{{ 'javascripts/heyshop-shop.min.js' | global_asset_url }}
	{{ 'javascripts/heyshop-front.min.js' | heyshop_asset_url }}?v={{build_no}}
	{{ 'javascripts/heyshop-njkenv.min.js' | heyshop_asset_url }}?v={{build_no}}
	

##接口目录

* [购物车](heyshopsdk/cart.md)
* [客户](heyshopsdk/customer.md)