# 文档目录结构

一个主题文件包的源代码文档目录结构如下：

```

theme_boilerplate
+-- settings/                       配置文件目录
| +-- settings.js                   基本配置，不用更改
| `-- settings_local.example.js     本地配置
*-- theme/                          主题文件目录
| +-- assets/                       资源文件目录
| | +-- fonts/                      字体文件目录
| | +-- images/                     图片文件目录
| | +-- javascripts/                js脚本文件目录
| | `-- stylesheets/                css样式文件目录
| +-- blocks/                       版块文件目录
| | +-- some_block.json             某版块的配置文件
| | `-- some_block.njk              某板块的模版文件
| +-- layouts/                      layout文件目录
| | `-- default.njk                 layout文件
| +-- locales/                      i18n文件目录
| | +-- zh.json                     模版的中文翻译文件
| | +-- en.json                     模版的英文翻译文件
| | +-- schema.zh.json              模版配置的中文翻译文件
| | `-- schema.en.json              模版配置的英文翻译文件
| +-- snippets/                     代码片段目录
| +-- presets/                      预设目录
| | `-- default/                    default预设目录
| |   +-- home.json                 home页面的配置文件
| |   +-- search.json               search页面的配置文件
| |   `-- ...                       其他页面的配置文件
| `-- theme.json                    全局配置文件
+-- cli.js                          主题编辑命令行工具
+-- gulpfile.js                     编译工具
`-- package.json                    npm配置文件


```