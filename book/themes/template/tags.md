# 模版标签

模版采用nunjucks渲染引擎，具体语法可以参考[Nunjucks文档](http://mozilla.github.io/nunjucks/templating.html)，一般我们会创建以.njk为扩展名的版块模版。

## 普通标签

普通标签与nunjucks完全相同，可以直接参考文档，该文档将主要篇幅用作介绍嘿店自定义标签。

## component & endcomponent
component标签用于定义嘿店版块，根据数据渲染出一些带有特定嘿店格式数据的component，会渲染出一个section标签带有一些嘿店特定的参数作为属性，然后\{ % component % \}与\{ % endcomponent % \}标签之间的内容会被渲染在这个section中。

## script & endscript
script标签用于嵌套进component中，在script标签中可以加入javascript代码，并且用$el可以访问本component元素，进行一些脚本操作，$el是一个jquery对象，可以采用jquery的api，也可以使用一些原生api或者其他插件达到想要的效果。这段脚本的运行时间是要等页面完全载入才之行。

## style & endstyle
style标签用于嵌套进component中, 在style标签中可以加入一些style代码，会被自动加上该component的选择器，保证样式仅在这个section生效

## asyncset
asyncset标签可以定义
\{ % asyncset results = products.query(settings_data.products) % \}