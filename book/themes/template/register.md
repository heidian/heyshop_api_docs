# 资源注册

在`theme/theme.json`中需要标识出在该主题中的资源。`theme/theme.json`的格式如下


```json

{
  "id": 0,    // 主题id
  "name": "Name",   // 主题数据库中的名称
  "title": "Title",    // 主题市场中显示的主题名
  "author": "Anonymous@HeyShop",    // 作者
  "layouts": [
    "default"
  ],
  "blocks": [
    "article_single",
    "article_list",
    "carousel",
    "cart",
    "contact",
    "collection_single",
    "collection_list",
    "featured_articles",
    "featured_products",
    "footer",
    "header",
    "page_body",
    "product_single",
    "product_list"
  ],
  "snippets": [
    "metadata"
  ],
  "assets": [
    "images/app-icon.png",
    "javascripts/theme.js",
    "javascripts/theme.min.js",
    "stylesheets/styles.css.njk"
  ],
  "presets": [
    "default",
    "alternate"
  ],
  "current_preset": "default",
  "settings_schema": {
    "menu": {
      "type": "Navigation"
    },
    "header_image": {
      "type": "Image"
    },
    "header_image_height": {
      "type": "Number"
    },
    "__colors__": {
      "color-primary": "主色",
      "color-secondary": "辅色",
      "color-text": "文字",
      "color-bg": "背景色",
      "color-link": "链接",
      "color-btn-primary": "按钮颜色",
      "color-btn-primary-text": "按钮文字"
    }
  }
}


```
主要包含三块，基本信息、资源文件和全局配置数据

## 基本信息
theme.json中可以指定上传的主题id(`id`)，名字(`name`, `title`)以及作者的信息(`author`)，用于描述主题的一些基本信息。

## 资源文件
在theme.json中，开发者需要注册使用的版块文件(`blocks`)，布局文件(`layouts`)，片段文件(`snippets`)，资源文件(`assets`)，预设文件(`presets`)以及语言文件(`locales`)。注册这些文件后，上传主题时才会把主题里面的文件上传到嘿店的cdn。

## 全局配置数据
settings_schema中设定了需要主题中全局配置的数据，这里不多说明，可以在[主题配置语法](/theme/schema/globals.html)中了解更多。



