# 全局变量

`theme/layouts/default.njk`定义了一个基础的布局文件，初学者不需要修改这一部分内容，里面规定了一些需要引入的资源文件，和一些全局变量，使得主题开发变的简化。

在`default.njk`里出现了4个全局变量的引入，分别是`__STYLES__`, `__COMPONENTS__`, `__HEYSHOPSDK__`, `__FINETUNING__`。

## __STYLES__

`__STYLES__`引入了全局的css文件。

## __COMPONENTS__

`__COMPONENTS__`引入了所有页面配置中的版块。

## __HEYSHOPSDK__

`__HEYSHOPSDK__`引入了一个HeyShop对象，提供了很多前端能用到的方法，如客户登录，获取商品，购物车操作等。

## __FINETUNING__

当url的query中有edit参数时，`__FINETUNING__`会被引入，是与建站工具通信的js代码。