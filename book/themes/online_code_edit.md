# 主题在线源码编辑页面与样式

需要添加一个 Template 的 njk文件，及相应的 css 文件

### A. 后台控制面板 > 页面 > 新建一个页面，例如 /test

### B. 主题 > 代码编辑 > 页面设置
```
{
  "template_name": "test",
  "locked": false,
  "settings_data": {
    "components": []
  }
}
```

### C. 模板/Templates > 新增一个 Template > test(.njk)

Html Goes Here. 只需要 <body> 里面的内容

###  D. 资源/Assets > 新增一个 stylesheets/test.css(.njk)

CSS Goes Here.

#### E. 再在 test.njk 里引入这段 CSS
```
<link rel="stylesheet" href={{ "stylesheets/test.css.njk" | asset_url }} >

```


