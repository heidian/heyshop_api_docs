#弹框通知

访问HeyShop.toast相关方法，可以向页面发送弹框通知

{% method -%}

##发起一次弹框通知

```
HeyShop.toast.popupToast(information,elapsetime)
```

###参数名称

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|-------
information   | 否      |String型|发送的消息内容，默认为"undifined"
elapsetime   | 否      |Int型|弹出消息的存在时长，默认为1500毫秒

{% sample lang="js" -%}

###返回结果

无具体返回结果 页面弹出消息通知

{% endmethod %}
