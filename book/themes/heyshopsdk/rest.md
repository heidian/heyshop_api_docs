#商品、专题、博客前端api

访问HeyShop.Products,HeyShop.Collections和HeyShop.Articles相关方法，可以进行商品，专题和博客方面的相关操作。在进行操作之前，必须先实例化对象，即对对象进行new操作。

{% method -%}

##获取所有商品

```
var heyProduct = new HeyShop.Products()
heyProduct.list(query)
heyProduct.listNext()
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
query   | 否      |对象|获取商品时，可以通过query进行商品筛选，若为空，则默认返回10个商品信息。listNext()会根据list(query)的参数设定返回其余的值，如query为page_size=5,即每页返回5个商品信息，listNext()会返回另外5个商品的信息，以此类推。

###Query 参数

```
id: 1002
id__in: 1002,1005
name: shoe
name__in: shoe,shoe2
voucherproduct__is_enabled: true  // 是否为代金券商品
published_at_from: 2017-02-01 00:00:00
published_at_to: 2017-03-31 00:00:00
published: true
title: 测试商品鞋
price_from: 10.00
price_to: 22.55
collection: 22
collection__exclude: 22
category: 5
category__in: 5,6,7,10
coupons: 28
coupons__exclude: 28
q: 搜索关键词
order_by: id (id / created_at / name / inventory_quantity / title / price / published_at)
page: 1
page_size: 10
```

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Array(6)
0:category_titles:[]
  compare_at_price: null
  created_at: "2017-10-10T17:10:05.783220+08:00"
  description:""
  id: 18925
  image: {id: 73366,
          src: "https://up.img.heidiancdn.com/o_1bnl8hs761hl21u301euro7s11b90grande.png",
          position: 0,
          metafield: {…}}
  images:[{…}]
  inventory_quantity: 998
  metafields: (2) [{…}, {…}]
  name: "e319c5"
  options: [{…}]
  price: "50.00"
  published_at: "2017-10-10T17:10:05.765381+08:00"
  requires_shipping: false
  tags: []
  title: "test111"
  variants: [{…}]
  vendor: null
...
__proto__: Object
```

{% endmethod %}



{% method -%}

##获取指定商品（通过id值）

```
var heyProduct = new HeyShop.Products()
heyProduct.get(id)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
id         |是 	    |Int型   |商品的id值

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
	body_html:""
	body_html_mobile: ""
	category_ids:[]
	category_titles: []
	compare_at_price: null
	created_at: "2017-10-10T17:10:05.783220+08:00"
	description: ""
	id: 18925
	image: {id: 73366,
	        src: "https://up.img.heidiancdn.com/o_1bnl8hs761hl21u301euro7s11b90grande.png",
	        position: 0,
	        metafield: {…}}
	images: [{…}]
	inventory_policy: "deny"
	inventory_quantity: 998
	meta_description: ""
	meta_image: "https://up.img.heidiancdn.com/o_1bnl8hs761hl21u301euro7s11b90grande.png"
	meta_title: "test111"
	metafields: (2) [{…}, {…}]
	name: "e319c5"
	options: [{…}]
	price: "50.00"
	published: true
	published_at: "2017-10-10T17:10:05.765381+08:00"
	requires_shipping: false
	shipping_method_group_id: null
	sold_quantity: 1
	tags:[]
	title: "test111"
	updated_at: "2017-10-10T17:10:05.783248+08:00"
	variants: [{…}]
	vendor: null
	voucher_product:{id: 674,
						product_id: 18925,
						is_enabled: true,
						white_product_ids: Array(0),
						black_product_ids: Array(0),
						…}
__proto__: Object
```

{% endmethod %}



{% method -%}

##获取指定商品（通过name）

```
var heyProduct = new HeyShop.Products()
heyProduct.getByName(name)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
name       |是 	 |String型   |商品的名称

{% sample lang="js" -%}

###返回结果

返回结果同上一接口 返回完整的商品信息

{% endmethod %}



{% method -%}

##获取全部专题

```
var heyCollection = new HeyShop.Collections()
heyCollection.list(query)
heyCollection.listNext()
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
query      |否 	 |  对象   |获取专题时，可以通过query进行专题筛选，若为空，则默认返回10个商品信息。listNext()会根据list(query)的参数设定返回其余的值，如query为page_size=5,即每页返回5个专题信息，listNext()会返回另外5个专题的信息，以此类推。

###Query 参数

```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
published_at_from: 2017-02-01 00:00:00
published_at_to: 2017-03-31 00:00:00
published: true
title: 专题名
page: 1
page_size: 10
```

{% sample lang="js" -%}

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Array(4)
0:
	description: "“布艺”这个词总是伴随“小清新”，浅色系的棉麻布料最能诠释清新淡雅的感觉。人们对个性化的追求，布料和木材的创新也与时俱进，让人眼前一亮。中国传统家具是运用木材的典范，在与布艺结合之后呈现出新东方风尚。"
	id: 6375
	image: {metafield: {…},
			 src: "https://up.img.heidiancdn.com/o_1b3cf0nv6g49qg91mec1f6v4u30img1.jpg"}
	metafields: []
	name: "c1"
	published_at: "2016-12-07T16:12:55.226141+08:00"
	title: "拥抱生活温暖我"
__proto__:Object
1:...
2:...
3:...
```

{% endmethod %}


{% method -%}

##获取指定专题（通过id值）

```
var heyCollection = new HeyShop.Collections()
heyCollection.get(id)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
id         |是 	    |Int型   |专题的id值

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
	body_html: ...
	created_at: "2017-09-06T15:04:22.739475+08:00"
	description: "古典与现代的编制中，在典雅与品质的交错下，皮质与布艺，原木色与漆艺的点缀，展现出耐人寻味的生活肌理。生态的陈列装饰，简单、简约，但却最有生活色彩。"
	disjunctive: false
	id: 6376
	image:	{metafield: {…},
			 src: "https://up.img.heidiancdn.com/o_1b3cf0nv7c89i2180lrvocu70img2.jpg"}
	is_running: true
	meta_description: "古典与现代的编制中，在典雅与品质的交错下，皮质与布艺，原木色与漆艺的点缀，展现出耐人寻味的生活肌理。生态的陈列装饰，简单、简约，但却最有生活色彩。"
	meta_image: "https://up.img.heidiancdn.com/o_1b3cf0nv7c89i2180lrvocu70img2.jpg"
	meta_title: "NEW ARRIVALS"
	metafields: []
	name: "c2"
	order_by: ""
	published: true
	published_at: "2016-12-07T16:12:55.242106+08:00"
	results: undefined
	rules: []
	smart: false
	title: "NEW ARRIVALS"
	updated_at: "2017-09-06T15:04:22.739498+08:00"
__proto__: Object
```

{% endmethod %}



{% method -%}

##获取指定专题（通过name）

```
var heyCollection = new HeyShop.Collections()
heyCollection.getByName(name)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
name       |是 	    |String型  |专题的名称

{% sample lang="js" -%}

###返回结果

返回结果同上一接口 返回完整的专题信息

{% endmethod %}



{% method -%}

##获取全部博客文章

```
var heyArticle = new HeyShop.Articles()
heyArticle.list(query)
heyArticle.listNext()
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
query      |否 	 |  对象   |获取博客时，可以通过query进行博客筛选，若为空，则默认返回10个商品信息。listNext()会根据list(query)的参数设定返回其余的值，如query为page_size=5,即每页返回5个博客信息，listNext()会返回另外5个博客的信息，以此类推。

###Query 参数

```
id: 1002
id__in: 1002,1005
name: test
name__in: test1,test2
published_at_from: 2017-02-01 00:00:00
published_at_to: 2017-03-31 00:00:00
published: true
title: test_tiele
tag: tag_title1,tag_title2
order_by: id (id / created_at / published_at)
page: 1
page_size: 10
```

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Array(4)
	0:
		description: "“布艺”这个词总是伴随“小清新”，浅色系的棉麻布料最能诠释清新淡雅的感觉。人们对个性化的追求，布料和木材的创新也与时俱进，让人眼前一亮。中国传统家具是运用木材的典范，在与布艺结合之后呈现出新的东方风尚。"
		id: 8926
		image:
				{metafield: {…},
							  src: "https://up.img.heidiancdn.com/o_1b3cf0nvaktd1rc9fa7opm1mor0img6.jpg"}
		metafields:(5) [{…}, {…}, {…}, {…}, {…}]
		name: "b4"
		published_at: "2016-12-07T16:12:00+08:00"
		title: "原木与布艺的故事"
__proto__: Object
	1: ...
	2: ...
	3: ...
	length: 4
__proto__: Array(0)
```

{% endmethod %}


{% method -%}

##获取指定博客（通过id值）

```
var heyArticle = new HeyShop.Articles()
heyArticle.get(id)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
id         |是 	    |Int型   |博客的id值

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
	author: "匿名"
	body_html: ...
	category: null
	category_id: null
	created_at: "2017-09-06T15:04:22.924002+08:00"
	description: "“布艺”这个词总是伴随“小清新”，浅色系的棉麻布料最能诠释清新淡雅的感觉。人们对个性化的追求，布料和木材的创新也与时俱进，让人眼前一亮。中国传统家具是运用木材的典范，在与布艺结合之后呈现出新的东方风尚。"
	id: 8926
	image:{metafield: {…},
			src: "https://up.img.heidiancdn.com/o_1b3cf0nvaktd1rc9fa7opm1mor0img6.jpg"}
	meta_description: "“布艺”这个词总是伴随“小清新”，浅色系的棉麻布料最能诠释清新淡雅的感觉。人们对个性化的追求，布料和木材的创新也与时俱进，让人眼前一亮。中国传统家具是运用木材的典范，在与布艺结合之后呈现出新的东方风尚。"
	meta_image: "https://up.img.heidiancdn.com/o_1b3cf0nvaktd1rc9fa7opm1mor0img6.jpg"
	meta_title: "原木与布艺的故事"
	metafields: (5) [{…}, {…}, {…}, {…}, {…}]
	name: "b4"
	published: true
	published_at: "2016-12-07T16:12:00+08:00"
	tags: (2) ["test", "test3"]
	title: "原木与布艺的故事"
	updated_at: "2017-10-20T14:51:36.690445+08:00"
__proto__: Object
```

{% endmethod %}



{% method -%}

##获取指定博客（通过name）

```
var heyArticle = new HeyShop.Articles()
heyArticle.getByName(name)
```

### 参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
name       |是 	    |String型  |博客的名称

{% sample lang="js" -%}

###返回结果

返回结果同上一接口 返回完整的文章信息

{% endmethod %}