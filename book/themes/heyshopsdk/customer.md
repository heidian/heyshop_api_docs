#顾客前端API

访问HeyShop.Customer相关方法，可以进行顾客登入、注册、登出等相关操作  


{% method -%}

##用户密码登入

```
HeyShop.Customer.login(mobile, password)
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|-------
mobile     | 是      |String型|用户的登入手机号 
password   | 是      |String型|用户的登入密码

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
id: 11409
mobile: "139XXXX2923"
token: "ec5d4d779ea518831b0e2ea28b6ea324a444bf75"
```

{% endmethod %}



{% method -%}

##用户验证码登入

```
HeyShop.Customer.loginWithCode(mobile, code)
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|-------
mobile     | 是      |String型|用户的登入手机号 
code       | 是      |Int型   |用户的手机验证码

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
id: 11409
mobile: "139XXXX2923"
token: "ec5d4d779ea518831b0e2ea28b6ea324a444bf75"
__proto__: Object
```

{% endmethod %}



{% method -%}

##用户注册

```
HeyShop.Customer.register(mobile, password)
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|-------
mobile     | 是      |String型|用户的登入手机号 
code       | 是      |Int型   |用户的手机验证码

{% sample lang="js" -%}

###返回结果

```
__proto__: Promise
[[PromiseStatus]]: "resolved"
[[PromiseValue]]: Object
birthday: null
cart_token: null
email: ""
full_name: ""
gender: "unknown"
id: 15212
mobile: "12345678901"
token: "38f2cf2c963d957635cb7abb81e93a3072e4933e"
__proto__: Object
```

{% endmethod %}



{% method -%}

##发送验证码

```
HeyShop.Customer.sendVerificationCode(mobile)
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
mobile     | 是      |String型|用户的登入手机号 


{% sample lang="js" -%}

###返回结果

无

{% endmethod %}



{% method -%}

##用户登出

```
HeyShop.Customer.logout()
```

###参数列表

无


{% sample lang="js" -%}

###返回结果


{% endmethod %}


{% method -%}

##微信登入

```
HeyShop.Customer.loginWithWechat(next_url)
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|--------
next_url   | 否      |String型|登入成功后跳转的网址，默认为买家的个人中心

{% sample lang="js" -%}

###返回结果 

无

{% endmethod %}