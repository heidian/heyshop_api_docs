#支付

访问HeyShop.Customer相关方法，可以进行顾客登入、注册、登出等相关操作

{% method -%}

##发起支付

```
HeyShop.payForOrder(token, channel)
HeyShop.payForOrder("1393df96437e96970989d908b37f7df093cca153","alipay_wap")
```

###参数列表

参数名称    | 是否必须  |参数类型|参数说明
-----------|---------|-------|-------
token      | 是      |String型|商品token值，可以在商品结算时的url中获取 
channel    | 是      |String型|用户的支付方式，嘿店目前支持微信和支付宝支付

{% sample lang="js" -%}

###返回结果

无返回结果 页面跳转至微信／支付宝支付页面

{% endmethod %}