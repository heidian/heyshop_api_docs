# 购物车相关前端API

访问HeyShop.cart相关方法，可以进行加入购物车和订单相关操作

参数中的`variant_id`和`item_id`可以通过/api/cart的api获取

{% method -%}

## 将商品加入购物车

```
HeyShop.cart.add(variant_id, quantity, callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
variant_id | 是      |商品某一SKU的id 
quantity   | 否      |加入购物车的数量，默认为1
callback   | 否      |加入购物车的回调函数，若失败则有参数，成功时没有参数

{% sample lang="js" -%}

### 返回结果

无返回值


```js
HeyShop.cart.add(variant_id, quantity, function(err) {
    if (err) {
        console.log('加入失败');
    } else {
        console.log('加入成功');
    }
});
```

{% endmethod %}

{% method -%}

## 将商品移出购物车

```
HeyShop.cart.delete(item_id, callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
item_id    | 是      |购物车中items列表中的条目的id
callback   | 否      |若删除成功，则会调用回调函数

{% sample lang="js" -%}

### 返回结果

无返回值


```js
HeyShop.cart.delete(item_id);
```

{% endmethod %}

{% method -%}

## 设置购物车商品数量

```
HeyShop.cart.setQuantity(variant_id, quantity, callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
variant_id | 是      |某一商品SKU的id
quantity   | 否      |设定购物车中该商品SKU的数量
callback   | 否      |回调函数，若失败则有参数，成功时没有参数


{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.delete(item_id, fuction(){
    console.log('delete succeed');
});
```

{% endmethod %}

{% method -%}

## 勾选购物车中的某一项商品（进入待结算状态）

```
HeyShop.cart.checkItem(item_id)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
item_id    | 是      |购物车中items列表中的条目的id


{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.checkItem(item_id);
```

{% endmethod %}

{% method -%}

## 取消勾选购物车中的某一项商品（不进入待结算状态）

```
HeyShop.cart.uncheckItem(item_id)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
item_id    | 是      |购物车中items列表中的条目的id

{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.uncheckItem(item_id);
```

{% endmethod %}

{% method -%}

## 勾选购物车中所有商品（进入待结算状态）

```
HeyShop.cart.checkAll()
```

### 参数列表

无

{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.checkAll());
```

{% endmethod %}

{% method -%}

## 取消勾选购物车中所有商品（不进入待结算状态）

```
HeyShop.cart.uncheckAll()
```

### 参数列表

无

{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.uncheckAll();
```

{% endmethod %}


{% method -%}

## 结算购物车中已选中的商品

```
HeyShop.cart.checkout()
```

### 参数列表

无，自动进入结算页面

{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.checkout();
```

{% endmethod %}

{% method -%}

## 立即结算参数中的商品

```
HeyShop.cart.checkoutVariant(variant_id, quantity, callback)
```

### 参数列表

参数名称    | 是否必须  |参数介绍
-----------|---------|--------------
variant_id | 是      |商品某一SKU的id 
quantity   | 否      |该商品SKU的数量，默认为1
callback   | 否      |若失败调用callback函数，参数为错误信息

自动进入结算页面

{% sample lang="js" -%}

### 返回结果

无返回值

```js
HeyShop.cart.checkoutVariant(variant_id, quantity, function(err){
    if (err) {
        console.log(err.detail || '出错了，商品信息有误或库存不足');
    } 
});
```

{% endmethod %}

