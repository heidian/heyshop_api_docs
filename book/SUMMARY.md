# Summary

## 简介

* [嘿店接口简介](README.md)

## API 文档

* [API 文档简介](api/README.md)
* [授权](api/authorization.md)
* [商品](api/catalogue/product.md)
  * [商品](api/catalogue/product.md)
  * [专题](api/catalogue/collection.md)
  * [SKU](api/catalogue/sku.md)
* [订单](api/order.md)
  * [发货](api/order/fulfillment.md)
  * [退款申请](api/order/refund_request.md)
  * [订单生命周期](api/order/lifecycle.md)
  * [订单状态说明](api/order/status.md)
* [店铺](api/shops.md)
  * [店铺前端页面](api/shops/pages.md)
  * [身份验证](api/shops/identification.md)
  * [自定义域名](api/shops/custom_domain.md)
  * [主菜单](api/shops/menu.md)
  * [店铺信息](api/shops/shop_info.md)
* [代金券](api/voucher.md)
* [优惠券](api/coupon.md)
* [博客](api/blog.md)
* [库存](api/inventory.md)
* [用户管理 \(CRM\)](api/crm.md)
* [微信授权](api/weixin.md)
* [店铺主题](api/shopthemes.md)
  * [主题市场](api/shopthemes/themes.md)
  * [主题文件](api/shopthemes/themefile.md)
* [运费模板](api/shippings.md)
* [媒体文件](api/medias.md)
* [会员积分](api/loyalty.md)

<!-- 前面要加空行才能注释掉
* [配置支付信息](api/payment_channel.md)
  * [Ping++子账户](api/payment_channel/pingxx.md)
    * [支付宝（wap端）](api/payment_channel/pingxx/alipay_wap.md)
    * [支付宝（PC端）](api/payment_channel/pingxx/alipay_pc_direct_channel.md)
    * [微信](api/payment_channel/pingxx/wx_pub_channel.md)
  * [Ping++配置信息](api/payment_channel/pingxx_account.md)
  * [发起付款请求](api/payment_channel/pay_for_order.md)
  * [测试支付](api/payment_channel/test_order.md)
-->

<!-- 前面要加空行才能注释掉
* [短信定制](api/customized_sms/marketing_messgae.md)
  * [营销类短信](api/customized_sms/marketing_messgae.md)
  * [通知类短信](api/customized_sms/notification_message.md)
-->

<!-- 前面要加空行才能注释掉
* [消息通知](api/notifications.md)
* [提现](api/balance.md)
* [套餐](api/pricing.md)
* [发票](api/invoice.md)
* [应用市场](api/appstore.md)
* [购买域名](api/domain.md)
* [帮助中心](api/supports.md)
* [数云](api/shuyun.md)
 -->

## Webhooks

* [Webhooks (旧)](webhooks/webhooks.md)

## 店铺前端接口

* [商品](shopfront/product.md)
* [商品专题](shopfront/collection.md)
* [博客文章](shopfront/article.md)
* [购物车](shopfront/cart.md)
* [买家登录/注册](shopfront/customer.md)
* [客户端配置](api/clients.md)
* [结算](api/checkout.md)

<!-- 前面要加空行才能注释掉
* [用户中心](api/customers/customer_order.md)
  * [买家订单管理](api/customers/customer_order.md)
  * [买家卡券管理](api/customers/customer_card.md)
  * [快速登录](api/customers/quicklogin.md)
  * [密码登录](api/customers/login.md)
  * [密码重置](api/customers/resetpassword.md)
  * [注册](api/customers/register.md)
-->

## 主题开发

* [主题开发简介](themes/README.md)
* [快速入门](themes/getting_started.md)
* [在线源码编辑](themes/online_code_edit.md)
* [文档目录结构](themes/document_structure.md)
* [网页模版语法](themes/template.md)
  * [资源注册](themes/template/register.md)
  * [全局变量](themes/template/layout.md)
  * [模版标签](themes/template/tags.md)
  * [模版过滤器](themes/template/filters.md)
  * [例子1：如何创建一个静态的自定义板块](themes/template/example_static.md)
  * [数据注入](themes/template/injection.md)
  * [例子2：如何创建一个有数据注入的自定义板块](themes/template/example_data.md)
  * [例子3: 在板块内部使用js](themes/template/example_script.md)
  * [语言支持](themes/template/locales.md)
  * [例子4: 为版块中的文字添加双语支持](themes/template/example_locales.md)
* [HeyShopSDK](themes/heyshopsdk.md)
  * [购物车](themes/heyshopsdk/cart.md)
  * [客户](themes/heyshopsdk/customer.md)
  * [商品、专题、博客数据查询](themes/heyshopsdk/rest.md)
  * [模版引擎](themes/heyshopsdk/njkenv.md)
  * [图片懒加载](themes/heyshopsdk/images.md)
  * [事件侦听](themes/heyshopsdk/events.md)
  * [支付](themes/heyshopsdk/pay.md)
  * [弹框通知](themes/heyshopsdk/toast.md)
* [主题配置语法](themes/schema.md)
  * [主题配置基本类型](themes/schema/basictype.md)
  * [主题配置特殊类型](themes/schema/specialtype.md)
  * [全局配置](themes/schema/globals.md)
  * [主题配置语言支持](themes/schema/locales.md)

## 插件开发

* [HeyShopApp嵌入API](appsdk/heyshopapp.md)
  * [导航](appsdk/heyshopapp/navigator.md)
  * [工具栏](appsdk/heyshopapp/toolbar.md)
  * [提示](appsdk/heyshopapp/notification.md)
  * [对话框](appsdk/heyshopapp/dialog.md)
  * [选择器](appsdk/heyshopapp/picker.md)

## 官方插件
<!--* [VIP会员积分插件](apps/loyalty.md)-->
* [线下门店](apps/offline.md)
