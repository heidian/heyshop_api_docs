## 如何运行

`npm install gitbook-cli -g`

`gitbook install`

`gitbook serve`


## Structure

[https://toolchain.gitbook.com/structure.html](https://toolchain.gitbook.com/structure.html)


## Production

`cd /workspace/heyshop_api_docs`

`git pull`

`gitbook build ./ /var/www/html/heyshop_api_docs`
